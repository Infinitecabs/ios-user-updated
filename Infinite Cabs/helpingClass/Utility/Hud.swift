//
//  Hud.swift
//  SocialHub
//
//  Created by Mac on 12/6/19.
//  Copyright © 2019 Mac. All rights reserved.
//

import Foundation
import SVProgressHUD

class Hud : NSObject
{
    static let shared = Hud()
    
    func showHud()
    {
        DispatchQueue.main.async {
            SVProgressHUD.setDefaultMaskType(.custom)
//            SVProgressHUD.setBackgroundColor(UIColor.init(white: 0.5, alpha: 0.2))
            SVProgressHUD.setBackgroundColor(UIColor.init(white: 1, alpha: 1))
            SVProgressHUD.setBackgroundLayerColor(UIColor.init(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.2))
            SVProgressHUD.show()
        }
    }
    
    func hideHud()
    {
        DispatchQueue.main.async {
            SVProgressHUD.dismiss()
        }
    }
    
    func showHudWithMsg(_ msg:String)
       {
           DispatchQueue.main.async {
               SVProgressHUD.setDefaultMaskType(.custom)
               SVProgressHUD.setBackgroundColor(UIColor.init(white: 1, alpha: 1))
               SVProgressHUD.setBackgroundLayerColor(UIColor.init(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.2))
            SVProgressHUD.show(withStatus: msg)
//            SVProgressHUD.show(UIImage(named: "car-middle1")!, status: msg)
           }
       }
    
    /*
    func show()
    {
        let sdLoader = SDLoader()
        sdLoader.startAnimating(atView: self.view)
    }
    */
    
}
