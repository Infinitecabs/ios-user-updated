//
//  ApiKeys.swift
//  SocialHub
//
//  Created by Mac on 12/3/19.
//  Copyright © 2019 Mac. All rights reserved.
//

import Foundation

enum ApiKey {
    
    static var status: String { return "status" }
    static var code: String { return "statusCode" }
    static var result: String { return "RESULT" }
    static var message: String { return "message" }

    
    //MARK:- Api keys
    static var userId: String { return "user_id" }
    static var email: String { return "email" }
    static var Password: String { return "password" }
    static var DeviceToken: String { return "deviceToken" }
    static var Image: String { return "image" }
    
    
    static var name: String { return "name" }
    static var phone: String { return "mobile" }
    
    static var address: String { return "address" }
    static var cityName: String { return "city" }
    static var countryName: String { return "country" }
    static var stateName: String { return "state" }
    static var userProfile: String { return "image" }
    static var userProfileThumb: String { return "thumbnailimage" }
    static var pincode: String { return "pincode" }
    
    //Bank Deatils
    
    static var bankName: String { return "bankName" }
    static var accountHolderName: String { return "accountHolderName" }
    static var accountNo: String { return "accountNo" }
    static var branch: String { return "branch" }
    
}

enum AppConstantKey {
    
    static var HallOwner: String { return "Hall-Owner" }
    static var Hall: String { return "Hall" }
    
}
