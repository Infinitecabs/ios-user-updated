//
//  CustomClass.swift
//  Infinite Cabs
//
//  Created by micro on 18/10/19.
//  Copyright © 2019 micro. All rights reserved.
//

import Foundation
import UIKit


//@IBDesignable
class CardView: UIView {

     var cornerRadius: CGFloat = 5

     var shadowOffsetWidth: Int = 0
     var shadowOffsetHeight: Int = 0
     var shadowRadius: Float = 4.0
     var shadowColor: UIColor? = UIColor.black
     var shadowOpacity: Float = 0.2
    

    override func layoutSubviews() {

        layer.cornerRadius = cornerRadius
        let shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius)
        layer.masksToBounds = false
        layer.shadowColor = shadowColor?.cgColor
        layer.shadowOffset = CGSize(width: shadowOffsetWidth, height: shadowOffsetHeight);
        layer.shadowOpacity = shadowOpacity
        layer.shadowRadius = CGFloat(shadowRadius)
        layer.shadowPath = shadowPath.cgPath
    }
}


class CardViewRide: UIView {

     var cornerRadius: CGFloat = 5

     var shadowOffsetWidth: Int = -1
     var shadowOffsetHeight: Int = 1
     var shadowRadius: Float = 1.0
     var shadowColor: UIColor? = UIColor.black
     var shadowOpacity: Float = 0.2
    

    override func layoutSubviews() {

        layer.cornerRadius = cornerRadius
        let shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius)
        layer.masksToBounds = false
        layer.shadowColor = shadowColor?.cgColor
        layer.shadowOffset = CGSize(width: shadowOffsetWidth, height: shadowOffsetHeight);
        layer.shadowOpacity = shadowOpacity
        layer.shadowRadius = CGFloat(shadowRadius)
        layer.shadowPath = shadowPath.cgPath
    }
}


class CardView1: UIView {

     var cornerRadius: CGFloat = 8

     var shadowOffsetWidth: Int = 0
     var shadowOffsetHeight: Int = 0
     var shadowRadius: Float = 5.0
     var shadowColor: UIColor? = UIColor.black
     var shadowOpacity: Float = 0.2
    

    override func layoutSubviews() {

        layer.cornerRadius = cornerRadius
        let shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius)
        layer.masksToBounds = false
        layer.shadowColor = shadowColor?.cgColor
        layer.shadowOffset = CGSize(width: shadowOffsetWidth, height: shadowOffsetHeight);
        layer.shadowOpacity = shadowOpacity
        layer.shadowRadius = CGFloat(shadowRadius)
        layer.shadowPath = shadowPath.cgPath
    }
}



//map view top corners
class mapTableView: UIView {

    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: [.topRight, .topLeft ,.bottomLeft ,.bottomRight], cornerRadii: CGSize(width: 8, height: 8)) // 17
        
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        
        
        layer.mask = mask
    }
}

// MARK:- CollectionView FlowLayout Class
class ColumnFlowLayout: UICollectionViewFlowLayout {

    let cellsPerRow: Int

    init(cellsPerRow: Int, minimumInteritemSpacing: CGFloat = 0, minimumLineSpacing: CGFloat = 0, sectionInset: UIEdgeInsets = .zero) {
        self.cellsPerRow = cellsPerRow
        super.init()

        self.minimumInteritemSpacing = minimumInteritemSpacing
        self.minimumLineSpacing = minimumLineSpacing
        self.sectionInset = sectionInset
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func prepare() {
        super.prepare()

        guard let collectionView = collectionView else { return }
        let marginsAndInsets = sectionInset.left + sectionInset.right + collectionView.safeAreaInsets.left + collectionView.safeAreaInsets.right + minimumInteritemSpacing * CGFloat(cellsPerRow - 1)
        let itemWidth = ((collectionView.bounds.size.width - marginsAndInsets) / CGFloat(cellsPerRow)).rounded(.down)
        itemSize = CGSize(width: itemWidth, height: itemWidth)
    }

    override func invalidationContext(forBoundsChange newBounds: CGRect) -> UICollectionViewLayoutInvalidationContext {
        let context = super.invalidationContext(forBoundsChange: newBounds) as! UICollectionViewFlowLayoutInvalidationContext
        context.invalidateFlowLayoutDelegateMetrics = newBounds.size != collectionView?.bounds.size
        return context
    }

}


//MARK:- HeadView
class headView:UIView{
    
    override func layoutSubviews() {
        
        self.layer.masksToBounds = false
        self.layer.shadowRadius = 2
        self.layer.shadowOpacity = 0.5
        self.layer.shadowColor = UIColor.lightGray.cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 2)

    }
    
}
