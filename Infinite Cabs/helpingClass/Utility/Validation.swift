//
//  Validation.swift
//  Portonovo
//
//  Created by Mac on 2/6/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation
import UIKit


extension UIViewController {
    
    func validateName(_ name : String) -> Bool {
        
        if(name == ""){
            presentAlertWithTitle(title: Constants.errorMessage.NameEmpty.rawValue , message: "", vc: self)
            return false
        }
        
        return true
    }
    
    func validateDate(_ date : String) -> Bool {
        
        if(date == ""){
            presentAlertWithTitle(title: Constants.errorMessage.DateEmpty.rawValue , message: "", vc: self)
            return false
        }
        
        return true
    }
    
    
    func validateCreditCardNumber(_ creditCardNumber : String) -> Bool {
        
        if(creditCardNumber == ""){
            presentAlertWithTitle(title: Constants.errorMessage.CreditCardNumberEmpty.rawValue , message: "", vc: self)
            return false
        }
        if(creditCardNumber.count != 19){
            presentAlertWithTitle(title: Constants.errorMessage.CreditCardNumberEnvalide.rawValue , message: "", vc: self)
            return false
        }
        
        return true
    }
    
    
    
    func validateCVV(_ cvv : String) -> Bool {
        
        if(cvv == ""){
            presentAlertWithTitle(title: Constants.errorMessage.CVVEmpty.rawValue , message: "", vc: self)
            return false
        }
        
        if(cvv.count != 3 && cvv.count != 4  ){
            presentAlertWithTitle(title: Constants.errorMessage.validCvv.rawValue , message: "", vc: self)
            return false
        }
        
        return true
    }
    
    
    
    
    func validateFirstName(_ name : String) -> Bool {
        
        if(name == ""){
            presentAlertWithTitle(title: Constants.errorMessage.FirstNameEmpty.rawValue , message: "", vc: self)
            return false
        }
        
        return true
    }
    func validateLastName(_ name : String) -> Bool {
        
        if(name == ""){
            presentAlertWithTitle(title: Constants.errorMessage.LastNameEmpty.rawValue , message: "", vc: self)
            return false
        }
        
        return true
    }
    
    
    func validateEmail(_ email : String) -> Bool {
           
           if(email == ""){
               presentAlertWithTitle(title: Constants.errorMessage.EmailEmpty.rawValue , message: "", vc: self)
               return false
           }
        if(!email.contains("@")){
            presentAlertWithTitle(title: Constants.errorMessage.EmailInvalid.rawValue , message: "", vc: self)
            return false
        }
           
           return true
       }
    
    
    func validateMessage(_ message : String) -> Bool {
        
        if(message == ""){
            presentAlertWithTitle(title: Constants.errorMessage.MessageEmpty.rawValue , message: "", vc: self)
            return false
        }
        
        return true
    }
    
    func validateMobileNumber(_ mobileNumber : String) -> Bool {
        
        if(mobileNumber == ""){
            presentAlertWithTitle(title: Constants.errorMessage.MobileNoEmpty.rawValue , message: "", vc: self)
            return false
        }
        
        if(mobileNumber.count < 9 || mobileNumber.count > 10  ){
            presentAlertWithTitle(title: Constants.errorMessage.MobileNoInvalid.rawValue , message: "", vc: self)
            return false
        }
        
        
        
        return true
    }
    
    func validatePassword(_ password : String) -> Bool {
        if(password == ""){
            presentAlertWithTitle(title: Constants.errorMessage.PasswordEmpty.rawValue , message: "", vc: self)
            return false
        }
        return true
    }
    
    func validateNewPassword(_ password : String) -> Bool {
        if(password == ""){
            presentAlertWithTitle(title: Constants.errorMessage.NewPasswordEmpty.rawValue , message: "", vc: self)
            return false
        }
        return true
    }
    func validateConfirmPassword(_ password : String) -> Bool {
        if(password == ""){
            presentAlertWithTitle(title: Constants.errorMessage.ConfirmPasswordEmpty.rawValue , message: "", vc: self)
            return false
        }
        return true
    }
    
    func validatePlaceName(_ placeName : String) -> Bool {
        if(placeName == ""){
            presentAlertWithTitle(title: Constants.errorMessage.PlaceNameEmpty.rawValue , message: "", vc: self)
            return false
        }
        return true
    }
    func validatePlaceAddress(_ placeAddress : String) -> Bool {
        if(placeAddress == ""){
            presentAlertWithTitle(title: Constants.errorMessage.PlaceAddressEmpty.rawValue , message: "", vc: self)
            return false
        }
        return true
    }
    
    
    func presentAlertWithTitle(title : String, message : String, vc : UIViewController){

        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)

        let okButton = UIAlertAction(title: "Ok", style: .default) { (_) in

        }
        alertController.addAction(okButton)
        vc.present(alertController, animated: true, completion: nil)
    }
    
    
    
}
