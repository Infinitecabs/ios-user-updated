//
//  global.swift
//  SocialHub
//
//  Created by Mac on 12/5/19.
//  Copyright © 2019 Mac. All rights reserved.
//

import Foundation


class global : NSObject
{
    static let shared = global()
    
    
        
    // is user logged in or not
    static var isUserLogin: Bool {
        set {
            UserDefaults.standard.set(newValue, forKey: "isUserLogin")
        }
        get {
            return UserDefaults.standard.bool(forKey: "isUserLogin")
        }
    }
    
    
    
    
    // deviceToken
     static var deviceToken: String {
          set {
              UserDefaults.standard.set(newValue, forKey: "deviceToken")
          }
          get {
           return UserDefaults.standard.string(forKey: "deviceToken") ?? ""
          }
      }
    
    static var isUserStartTrip: Bool {
           set {
               UserDefaults.standard.set(newValue, forKey: "isUserStartTrip")
           }
           get {
               return UserDefaults.standard.bool(forKey: "isUserStartTrip")
           }
       }
    
    
    
    // user id
    static var userId: String {
        set {
            UserDefaults.standard.set(newValue, forKey: "userId")
        }
        get {
        return UserDefaults.standard.string(forKey: "userId") ?? ""
        }
    }
    
    // user first name
    static var isNotificationIsOff: Bool {
        set {
            UserDefaults.standard.set(newValue, forKey: "isNotificationIsOff")
        }
        get {
         return UserDefaults.standard.bool(forKey: "isNotificationIsOff")
        }
    }
    
    
    // user first name
    static var userFirstName: String {
        set {
            UserDefaults.standard.set(newValue, forKey: "userFirstName")
        }
        get {
         return UserDefaults.standard.string(forKey: "userFirstName") ?? ""
        }
    }
    
    // user last name
    static var userLastName: String {
        set {
            UserDefaults.standard.set(newValue, forKey: "userLastName")
        }
        get {
         return UserDefaults.standard.string(forKey: "userLastName") ?? ""
        }
    }
    
    // user notification
    static var userNotification: String {
        set {
            UserDefaults.standard.set(newValue, forKey: "userNotification")
        }
        get {
         return UserDefaults.standard.string(forKey: "userNotification") ?? "1"
        }
    }
    
    // user userMobileNumber Country code
    static var userCountryCode: String {
        set {
            UserDefaults.standard.set(newValue, forKey: "userCountryCode")
        }
        get {
         return UserDefaults.standard.string(forKey: "userCountryCode") ?? ""
        }
    }
    
    // user userMobileNumber
    static var userMobileNumber: String {
        set {
            UserDefaults.standard.set(newValue, forKey: "userMobileNumber")
        }
        get {
         return UserDefaults.standard.string(forKey: "userMobileNumber") ?? ""
        }
    }
    
    // user userProfileImage
    static var userProfileImage: String {
        set {
            UserDefaults.standard.set(newValue, forKey: "userProfileImage")
        }
        get {
         return UserDefaults.standard.string(forKey: "userProfileImage") ?? ""
        }
    }
    
    
    // user userProfileImage
    static var userEmail: String {
        set {
            UserDefaults.standard.set(newValue, forKey: "userEmail")
        }
        get {
         return UserDefaults.standard.string(forKey: "userEmail") ?? ""
        }
    }
    
    
    
    func saveDataInUserDefult(dict:  NSDictionary, key: String)
    {
        
        let encodedData = NSKeyedArchiver.archivedData(withRootObject: dict)
        UserDefaults.standard.set(encodedData, forKey: key)
        let userId = "\(dict[ApiKey.userId]!)"
        UserDefaults.standard.set(userId, forKey: ApiKey.userId)
        UserDefaults.standard.synchronize()
    }
    
    func getUserDataInInUserDefult(key: String) -> NSDictionary
    {
        if(UserDefaults.standard.object(forKey: key) != nil)
        {
            guard let data = UserDefaults.standard.object(forKey: key) as? Data else { return NSDictionary() }
            let dict =  NSKeyedUnarchiver.unarchiveObject(with: data) as! NSDictionary
            
            //let dict = UserDefaults.standard.object(forKey: key) as! NSDictionary
            return dict
        }
        return NSDictionary()
        
    }
    
    func getUserDataValueByKey(strKey: String) -> String
    {
        if(UserDefaults.standard.object(forKey: strKey) != nil)
        {
            let value =
                "\(UserDefaults.standard.object(forKey: strKey)!)"
            return value
        }
        return ""
    }
 
    func removeNullFromDict (dict : NSMutableDictionary) -> NSMutableDictionary
    {
        let dic = dict;

        for (key, value) in dict {

            let val : NSObject = value as! NSObject;
            if(val.isEqual(NSNull()))
            {
                dic.setValue("", forKey: (key as? String)!)
            }
            else
            {
                dic.setValue(value, forKey: key as! String)
            }

        }

        return dic;
    }
    
    func removeDataFromUserDefult()
    {
        UserDefaults.standard.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
        UserDefaults.standard.synchronize()
    }
}
