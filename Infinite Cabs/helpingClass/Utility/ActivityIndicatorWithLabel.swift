//
//  ActivityIndicatorWithLabel.swift
//  Infinite Cabs
//
//  Created by Apple on 04/06/20.
//  Copyright © 2020 micro. All rights reserved.
//

import Foundation
import UIKit

public class ActivityIndicatorWithLabel {
    
    var containerView = UIView()
    var progressView = UIView()
    var activityIndicator = UIActivityIndicatorView()

    
    public class var shared: ActivityIndicatorWithLabel {
        struct Static {
            static let instance: ActivityIndicatorWithLabel = ActivityIndicatorWithLabel()
        }
        return Static.instance
    }
    
    var pinchImageView = UIImageView()
    
    public func showProgressView(uiView: UIView) {
        containerView.frame = CGRect(x: 0, y: 0, width: uiView.frame.width, height: uiView.frame.height)
//        containerView.backgroundColor = UIColorFromHex(rgbValue: 0xffffff, alpha: 0.80)
        containerView.backgroundColor = UIColor(white: 0, alpha: 0.7)
        let imageData = NSData(contentsOf: Bundle.main.url(forResource: "car-middle1", withExtension: "gif")!)
        let animatedImage = UIImage.gif(data: imageData! as Data)//UIImage.gifWithData(imageData!)
         pinchImageView = UIImageView(image: animatedImage)
        pinchImageView.frame = CGRect(x: 22.0, y: 22.0, width: 80, height: 80)
//        pinchImageView.center = CGPoint(x: <#T##CGFloat#>, y: <#T##CGFloat#>)
        progressView.frame = CGRect(x: 0, y: 0, width:124, height: 124)//CGRectMake(0, 0, (pinchImageView.frame.size.width), 60)
        progressView.center = uiView.center
        progressView.layer.cornerRadius = 8
        progressView.clipsToBounds = true
        progressView.backgroundColor = .white
        progressView.addSubview(pinchImageView)
        containerView.addSubview(progressView)
        uiView.addSubview(containerView)
    }
    
    public func hideProgressView() {
        activityIndicator.stopAnimating()
        pinchImageView.removeFromSuperview()
        activityIndicator.removeFromSuperview()
        progressView.removeFromSuperview()
        containerView.removeFromSuperview()
       
    }
    
    public func UIColorFromHex(rgbValue:UInt32, alpha:Double=1.0)->UIColor {
        let red = CGFloat((rgbValue & 0xFF0000) >> 16)/256.0
        let green = CGFloat((rgbValue & 0xFF00) >> 8)/256.0
        let blue = CGFloat(rgbValue & 0xFF)/256.0
        return UIColor(red:red, green:green, blue:blue, alpha:CGFloat(alpha))
    }
}
