//
//  HelpingMethod.swift
//  SocialHub
//
//  Created by Mac on 12/2/19.
//  Copyright © 2019 Mac. All rights reserved.
//

import Foundation
import UIKit

class HelpingMethod : NSObject
{
    static let shared = HelpingMethod()
    
    func VaildateTxtEmail(strEmail : String) -> Bool
    {
        let REGEX: String
        REGEX = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
        return NSPredicate(format: "SELF MATCHES %@", REGEX).evaluate(with: strEmail)
    }
    
    
    func presentAlertWithTitle(title : String, message : String, vc : UIViewController)
    {
        DispatchQueue.main.async {
            
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)

        let okButton = UIAlertAction(title: "Ok", style: .default) { (_) in

        }
        alertController.addAction(okButton)
        vc.present(alertController, animated: true, completion: nil)
        }
    }
    
}
