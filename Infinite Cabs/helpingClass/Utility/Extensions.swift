//
//  Extensions.swift
//  Infinite Cabs
//
//  Created by micro on 17/10/19.
//  Copyright © 2019 micro. All rights reserved.
//

import Foundation
import UIKit

// MARK:- Set Bottom line in Textfield
extension UITextField {
    
    
    //Set Border in bottom
    
    func setBottomBorder(borderColor: CGColor = UIColor.black.cgColor,backgroundColor: CGColor = UIColor.white.cgColor)
    {
        
        
        DispatchQueue.main.async {
            
            self.borderStyle = .none
            self.layer.backgroundColor = backgroundColor
            
            let border = CALayer()
            let width = CGFloat(2.0)
            border.borderColor = borderColor
            border.frame = CGRect(x: 0, y: self.frame.size.height - width, width:  self.frame.size.width, height: self.frame.size.height)
            border.borderWidth = width
            self.layer.addSublayer(border)
            self.layer.masksToBounds = true
        }
        
    }
    
    
    
    
    
    // Set image in RightView
    func setRightViewImage(_ imgStr:String){
        
        DispatchQueue.main.async {
            let imageView = UIImageView(frame: CGRect(x: 5, y: 2, width: 40, height: 40))
            imageView.image = UIImage(named: imgStr)
            self.rightView = imageView
            self.rightViewMode = .always
        }
        
    }
    
    // Set image in LeftView
    func setLeftViewImage(_ imgStr:String){
        
        DispatchQueue.main.async {
            let leftVw = UIView(frame: CGRect(x: 0, y: 0, width:40, height: 40))
            let imageView = UIImageView(frame: CGRect(x: 2, y: 2, width: 30, height: 30))
            imageView.image = UIImage(named: imgStr)
            leftVw.addSubview(imageView)
            self.leftView = leftVw
            self.leftViewMode = .always
        }
        
    }
    
}


// MARK:- UIApplication

extension UIApplication {
    
    var statusBarUIView: UIView? {
        if #available(iOS 13.0, *) {
            let tag = 38482
            let keyWindow = UIApplication.shared.windows.filter {$0.isKeyWindow}.first
            
            if let statusBar = keyWindow?.viewWithTag(tag) {
                return statusBar
            } else {
                guard let statusBarFrame = keyWindow?.windowScene?.statusBarManager?.statusBarFrame else { return nil }
                let statusBarView = UIView(frame: statusBarFrame)
                statusBarView.tag = tag
                keyWindow?.addSubview(statusBarView)
                return statusBarView
            }
        } else if responds(to: Selector(("statusBar"))) {
            return value(forKey: "statusBar") as? UIView
        } else {
            return nil
        }
    }
    
}


// MARK:-Extension for Shadow

extension UIButton{
    
    func setShadow(){
        self.layer.shadowColor = UIColor.gray.cgColor
        self.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
        self.layer.shadowRadius = 4
        self.layer.shadowOpacity = 0.5
    }
    
    
}

// MARK:- Border Color

extension UIView{
    
    func borderColorWidth(clr:CGColor,wdth:CGFloat = 1,cornerRadius:CGFloat = 4){
        
        self.layer.borderColor = clr
        self.layer.borderWidth = wdth
        self.layer.cornerRadius = cornerRadius
        
    }
    
}

//MARK:- Add SubView and Remove Subview

public extension UIViewController {
    
    /// Adds child view controller to the parent.
    ///
    /// - Parameter child: Child view controller.
    func addChld(_ child: UIViewController) {
        addChild(child)
        view.addSubview(child.view)
        child.didMove(toParent: self)
    }
    
    /// It removes the child view controller from the parent.
    func removeChld() {
        guard parent != nil else {
            return
        }
        willMove(toParent: nil)
        removeFromParent()
        view.removeFromSuperview()
    }
}


//MARK:- Gradients Color

extension UIView {
    
    func applyGradient(colours: [UIColor]) -> Void {
        
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.colors = colours.map { $0.cgColor }
        //for left to right
        //        gradient.startPoint = CGPoint(x : 0.0, y : 0.5)
        //        gradient.endPoint = CGPoint(x :1.0, y: 0.5)
        //for top to bottom
        gradient.startPoint = CGPoint(x : 0.5, y : 0.0)
        gradient.endPoint = CGPoint(x :0.5, y: 1.0)
        self.layer.insertSublayer(gradient, at: 0)
        
    }
    
}


extension UIImageView{
    
    func roundImage(){
        
        self.layer.cornerRadius = self.frame.size.height/2
        self.clipsToBounds = true
        
    }
    
}

//MARK:- Add Tool Bar on keyboard
extension UIViewController{
    
    //MARK: Toolbar method
    func methodAddToolbar() -> UIToolbar
    {
        let toolbar = UIToolbar(frame:CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        toolbar.barStyle = .default
        toolbar.items = [
//            UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(clickOnCancelEx)),
            UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil),
            UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(clickOnDoneEx))]
        toolbar.sizeToFit()
        
        return toolbar
        
    }
    
    @objc func clickOnCancelEx()
    {
        self.view.endEditing(true)
    }
    
    @objc func clickOnDoneEx()
    {
        self.view.endEditing(true)
    }
}



// MARK:- Set Bottom line in Textview

extension UITextView {
    func setBottomBorder(borderColor: CGColor = UIColor.black.cgColor,
                         backgroundColor: CGColor = UIColor.white.cgColor) {
        
        self.layer.backgroundColor = backgroundColor
        let border = CALayer()
        let width = CGFloat(2.0)
        border.borderColor = borderColor
        border.frame = CGRect(x: 0, y: self.frame.size.height - width, width:  self.frame.size.width, height: self.frame.size.height)
        border.borderWidth = width
        self.layer.addSublayer(border)
        self.layer.masksToBounds = true
    }
}




// MARK:- viewController

extension UIViewController {
    
    
    // Hide KeyBoard when touch around
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    
    func showToast( message : String, seconds: Double) {
        
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        alert.view.backgroundColor = UIColor.black
        alert.view.alpha = 0.9
        alert.view.layer.cornerRadius = 15
        
        present(alert, animated: true)
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + seconds) {
            alert.dismiss(animated: true)
        }
    }
    
    
    func showAlert(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action=UIAlertAction(title: "OK", style: .cancel, handler: nil)
        alert.addAction(action)
        present(alert, animated: true, completion: nil)
    }
    
    
    
    
    
    
    
    
    //    func showToast( message : String, seconds: Double) {
    //
    //
    //        let attributedString = NSAttributedString(string: message, attributes: [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 16), NSAttributedString.Key.foregroundColor: UIColor.white])
    //
    //        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .alert)
    //        alert.view.backgroundColor = #colorLiteral(red: 0.1294117719, green: 0.2156862766, blue: 0.06666667014, alpha: 1)
    //
    //        alert.view.layer.cornerRadius = 15
    //
    //
    //        alert.setValue(attributedString, forKey: "attributedTitle")
    //
    //
    //
    //        present(alert, animated: true)
    //
    //        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + seconds) {
    //            alert.dismiss(animated: true)
    //        }
    //    }
    
}



extension String{
    
    //MARK:- func to get time string
    func getTimeAndDate(_ strDate:String)->(date:String,time:String){
        
        //        time=calendar.dateComponents([.hour,.minute,.second], from: obj)
        //        //        print("\(time?.hour!):\(time?.minute!):\(time?.second!)")
        //        let strTime = String(time!.hour!) + ":" + String(time!.minute!)
        
        
        
        let dateFormatter : DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy HH:mm"
        guard let fullDate = dateFormatter.date(from: strDate)else{return ("","")}
        
        
        //dateFormatter.dateFormat = "yyyy-MM-dd HH:mm"
        dateFormatter.dateFormat = "dd/MM/yyyy"
        let dateString = dateFormatter.string(from: fullDate)
        
        dateFormatter.dateFormat = "HH:mm"
        let timestring = dateFormatter.string(from: fullDate)
        
        //            dateFormatter.dateFormat = "HH:mm"
        //let date = Date()
        //        let dateString = dateFormatter.string(from: obj)
        print(dateString)
        print(timestring)
        
        return(dateString,timestring)
    }
    
}


//MARK:- Change Status Bar Color

extension UIViewController{
    
    func changeStatusBarColor(){
        
        if #available(iOS 13.0, *) {
            let app = UIApplication.shared
            let statusBarHeight: CGFloat = app.statusBarFrame.size.height
            
            let statusbarView = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: statusBarHeight))
            statusbarView.backgroundColor = UIColor(named: "ThemeColor")
            view.addSubview(statusbarView)
            
        } else {
            let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
            statusBar?.backgroundColor = UIColor(named: "ThemeColor")
        }
    }
    
}




extension UIViewController {
    
    func showToastNew(message : String, font: UIFont = UIFont.systemFont(ofSize: 14)) {
        
        let toastLabel = UILabel(frame: CGRect(x: self.view.frame.size.width/2 - 100, y: self.view.frame.size.height-100, width: 200, height: 40))
        toastLabel.backgroundColor = .red//UIColor.black.withAlphaComponent(0.6)
        toastLabel.textColor = UIColor.white
        toastLabel.font = font
        toastLabel.textAlignment = .center
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10
        toastLabel.numberOfLines = 0
        toastLabel.clipsToBounds  =  true
        toastLabel.sizeToFit()
        toastLabel.frame = CGRect( x: toastLabel.frame.minX, y: toastLabel.frame.minY,width:   toastLabel.frame.width + 20, height: toastLabel.frame.height + 8)
        toastLabel.center.x = self.view.center.x//        toastLabel.center = self.view.center
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 4.0, delay: 2, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
    
}
