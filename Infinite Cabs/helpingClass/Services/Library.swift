//
//  Library.swift
//  TaskbeGas
//
//  Created by Savita Kunjan Sharma  on 6/4/18.
//  Copyright © 2018 Orion. All rights reserved.
//

import UIKit
import Alamofire

class ApiLibrary: NSObject {
    
    static let shared = ApiLibrary()
    
    // MARK:- Common API Method
    
    
    func APICalling(postDictionary: [String: Any]?, strApiUrl: String, in vc:UIViewController, showHud hud: Bool = true, completion: @escaping (_ responce : AnyObject?,_ message : String,_ status : Bool) -> ()) {
        
        do{
            print("\(Constants.AppLinks.API_BASE_URL)\(strApiUrl)")
            Alamofire.request("\(Constants.AppLinks.API_BASE_URL)\(strApiUrl)", method: .post, parameters:postDictionary, encoding: URLEncoding.init(destination: .httpBody), headers: nil).responseJSON { (response) in
                
                print(response)
                
//                switch(response.result) {
//
//                case .success(_):
//
//                    if response.result.value != nil
//                    {
//                        let JSON = response.result.value! as AnyObject
//
//                        let responce = JSON as! NSDictionary
//
//                        let sucess = responce.value(forKey: "status") as? String
//
//                        let message = responce.value(forKey: "message") as? String ?? ""
//
//                        if sucess == "200"
//                        {
//                            let data = responce.value(forKey: "data") as? AnyObject
//
//                            completion(data, message, true)
//                        }
//                        else
//                        {
//
//                            completion(nil, message, false)
//                            self.presentAlertWithTitle(title: "", message: "", vc: vc)
//                        }
//
//
//                    }
//
//                    break
//
//                case .failure(_):
//                    
//                    print(response.result.error ?? "Fail")
//                    completion(nil, (response.result.error?.localizedDescription)!, false)
//                    self.presentAlertWithTitle(title: "", message: (response.result.error?.localizedDescription)!, vc: vc)
//                    break
//
//                }
                
                
            }
            
            
            
        }
        
    }
    
    func APICallingWithParameters(postDictionary: [String: Any]?, strApiUrl: String, in vc:UIViewController, showHud hud: Bool = true, completion: @escaping (_ responce : AnyObject?,_ message : String,_ status : Bool) -> ()) {

        do{

            print("\(Constants.AppLinks.API_BASE_URL)\(strApiUrl)")
            Alamofire.request("\(Constants.AppLinks.API_BASE_URL)\(strApiUrl)", method: .post, parameters:postDictionary, encoding: URLEncoding.init(destination: .httpBody), headers: nil).responseJSON { (response) in

                print(response)

                switch(response.result) {

                case .success(_):

                    if response.result.value != nil
                    {
                        let JSON = response.result.value! as AnyObject

                        let responce = JSON as! NSDictionary

                        completion(responce, "", true)

                    }

                    break

                case .failure(_):
               //     CommonFile.shared.progressBarVisibility(false)
                    print(response.result.error ?? "Fail")
                    completion(nil, (response.result.error?.localizedDescription)!, false)
                    self.presentAlertWithTitle(title: "", message: (response.result.error?.localizedDescription)!, vc: vc)

                    break

                }
            }
        }
    }

    
    //image upload function
    func imageUploadKeys(postDictionary: [String: String], strApiUrl: String, image:Data?, imageKey:String, in vc:UIViewController, showHud hud: Bool?, completion: @escaping (_ responce : AnyObject?,_ message : String,_ status : Bool, _ statusCode : String, _ number:String) -> ()) {


        let urlString = "\(Constants.AppLinks.API_BASE_URL)\(strApiUrl)"

        let url = try! URLRequest(url: URL(string:urlString)!, method: .post, headers: nil)

        Alamofire.upload(
            multipartFormData: { multipartFormData in
                for (key, value) in postDictionary {
                    guard let data = value.data(using: .utf8) else { continue }
                    multipartFormData.append(data, withName: key)
                }
                let date = Date()
                let formatter = DateFormatter()
                formatter.dateFormat = "ddyyyy_hhmmss"
                let fileName = "profilePic".appending(formatter.string(from: date))
                if image != nil {
                    multipartFormData.append(image!, withName: imageKey, fileName: "\(fileName)_.png", mimeType: "image/png")
                }

        }, with: url,
           encodingCompletion: { encodingResult in

            switch encodingResult {
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    print(response)
                    if let json = response.result.value as? [String: Any] {
                        let status = "\(json["statusCode"]!)"
                        let message = "\(json["message"]!)"
                        if status == "200"{
                            let data = json["data"] as? NSDictionary
                            completion(data, message, true, status, "")

                        }else if status == "201"  {
                            let phone = "\(json["phone"]!)"
                            let data = json["data"] as? NSDictionary
                            completion(data, message, true, status, phone)

                        } else
                        {
                            print("Fails")
                            //self.presentAlertWithTitle(title: "save", message: message, vc: vc)

                            completion(nil, message, false, "", "")

                            self.presentAlertWithTitle(title: "", message: message, vc: vc)

                        }

                    }
                    
                }
            
            case .failure(_):
                
                completion(nil, "Error", false, "", "")
                self.presentAlertWithTitle(title:"", message: "Error", vc: vc)
                break

            }
        }
        )

    }

    func ApiUploadParametterWithMultipleimages(postDictionary: [String: String], strApiUrl: String, images:[Data], imageKey:String, in vc:UIViewController, showHud hud: Bool?, completion: @escaping (_ responce : AnyObject?,_ message : String,_ status : Bool) -> ()) {


        let urlString = "\(Constants.AppLinks.API_BASE_URL)\(strApiUrl)"

        let url = try! URLRequest(url: URL(string:urlString)!, method: .post, headers: nil)

        Alamofire.upload(
            multipartFormData: { multipartFormData in
                for (key, value) in postDictionary {
                    guard let data = value.data(using: .utf8) else { continue }
                    multipartFormData.append(data, withName: key)
                }
                let date = Date()
                let formatter = DateFormatter()
                formatter.dateFormat = "ddyyyy_hhmmss"
                let fileName = "profilePic".appending(formatter.string(from: date))
                
                for img in images {

                    multipartFormData.append(img, withName: imageKey, fileName: "\(fileName)_.png", mimeType: "image/png")
                    
                }
                

        }, with: url,
           encodingCompletion: { encodingResult in

            switch encodingResult {
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    print(response)
                    if let json = response.result.value as? [String: Any] {
                        let status = "\(json["statusCode"]!)"
                        let message = "\(json["message"]!)"
                        if status == "200"  {
                            let data = json["data"] as? NSDictionary
                            completion(data, message, true)

                        }

                        else
                        {
                            print("Fails")
                            //self.presentAlertWithTitle(title: "save", message: message, vc: vc)

                            completion(nil, message, false)

                            self.presentAlertWithTitle(title: "", message: message, vc: vc)

                        }

                    }
                    
                }
            
            case .failure(_):
                
                completion(nil, "Error", false)
                self.presentAlertWithTitle(title:"", message: "Error", vc: vc)
                break

            }
        }
        )

    }

    func presentAlertWithTitle(title : String, message : String, vc : UIViewController){

        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)

        let okButton = UIAlertAction(title: "Ok", style: .default) { (_) in

        }
        alertController.addAction(okButton)
        vc.present(alertController, animated: true, completion: nil)
    }
    
    
}

