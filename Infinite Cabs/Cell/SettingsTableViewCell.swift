//
//  SettingsTableViewCell.swift
//  Infinite Cabs
//
//  Created by micro on 17/10/19.
//  Copyright © 2019 micro. All rights reserved.
//

import UIKit

class SettingsTableViewCell: UITableViewCell {
    
    //Outlets of Cell
    
    @IBOutlet weak var lblOption: UILabel!
    @IBOutlet weak var btnSwitch: UISwitch!
    @IBOutlet weak var appVersionLabel: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
