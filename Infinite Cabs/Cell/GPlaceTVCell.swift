//
//  GPlaceTVCell.swift
//  Infinite Cabs
//
//  Created by Apple on 10/11/20.
//  Copyright © 2020 micro. All rights reserved.
//

import UIKit

class GPlaceTVCell: UITableViewCell {

    @IBOutlet weak var placeNameLabel: UILabel!
    @IBOutlet weak var placeDetailLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
