//
//  CompleteRidesTableViewCell.swift
//  Infinite Cabs
//
//  Created by micro on 18/10/19.
//  Copyright © 2019 micro. All rights reserved.
//

import UIKit

class CompleteRidesTableViewCell: UITableViewCell {
    
    //Outlets Head
    
    @IBOutlet weak var lblHeadName: UILabel!
    @IBOutlet weak var lblDateHead: UILabel!
    @IBOutlet weak var lblHeadPrice: UILabel!
    
    
    
    
    
    //Outlets Cell
    
    @IBOutlet weak var lblCellDate: UILabel!
    @IBOutlet weak var lblCarType: UILabel!
    @IBOutlet weak var lblPriceCell: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
