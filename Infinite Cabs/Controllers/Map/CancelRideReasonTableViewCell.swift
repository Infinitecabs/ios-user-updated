//
//  CancelRideReasonTableViewCell.swift
//  Infinite Cabs
//
//  Created by Mac on 4/2/20.
//  Copyright © 2020 micro. All rights reserved.
//

import UIKit

class CancelRideReasonTableViewCell: UITableViewCell {

    @IBOutlet weak var reasonLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
