//
//  GooglePlaceSearchTableViewCell.swift
//  Infinite Cabs
//
//  Created by Mac on 3/2/20.
//  Copyright © 2020 micro. All rights reserved.
//

import UIKit

class GooglePlaceSearchTableViewCell: UITableViewCell {

    @IBOutlet weak var placeNameLabel: UILabel!
    @IBOutlet weak var placeDetailLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
