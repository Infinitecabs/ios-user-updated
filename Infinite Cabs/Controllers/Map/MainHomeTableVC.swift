//
//  MainHomeTableVC.swift
//  Infinite Cabs
//
//  Created by Apple on 03/01/20.
//  Copyright © 2020 micro. All rights reserved.
//

import UIKit
import SwiftyJSON
import SDWebImage


class MainHomeTableVC: UIViewController {
    
    @IBOutlet weak var tbl: UITableView!
    
    static var isNextAvailable = true
    static var isDriverDetailShow = false
    static var isTimeMoneyCellShow = false
    
    var mapView:UIView!
    
    static var availableCarsForeRide = NSArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialsetUp()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        if(MapViewController.selectedSourceLocation.latitude != 0 &&  MapViewController.selectedDestinationLocation.latitude != 0){
            
            if(MapViewController.selectedCarIndex == -1){
                showAvailableCarsForeRideAPI()
            }
            
        }
    }
    
    
    
    //initial SetUp
    func initialsetUp(){
        
        //tbl.register(UINib(nibName: "EnterLocationTVCell", bundle: nil), forCellReuseIdentifier: "EnterLocationTVCell")
        tbl.register(UINib(nibName: "NextAvailableTVCell", bundle: nil), forCellReuseIdentifier: "NextAvailableTVCell")
        tbl.register(UINib(nibName: "CarDetailTVCell", bundle: nil), forCellReuseIdentifier: "CarDetailTVCell")
        tbl.register(UINib(nibName: "DriverPickDetailTVCell", bundle: nil), forCellReuseIdentifier: "DriverPickDetailTVCell")
        tbl.register(UINib(nibName: "TimeMoneyTVCell", bundle: nil), forCellReuseIdentifier: "TimeMoneyTVCell")
        
        
        //Notification Observer
        NotificationCenter.default.addObserver(self, selector: #selector(showDriverDetail), name: Notification.Name(rawValue: "ShowDriverDetail"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(btnReadyClicked), name: Notification.Name(rawValue: "ReadyClicked"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(cancelButtonTappedNotificationTask), name: Notification.Name(rawValue: "CancelButtonClicked"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(reloadTable), name: Notification.Name(rawValue: "ReloadTabel"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(scrollTableToTop), name: Notification.Name(rawValue: "scrollToTop"), object: nil)
        //ReloadTabel
    }
    
    
    
    
    //MARK:- ACTION
    
    
    @objc func scrollTableToTop(){
        tbl.scrollRectToVisible(CGRect(x: 0, y: 0, width: 1, height: 1), animated: true)
    }
    
    //Head Clicked
    @objc func btnHeadClicked(_ sender:UIButton){
        
        if(MapViewController.selectedSourceLocation.latitude != 0 &&  MapViewController.selectedDestinationLocation.latitude != 0){
            
            NotificationCenter.default.post(Notification(name: Notification.Name("Head Clicked")))
        }
        else{
            showToast(message: "First Select Pickup And Destination location", seconds: 2)
        }
      //  NotificationCenter.default.post(Notification(name: Notification.Name("Head Clicked")))
    }
    
    //Ready button Clicked
    @objc func btnReadyClicked(_ sender:UIButton){
        
        MainHomeTableVC.self.isTimeMoneyCellShow = true
        MainHomeTableVC.self.isNextAvailable = false
        MainHomeTableVC.self.isDriverDetailShow = false
        
        DispatchQueue.main.async {
            self.tbl.reloadData()
        }
    }
    
    @objc func cancelButtonTappedNotificationTask(_ sender:UIButton){
        
        MainHomeTableVC.self.isTimeMoneyCellShow = false
        MainHomeTableVC.self.isNextAvailable = true
        MainHomeTableVC.self.isDriverDetailShow = false
        
        DispatchQueue.main.async {
            self.tbl.reloadData()
        }
    }
    
    
    @objc func reloadTable(){
        DispatchQueue.main.async {
            self.tbl.reloadData()
        }
    }
    
    
    
    //Show Driver Detail
    @objc func showDriverDetail(){
        
        MainHomeTableVC.isNextAvailable = false
        MainHomeTableVC.isDriverDetailShow = true
        MainHomeTableVC.isTimeMoneyCellShow = false
        
        DispatchQueue.main.async {
            self.tbl.reloadData()
        }
    }
    
    
    func chooseCar(){
        NotificationCenter.default.post(Notification(name: Notification.Name("Car Selected")))
    }
   
    
    
    //MARK:- func to get time string
        func getTimeString(_ obj:Date)->String{
            
    //        time=calendar.dateComponents([.hour,.minute,.second], from: obj)
    //        //        print("\(time?.hour!):\(time?.minute!):\(time?.second!)")
    //        let strTime = String(time!.hour!) + ":" + String(time!.minute!)
            
            let dateFormatter : DateFormatter = DateFormatter()
              //dateFormatter.dateFormat = "yyyy-MM-dd HH:mm"
             dateFormatter.dateFormat = "dd/MM/yyyy HH:mm"
            
//            dateFormatter.dateFormat = "HH:mm"
            //let date = Date()
            let dateString = dateFormatter.string(from: obj)
            print(dateString)
            
            return dateString
        }
        
    
    
    

    
    private func showAvailableCarsForeRideAPI(){

    
 //   let dictKeys = [ "user_pickup_latitude" : "26.8545819",  "user_pickup_longlatitude" : "75.7666681" , "user_drop_latitude" : "26.8545819",  "user_drop_longlatitude" : "75.7666681"] as! [String:String]
        
//        (2020-05-01 10:00)
        
        
        let date = Date()
        let strDate = getTimeString(date)
        
        
        ActivityIndicatorWithLabel.shared.showProgressView(uiView:mapView)
        
        let dictKeys = [ "user_pickup_latitude" : MapViewController.selectedSourceLocation.latitude,  "user_pickup_longlatitude" : MapViewController.selectedSourceLocation.longitude , "user_drop_latitude" : MapViewController.selectedDestinationLocation.latitude,  "user_drop_longlatitude" : MapViewController.selectedDestinationLocation.longitude,"current_time":strDate] as [String : Any]

        DispatchQueue.global(qos: .background).async
            {

            ApiLibrary.shared.APICallingWithParameters(postDictionary: dictKeys, strApiUrl: Constants.ApiUrl.serchUserTexiAmount.rawValue, in: self) { (response,status,message) in

//                Hud.shared.hideHud()
           ActivityIndicatorWithLabel.shared.hideProgressView()

                guard  let res = response as? NSDictionary else
                {
                    return
                }

                let statusCode = "\(String(describing: res["statusCode"]!))"
                let strMessage = res["message"] as! String


                if statusCode == "200"
                {
                    //                        self.availabelCarData.removeAllObjects()
                    let availabelData = res["data"] as! NSArray
                    
                    MainHomeTableVC.availableCarsForeRide = NSMutableArray(array: availabelData)
                    
                    DispatchQueue.main.async {
                        self.tbl.reloadData()
                        NotificationCenter.default.post(Notification(name: Notification.Name("Head Clicked")))
                        
                    }
                    
                }
                else{
                    HelpingMethod.shared.presentAlertWithTitle(title: "Error!", message: strMessage, vc: self)
                    //                    self.showToast(message: strMessage, seconds: 2)
                }
            }
        }


    }
    
}


//MARK:- Textfield Delegate
extension MainHomeTableVC:UITextFieldDelegate{
    /*
     func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
     
     let indx = IndexPath(row: 0, section: 0)
     let cell = tbl.cellForRow(at: indx) as! EnterLocationTVCell
     
     if textField == cell.txtPickUpLocation || textField == cell.txtDestination{
     
     let strbd = staticClass.getStoryboard_Map()
     let vc = strbd?.instantiateViewController(withIdentifier: "SelectDestinationVC") as! SelectDestinationVC
     self.navigationController?.pushViewController(vc, animated: true)
     
     }
     
     return false
     }
     */
    
}

//MARK:- TableView Delegate and DataSource
extension MainHomeTableVC:UITableViewDelegate,UITableViewDataSource{
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return MainHomeTableVC.availableCarsForeRide.count
    }
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if(MainHomeTableVC.isNextAvailable == false &&  MainHomeTableVC.isDriverDetailShow == false ){
            return 0
        }
        
        return 80
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if MainHomeTableVC.isNextAvailable{
            return 100
        }else if MainHomeTableVC.isDriverDetailShow{
            return  110
        }else{
            return 160
        }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tbl.dequeueReusableCell(withIdentifier: "CarDetailTVCell") as! CarDetailTVCell
        
        if(MapViewController.selectedCarIndex == indexPath.row){
//            cell.backgroundColor = #colorLiteral(red: 0.9764705882, green: 0.6431372549, blue: 0.1058823529, alpha: 1)
            cell.viewC.backgroundColor = #colorLiteral(red: 0.9764705882, green: 0.6431372549, blue: 0.1058823529, alpha: 1)
//            cell.backgroundColor = .clear
        }
        else{
//            cell.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            cell.viewC.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
//            cell.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            
        }

        let data = MainHomeTableVC.availableCarsForeRide[indexPath.row] as! NSDictionary
        
        cell.carNameLabel.text = data["cartype"] as? String //cartype
        
        let seatCapacity = data["seat_capacity"] as? String ?? "0"
        
        if(seatCapacity == "0"){
            cell.numberOfSeateLabel.text = ""
        }else{
            cell.numberOfSeateLabel.text = "\(seatCapacity) Seater"
        }
        //if MapViewController.isPromoApplied{
           // cell.priceLabel.text =  "\(MapViewController.selectedRideMaxAmount)"
       // }else{
        let minRate : String  = "\(data["min_rate"] ?? "")"
        let maxRate : String = "\(data["max_rate"] ?? "")"
            cell.priceLabel.text =  "$\(minRate)-$\(maxRate)"
       // }
        
        
        
        
        let image = data["carimage"] as! String
        
        
//        var originalString = image
//        var escapedString = originalString.stringByAddingPercentEncodingWithAllowedCharacters(.URLHostAllowedCharacterSet())
//        println("escapedString: \(escapedString)")
        
        
        
        let urlString = image.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
        
        
        if let imageURL = URL(string: urlString) {
            cell.carImageView.sd_setImage(with: imageURL, placeholderImage: UIImage(named: "sedan"), options: SDWebImageOptions.progressiveLoad) { (image, error, sdimagecacheType, url) in
                
            }
        }
        
        
        
//        cell.contentView.layer.cornerRadius = 8
        if indexPath.row == 0{
            cell.layer.cornerRadius = 8
            cell.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
            cell.clipsToBounds = true
        }else{
            cell.layer.cornerRadius = 0
            
        }
        
        
        
        return cell
        
        //}
        
    }
    
    
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if MainHomeTableVC.isNextAvailable{
            
            let cell = tbl.dequeueReusableCell(withIdentifier: "NextAvailableTVCell") as! NextAvailableTVCell
            cell.btnHead.addTarget(self, action: #selector(btnHeadClicked(_:)), for: .touchUpInside)
            
            
            
            if(MapViewController.selectedCarIndex == -1){
//                cell.contentView.backgroundColor  = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
                cell.viewChooseRide.backgroundColor  = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
                cell.lblEstimateFare.isHidden = true
                cell.lblKm.isHidden = true
                cell.lblPassengersAvailabilty.isHidden = true
                
            }
            else{
//                cell.contentView.backgroundColor = #colorLiteral(red: 0.9764705882, green: 0.6431372549, blue: 0.1058823529, alpha: 1)
                cell.viewChooseRide.backgroundColor = #colorLiteral(red: 0.9764705882, green: 0.6431372549, blue: 0.1058823529, alpha: 1)
                cell.lblEstimateFare.isHidden = false
                cell.lblPassengersAvailabilty.isHidden = false
                
                let  data = MainHomeTableVC.availableCarsForeRide[MapViewController.selectedCarIndex] as! NSDictionary
                
                cell.carNameLabel.text = data["cartype"] as? String //cartype
                        
                
                let seatCapacity = data["seat_capacity"] as? String ?? "0"
                if(seatCapacity == "0"){
                    cell.lblPassengersAvailabilty.text = ""
                }else{
                    cell.lblPassengersAvailabilty.text = "\(seatCapacity) Seater"
                }
                
                
                
//                if MapViewController.isPromoApplied{
//                    cell.lblEstimateFare.text =  "\(MapViewController.selectedRideMaxAmount)"
//                }else{
                let minRate : String  = "\(data["min_rate"] ?? "")"
                let maxRate : String = "\(data["max_rate"] ?? "")"
                    cell.lblEstimateFare.text =  "$\(minRate)-$\(maxRate)"
//                }
                        
                
//                cell.lblEstimateFare.text =  "$\(maxRate)"
                cell.lblKm.text = String(data["km"] as? Double ?? 0.0) + " Km"
                let image = data["carimage"] as! String
                        
                let urlString = image.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
                        
                if let imageURL = URL(string: urlString) {
                    cell.imgCarHead.sd_setImage(with: imageURL, placeholderImage: UIImage(named: "sedan"), options: SDWebImageOptions.progressiveLoad) { (image, error, sdimagecacheType, url) in
                                
                    }
                }
            }
            
//            cell.contentView.layer.cornerRadius = 8
            cell.viewChooseRide.layer.cornerRadius = 8
            
            return cell.contentView
            
        }
        else if MainHomeTableVC.isTimeMoneyCellShow{
            
            let cell = tbl.dequeueReusableCell(withIdentifier: "TimeMoneyTVCell") as! TimeMoneyTVCell
            
            if let  data = MainHomeTableVC.availableCarsForeRide[MapViewController.selectedCarIndex] as? NSDictionary{
                
                
                if MapViewController.isPromoApplied{
//                    cell.estPriceLabel.text =  "Estimated Fare :" + "\(MapViewController.selectedRideAmountAfterPromo)"
                       cell.estPriceLabel.text = MapViewController.selectedRideAmountAfterPromo
                    cell.lblPromoCodeApplied.isHidden = false
                    
                }else{
                    let minRate : String  = "\(data["min_rate"] ?? "")"
                    let maxRate : String = "\(data["max_rate"] ?? "")"
                    cell.estPriceLabel.text =  "$\(minRate)-$\(maxRate)"
                    cell.lblPromoCodeApplied.isHidden = true
                }
                
//                cell.estPriceLabel.text =  "Estimated Fare : $\(maxRate)"
            }
             let paymentStatus = UserDefaults.standard.value(forKey: "PAYMENT") as! String
            cell.paymentTypeLabel.text = paymentStatus //MapViewController.paymentType
            cell.dateLabel.text = MapViewController.date
            cell.timeLabel.text = "ETA: " + MapViewController.time
            cell.couponCodeTextField.text = MapViewController.coupanCode
            
            if cell.paymentTypeLabel.text == "Pay the Driver Directly"{
                cell.imgPaymentType.image = UIImage(named: "ic_NDoller")
                cell.btnSelect.setTitle("Change", for: .normal)
            }else if cell.paymentTypeLabel.text == "Pay with the App"{
                cell.imgPaymentType.image = UIImage(named: "PayWithPhone")
                cell.btnSelect.setTitle("Change", for: .normal)
            }else{
                cell.imgPaymentType.image = UIImage(named: "credit-card")
                cell.btnSelect.setTitle("Select", for: .normal)
            }
            
            
            if MapViewController.isPromoApplied{
                cell.btnApply.setTitle("Remove", for: .normal)
                cell.btnApply.setTitleColor(.red, for: .normal)
            }else{
                cell.btnApply.setTitle("Apply", for: .normal)
                cell.btnApply.setTitleColor(.black, for: .normal)
            }
            
            
            return cell
            
            
        }else{
            
            let cell = tbl.dequeueReusableCell(withIdentifier: "DriverPickDetailTVCell") as! DriverPickDetailTVCell
            
            cell.driverNameLabel.text = MapViewController.AppData["driverName"] ?? ""
//            cell.rattingLabel.text = MapViewController.AppData["driverRating"] ?? ""
            cell.viewCosmos.rating = Double(MapViewController.AppData["driverRating"] ?? "0.0")!
            
            cell.carNumberLabel.text = MapViewController.AppData["carNumber"] ?? ""
            cell.carNameLabel.text = MapViewController.AppData["carName"] ?? ""
            cell.timeLabel.text = "ETA: " + (MapViewController.AppData["time"] ?? "")
            
            
            let strimg = Constants.AppLinks.API_BASE_URL_IMG + "driverimages/" + (MapViewController.AppData["deiverImage"] ?? "")
            
            if let encoded = strimg.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed){
                if let imageurl = URL(string: encoded){
                    print(imageurl)
                    cell.driverImageView.sd_setImage(with: imageurl, placeholderImage: UIImage(named: "user_Ic"), options: SDWebImageOptions(rawValue: 0), context: nil)
                }
            }
            
            
            
//            if let imageURL = URL(string: "http://stageofproject.com/texiapp-new/user_image/" + (MapViewController.AppData["deiverImage"] ?? "") ) {
//                cell.driverImageView.sd_setImage(with: imageURL, placeholderImage: UIImage(named: "user_Ic"), options: SDWebImageOptions.progressiveLoad) { (image, error, sdimagecacheType, url) in
//
//                }
//            }
            
            
            let strimg1 = Constants.AppLinks.API_BASE_URL_IMG + "car_image/" + (MapViewController.AppData["carImage"] ?? "")
            
            if let encoded1 = strimg1.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed){
                if let imageurl = URL(string: encoded1){
                    print(imageurl)
                    cell.carImageView.sd_setImage(with: imageurl, placeholderImage: UIImage(named: "sedan"), options: SDWebImageOptions(rawValue: 0), context: nil)
                }
            }
            
            
            
            return cell.contentView
            
        }
        
        
    }
    
    
   
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        //let selectedCell:UITableViewCell = tableView.cellForRow(at: indexPath)!
        //selectedCell.contentView.backgroundColor = UIColor.yellow
        
     //   let selectedCell:UITableViewCell = tableView.cellForRow(at: indexPath)!
     //   selectedCell.contentView.backgroundColor = UIColor.clear
        
        MapViewController.selectedCarIndex = indexPath.row
        
        let data = MainHomeTableVC.availableCarsForeRide[indexPath.row] as! NSDictionary
        
        if let cabId : String = data["cab_type_id"] as? String{
            MapViewController.AppData.updateValue(cabId, forKey: "cabId")
        }
        
        if let maxAmmount = data["max_rate"]{
            MapViewController.selectedRideMaxAmount = "\(maxAmmount)"
        }
        
        if let maxAmmount = data["min_rate"]{
            MapViewController.selectedRideMinAmount = "\(maxAmmount)"
        }
        
        tbl.reloadData()
        chooseCar()
    }
    
    
}
