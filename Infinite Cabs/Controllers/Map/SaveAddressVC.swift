//
//  SaveAddressVC.swift
//  Infinite Cabs
//
//  Created by Apple on 17/11/19.
//  Copyright © 2019 micro. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class SaveAddressVC: UIViewController,UITextFieldDelegate {
    
    
    @IBOutlet weak var viewHome: UIView!
    @IBOutlet weak var viewWork: UIView!
    @IBOutlet weak var viewOther: UIView!
    
    @IBOutlet weak var txtPlaceName: UITextField!
    @IBOutlet weak var txtAddress: UITextField!
    @IBOutlet weak var btnPickLocation: UIButton!
    
    var placePostion = "Home"
    var placeLat = 0.0
    var placeLng = 0.0
    
    @IBOutlet weak var btnSavePlaces: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialSetUp()
        hideKeyboardWhenTappedAround()
        // Do any additional setup after loading the view.
    }
    
    
    
    //initial ui set up
    func initialSetUp(){
        
        btnSavePlaces.layer.cornerRadius = 4
        btnPickLocation.layer.cornerRadius = 4
        
        txtAddress.delegate = self
        txtPlaceName.delegate = self
        
        btnPickLocation.setShadow()
        
        viewHome.borderColorWidth(clr: staticClass.shared.themeColor)
        viewWork.borderColorWidth(clr: UIColor.lightGray.cgColor)
        viewOther.borderColorWidth(clr: UIColor.lightGray.cgColor)
        
        let gestureHome = UITapGestureRecognizer(target: self, action: #selector(homeGesture(_:)))
        viewHome.addGestureRecognizer(gestureHome)
        
        let gestureWork = UITapGestureRecognizer(target: self, action: #selector(workGesture(_:)))
        viewWork.addGestureRecognizer(gestureWork)
        
        let gestureOther = UITapGestureRecognizer(target: self, action: #selector(otherGesture(_:)))
        viewOther.addGestureRecognizer(gestureOther)
        
        txtPlaceName.text = "Home"
    }
    
    
    
    
    
    //MARK:- ACTION
    
    @IBAction func btnPickLocationClicked(_ sender: UIButton) {
        
        txtAddress.text = MapViewController.sourceLocationNameTemp
        self.placeLat = MapViewController.sourceLocation.latitude
        self.placeLng = MapViewController.sourceLocation.longitude
        
    }
    
    
    @IBAction func btnSavePlacesClicked(_ sender: UIButton) {
        
        if(validatePlaceName(txtPlaceName.text ?? "") && validatePlaceAddress(txtAddress.text ?? "")){
            
//            updateAddressOnLocalDataBase()
            API_addUserLocation()
            
        }
    }
    
    
    @IBAction func btnPlaceAddressTapped(_ sender: UIButton) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "GPlaceSearchVC") as! GPlaceSearchVC
        vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    
    
    
    
    func showMapVc(){
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1.5) {
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: MapViewController.self) {
                    _ =  self.navigationController!.popToViewController(controller, animated: true)
                    break
                }
            }
        }
    }
    
    private func updateAddressOnLocalDataBase(){
        
            placePostion   =   txtPlaceName.text ?? ""
        let placeAddress   =   txtAddress.text ?? ""
        
        
        
        var address = [String] ()
        
        address.append(placePostion)
        address.append(placeAddress)
        address.append("\(self.placeLat)")
        address.append("\(self.placeLng)")
        
        
        let defaults = UserDefaults.standard
        defaults.set(address, forKey: placePostion)
        
        var addressKeys = UserDefaults.standard.stringArray(forKey: "PlaceName")
        if addressKeys == nil{
            addressKeys = [String]()
            addressKeys!.append(placePostion)
        }else{
            addressKeys!.append(placePostion)
        }
        
        defaults.set(addressKeys, forKey: "PlaceName")
        
        self.showToast(message: "Add Address Successfully", seconds: 3)
        self.showMapVc()
        
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    
    
    @IBAction func btnBackClicked(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    @objc func homeGesture(_ sender:UITapGestureRecognizer){
        placePostion = "Home"
        txtPlaceName.text = "Home"
        viewHome.borderColorWidth(clr: staticClass.shared.themeColor)
        viewWork.borderColorWidth(clr: UIColor.lightGray.cgColor)
        viewOther.borderColorWidth(clr: UIColor.lightGray.cgColor)
        
    }
    @objc func workGesture(_ sender:UITapGestureRecognizer){
        placePostion = "Work"
        txtPlaceName.text = "Work"
        viewHome.borderColorWidth(clr: UIColor.lightGray.cgColor)
        viewWork.borderColorWidth(clr: staticClass.shared.themeColor)
        viewOther.borderColorWidth(clr: UIColor.lightGray.cgColor)
        
    }
    @objc func otherGesture(_ sender:UITapGestureRecognizer){
        placePostion = "Other"
        txtPlaceName.text = "Other"
        viewHome.borderColorWidth(clr: UIColor.lightGray.cgColor)
        viewWork.borderColorWidth(clr: UIColor.lightGray.cgColor)
        viewOther.borderColorWidth(clr: staticClass.shared.themeColor)
        
    }
    
    
    
    
    private func API_addUserLocation(){
        
        
        ActivityIndicatorWithLabel.shared.showProgressView(uiView: self.view)
        
        
        
        let userID = global.userId
        let dictKeys = [ "userId" : userID, "locations" : self.txtAddress.text!, "type" : txtPlaceName.text!, "lat":self.placeLat, "lng":self.placeLng] as [String : Any]
        
        
        let url = Constants.AppLinks.API_BASE_URL + Constants.ApiUrl.AddUserLocation.rawValue
        
        Alamofire.request(url , method: .post, parameters: dictKeys, headers:nil).responseJSON(completionHandler: { (response) in
            
            //            Hud.shared.hideHud()
            ActivityIndicatorWithLabel.shared.hideProgressView()
            do {
                let json: JSON = try JSON(data: response.data!)
                if (json["statusCode"].stringValue == "200"){
                    
                    self.showToast(message: "Add Address Successfully", seconds: 3)
                }
                else{
                    // error
                    self.showAlert(title: "Error", message: json["message"].stringValue)
                }
                
            } catch {
                // show a alert
                //                    Hud.shared.hideHud()
                ActivityIndicatorWithLabel.shared.hideProgressView()
                self.showAlert(title: "Error", message: "Please check network connection")
            }
        })
    }
    
}


extension SaveAddressVC:GooglePlacesDelegate{
    
    func googlePlaces(mainText: String, secondaryText: String, lat: Double, lng: Double) {
        self.txtAddress.text = mainText + " " + secondaryText
        self.placeLat = lat
        self.placeLng = lng
    }
    
}
