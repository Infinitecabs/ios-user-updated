//
//  PaymentViewController.swift
//  Infinite Cabs
//
//  Created by Mac on 4/2/20.
//  Copyright © 2020 micro. All rights reserved.
//

import UIKit
import WebKit
import Alamofire
import SwiftyJSON

class PaymentViewController: UIViewController , WKUIDelegate , WKNavigationDelegate {

    @IBOutlet weak var webView: WKWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        webView.uiDelegate = self
        webView.navigationDelegate = self
        
        CheckRequestuserAPI()
    }
    
    @IBAction func backButtonTapped(_ sender: Any) {
        
        // this notification call CheckRequestuserAPI on mapview controller and manage payment
        NotificationCenter.default.post(Notification(name: Notification.Name("paymentSuccess")))
        
        self.navigationController?.popViewController(animated: true)
    }
    
    
   private func CheckRequestuserAPI(){
             //userCancelRide
//             Hud.shared.showHud()
    ActivityIndicatorWithLabel.shared.showProgressView(uiView: self.view)
          
             let userID = global.userId
             let dictKeys = [ "user_id" : userID
                         ] as [String : Any]


                let url = "\(Constants.AppLinks.API_BASE_URL)\(Constants.ApiUrl.checkRequestuser.rawValue)"//Constants.ApiUrl.checkRequestuser.rawValue

                Alamofire.request(url , method: .post, parameters: dictKeys, headers:nil).responseJSON(completionHandler: { (response) in

//                   Hud.shared.hideHud()
                    ActivityIndicatorWithLabel.shared.hideProgressView()
                    do {
                        let json: JSON = try JSON(data: response.data!)

                        if (json["statusCode"].stringValue == "200"){

                          let tempPaymentlink = json["bookingData"]["paymentlink"].stringValue
                         
                            self.openPaymentURL(url: tempPaymentlink)
                        
                        }
                        else if (json["statusCode"].stringValue == "201"){
                          
                        }
                        else {
                            // error
                            self.showAlert(title: "Error", message: json["message"].stringValue)
                            }

                    } catch {
                        // show a alert
//                        Hud.shared.hideHud()
                        ActivityIndicatorWithLabel.shared.hideProgressView()
                        self.showAlert(title: "Error", message: "Please check network connection")
                    }
                })
             
             
             
         }
    
    private func openPaymentURL(url : String){
        
        //open web view
        webView.load(NSURLRequest(url: NSURL(string: url)! as URL) as URLRequest)
        
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Swift.Void){
           
              
              if let urlStr = navigationAction.request.url?.absoluteString{
                print(urlStr)
                
                if(urlStr.contains(Constants.AppLinks.API_BASE_URL + "check/"))
                  {
                      
                    //paymentSuccess
                    NotificationCenter.default.post(Notification(name: Notification.Name("paymentSuccess")))
                    
                    self.showToast(message: "Payment Success", seconds: 3)
                    
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1.5) {
                        self.navigationController?.popViewController(animated: true)
                    }
                  }
              }
              
              decisionHandler(.allow)
        }
          
}
