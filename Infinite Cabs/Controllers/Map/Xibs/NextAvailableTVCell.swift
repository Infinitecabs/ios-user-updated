//
//  NextAvailableTVCell.swift
//  Infinite Cabs
//
//  Created by Apple on 03/01/20.
//  Copyright © 2020 micro. All rights reserved.
//

import UIKit

class NextAvailableTVCell: UITableViewCell {

    
    @IBOutlet weak var imgCarHead: UIImageView!
    @IBOutlet weak var lblPassengersAvailabilty: UILabel!
    @IBOutlet weak var lblEstimateFare: UILabel!
    @IBOutlet weak var btnHead: UIButton!
    @IBOutlet weak var carNameLabel: UILabel!
    @IBOutlet weak var viewChooseRide: UIView!
    @IBOutlet weak var lblKm: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
