//
//  TimeMoneyTVCell.swift
//  Infinite Cabs
//
//  Created by Apple on 03/01/20.
//  Copyright © 2020 micro. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class TimeMoneyTVCell: UITableViewCell , UITextFieldDelegate {

    @IBOutlet weak var estPriceLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var paymentTypeLabel: UILabel!
    
    @IBOutlet weak var couponCodeTextField: UITextField!
    @IBOutlet weak var btnApply: UIButton!
    
    @IBOutlet weak var imgPaymentType: UIImageView!
    
    @IBOutlet weak var lblPromoCodeApplied: UILabel!
    @IBOutlet weak var btnSelect: UIButton!
    
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        couponCodeTextField.delegate = self
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    
    @IBAction func changePaymentTypeButtonTapped(_ sender: Any) {
        
        let dataDict:[String: String] = ["id" : "P"]
        NotificationCenter.default.post(Notification(name: Notification.Name(rawValue: "ShowDateAndTimePiker") , userInfo: dataDict))
    }
    
    @IBAction func timePikerButtonTapped(_ sender: Any) {
        let dataDict:[String: String] = ["id" : "T"]
        NotificationCenter.default.post(Notification(name: Notification.Name(rawValue: "ShowDateAndTimePiker"), userInfo: dataDict))
    }
    
    @IBAction func datePikerButtonTapped(_ sender: Any) {
        let dataDict:[String: String] = ["id" : "D"]
        NotificationCenter.default.post(Notification(name: Notification.Name(rawValue: "ShowDateAndTimePiker"), userInfo: dataDict))
    }
    
    
    @IBAction func applyCouponCodeButtonTapped(_ sender: Any) {
        
        resignFirstResponder()
        let couponcode = couponCodeTextField.text ?? ""
        
        let dataDict:[String: String] = ["couponCode" : couponcode]
        
        NotificationCenter.default.post(Notification(name: Notification.Name(rawValue: "applyCouponCode") , userInfo: dataDict))
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        MapViewController.coupanCode = textField.text!
        couponCodeTextField.resignFirstResponder()
        NotificationCenter.default.post(Notification(name: Notification.Name("ReadyClicked")))
        return true
    }
    
}
