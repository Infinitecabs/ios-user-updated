//
//  SelectDestinationVC.swift
//  Infinite Cabs
//
//  Created by Apple on 03/01/20.
//  Copyright © 2020 micro. All rights reserved.
//

import UIKit
import GooglePlaces
import Alamofire
import SwiftyJSON
import SwiftLocation


class SelectDestinationVC: UIViewController , UITextFieldDelegate  {
    
    
    var placesClient: GMSPlacesClient!
    
    
    @IBOutlet weak var tbl: UITableView!
    
    @IBOutlet weak var txtEnterPickUp: UITextField!
    @IBOutlet weak var txtDestination: UITextField!
    
    @IBOutlet weak var viewTop: UIView!
    
    @IBOutlet weak var btnCrossPickUp: UIButton!
    @IBOutlet weak var btnCrossDrop: UIButton!
    
    
    var addresArray =  [[String]] ()
    var searchedPlaceData = JSON()
    var arrySavedAddress = [JSON]()
    var lastTime = TimeInterval()
    
    var locationManager = CLLocationManager()
    
    
    var isSelectionDestinationLocation = true
    
    //    var strPickUpLocation:String = ""
    //    var strDropLocation:String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.hideKeyboardWhenTappedAround()
        
        txtDestination.delegate = self
        txtEnterPickUp.delegate = self
        
//        getDataFromLocalDataBase()
        
        
        //        txtEnterPickUp.text = strPickUpLocation
        //        txtDestination.text = strDropLocation
        
        MapViewController.selectedSourceLocation = MapViewController.sourceLocation
        MapViewController.selectedSourceLocationName = MapViewController.sourceLocationNameTemp
        txtEnterPickUp.text = MapViewController.sourceLocationNameTemp
        
        
        
        if txtDestination.text == ""{
            btnCrossDrop.isHidden = true
        }
        
        
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        API_getLocationList()
    }
    
    
    func getDataFromLocalDataBase(){
        
        addresArray.removeAll()
        
        let defaults = UserDefaults.standard
        //        let myarray = defaults.stringArray(forKey: "Home") ?? [String]()
        //        let myarray2 = defaults.stringArray(forKey: "Work") ?? [String]()
        //        let myarray3 = defaults.stringArray(forKey: "Other") ?? [String]()
        
        if let addressKeys = UserDefaults.standard.stringArray(forKey: "PlaceName"){
            
            for key in addressKeys{
                
                let myarray = defaults.stringArray(forKey: key) ?? [String]()
                
                if(!myarray.isEmpty){
                    addresArray.append(myarray)
                }
            }
        }
        
        
        //        if(!myarray.isEmpty){
        //            addresArray.append(myarray)
        //        }
        //        if(!myarray2.isEmpty){
        //            addresArray.append(myarray2)
        //        }
        //        if(!myarray3.isEmpty){
        //            addresArray.append(myarray3)
        //        }
        
        self.tbl.reloadData()
    }
    
    
    // Present the Autocomplete view controller when the textField is tapped.
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        
        if(textField == txtDestination){
            isSelectionDestinationLocation = true
            // btnCrossDrop.isHidden = false
        }
        else{
            isSelectionDestinationLocation = false
        }
        
        
        
        if(textField == txtDestination){
            
            if(MapViewController.selectedSourceLocation.latitude == 0 && MapViewController.selectedSourceLocation.longitude == 0){
                // show allert
                showToast(message: "First Select PickUp Location", seconds: 2)
            }
            
        }
        
    }
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField == txtEnterPickUp && textField.text == "" {
            
            MapViewController.selectedSourceLocation.latitude = 0
            MapViewController.selectedSourceLocation.longitude = 0
            
        }else if textField == txtDestination && textField.text == ""{
            //btnCrossDrop.isHidden = true
        }
    }
    
    
    func textFieldDidChangeSelection(_ textField: UITextField) {
        if textField == txtEnterPickUp && textField.text != ""{
            btnCrossPickUp.isSelected = false
        }else if textField == txtEnterPickUp && textField.text == "" {
            btnCrossPickUp.isSelected = true
        }
        
        if textField == txtDestination {
            
            if textField.text != ""{
                btnCrossDrop.isHidden = false
            }else{
                btnCrossDrop.isHidden = true
            }
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        
        lastTime = Date().timeIntervalSince1970
        
        
        let seconds = 1.0
        DispatchQueue.main.asyncAfter(deadline: .now() + seconds) {
            self.getSearchPlaceApiData()
        }
        
        return true
    }
    
    //func getSearchPlaceApiData(place : String){
    
    
    func getSearchPlaceApiData(){
        
        let differenceTime = Date().timeIntervalSince1970 - lastTime
        
        print(differenceTime)
        
        
        if(differenceTime < 1.0){
            return
        }
        
        
        let token = ""
        
        
        //        var para = [  "input": place,
        //             "type": "(\(token.description))",
        //             "key": "AIzaSyAekPLq6U-9oTyxvwSI_DmtcZSsxgoKP9o"]
        //
        
        //  let url = "https://maps.googleapis.com/maps/api/place/autocomplete/json"
        
        
        
        var textForSearch = ""
        
        if(isSelectionDestinationLocation){
            textForSearch = txtDestination.text ?? ""
            
            if(textForSearch == "" ){
                self.searchedPlaceData = JSON()
                self.tbl.reloadData()
                return
            }
            
        }else{
            textForSearch = txtEnterPickUp.text ?? ""
            
            if(textForSearch  ==  ""){
                self.searchedPlaceData = JSON()
                self.tbl.reloadData()
                return
            }
        }
        
        
        let urlString = "https://maps.googleapis.com/maps/api/place/autocomplete/json?input=\(textForSearch)&sensor=\(token.description)&components=country:au&key=AIzaSyAekPLq6U-9oTyxvwSI_DmtcZSsxgoKP9o"
        
        
//          let urlString = "https://maps.googleapis.com/maps/api/place/autocomplete/json?input=\(textForSearch)&sensor=\(token.description)&key=AIzaSyAekPLq6U-9oTyxvwSI_DmtcZSsxgoKP9o"
        
        //https://maps.googleapis.com/maps/api/place/nearbysearch/json //"https//maps.googleapis.com/maps/api/place/autocomplete/json?query=\(place)&key=AIzaSyAekPLq6U-9oTyxvwSI_DmtcZSsxgoKP9o" ,
        
        
        let url = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
        
        
        
        Alamofire.request( url , method: .get , parameters: nil , headers : nil).responseJSON(completionHandler: { (response) in
            
            print(response)
            
            if(response.result.isSuccess){
                
                let jsondata :JSON = JSON(response.data ?? "")
                
                if(jsondata != ""){
                    self.searchedPlaceData =  jsondata["predictions"]
                    self.tbl.reloadData()
                }
            }
        })
    }
    
    
    
    //    func showGoogleMapAddressPiker(){
    //
    //     //   let vc = self.storyboard?.instantiateViewController(withIdentifier: "GooglePlaceSearchViewController") as! GooglePlaceSearchViewController
    //
    //   //     self.navigationController?.present( vc, animated: true, completion: nil )
    //
    //
    ////        let acController = GMSAutocompleteViewController()
    ////        acController.delegate = self
    ////        present(acController, animated: true, completion: nil)
    //    }
    
    //MARK:- ACTION
    
    @IBAction func pickupLocationButtonTapped(_ sender: Any) {
        
        
        if btnCrossPickUp.isSelected{
            
//            MapViewController.selectedSourceLocation = MapViewController.sourceLocation
//            MapViewController.selectedSourceLocationName = MapViewController.sourceLocationNameTemp
//            txtEnterPickUp.text = MapViewController.sourceLocationNameTemp
            
            API_ReverseGeoCoding { (success) in }
            
        }else{
            
            MapViewController.selectedSourceLocation = CLLocationCoordinate2D(latitude: 0, longitude: 0)
            MapViewController.selectedSourceLocationName = ""
            txtEnterPickUp.text = ""
            
        }
        
        btnCrossPickUp.isSelected = !btnCrossPickUp.isSelected
        
    }
    
    
    
    @IBAction func btnCrossDropClicked(_ sender: UIButton) {
        
        btnCrossDrop.isHidden = true
        txtDestination.text = ""
        
    }
    
    
    
    @IBAction func btnCrossClicked(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func dismissSelectDestinationVC(){
        self.navigationController?.popViewController(animated: false)
    }
    
    
    @objc func btnHeadClicked(_ sender: UIButton)
    {
        let strbd = staticClass.getStoryboard_Map()
        let vc = strbd?.instantiateViewController(withIdentifier: "SaveAddressVC") as! SaveAddressVC
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    
    //MARK:- REVERSE GEO CODE API
    
    func API_ReverseGeoCoding(completion:@escaping(Bool)->()){
        
        if locationManager.location == nil{return}
        CLGeocoder().reverseGeocodeLocation(locationManager.location!, completionHandler: {(placemarks, error)-> Void in
            if (error != nil) {
                print("Reverse geocoder failed with error" + error!.localizedDescription)
                completion(false)
                return
            }
            
            if placemarks!.count > 0 {
                let pm = placemarks![0] as CLPlacemark
                
               
                
                //                   self.displayLocationInfo(placemark: pm)
                self.displayLocationInfo(placemark: pm) { (success) in
                    if success{
                        completion(true)
                    }else{
                        completion(false)
                    }
                }
                
            } else {
                print("Problem with the data received from geocoder")
            }
        })
        
    }
    
    
    func displayLocationInfo(placemark: CLPlacemark,completion:@escaping(Bool) -> ()) {
        
        //stop updating location to save battery life
        locationManager.stopUpdatingLocation()
        //            print((placemark.locality != nil) ? placemark.locality : "")
        //            print((placemark.postalCode != nil) ? placemark.postalCode : "")
        //            print((placemark.administrativeArea != nil) ? placemark.administrativeArea : "")
        //            print((placemark.country != nil) ? placemark.country : "")
        //            print(placemark.subLocality)
        //            print(placemark.name)
        //            print(placemark.region)
        
        
        let lat = placemark.location!.coordinate.latitude
        let lng = placemark.location!.coordinate.longitude
        
        let service = Geocoder.Google(lat:lat , lng: lng, APIKey: Constants.AppLinks.APIKEY_Google)
        
        SwiftLocation.geocodeWith(service).then { result in
            // Different services, same expected output [GeoLocation]
            print(result.data)
            guard let geolocation = result.data?.first else{return}
            guard let formattedAddress = geolocation.info[.formattedAddress] else{return}
            

            MapViewController.sourceLocation = placemark.location!.coordinate
            MapViewController.sourceLocationNameTemp = formattedAddress ?? ""
            MapViewController.pincode = placemark.postalCode ?? ""
            self.txtEnterPickUp.text = formattedAddress
            MapViewController.selectedSourceLocation = placemark.location!.coordinate
            MapViewController.selectedSourceLocationName = formattedAddress ?? ""
            completion(true)
            
        }
        
        
        /*
        let name = placemark.name ?? ""
        let subLocality = placemark.subLocality ?? ""
        let locality = placemark.locality ?? ""
        let administrativeArea = placemark.administrativeArea ?? ""
        let country = placemark.country ?? ""
        let postalCode = placemark.postalCode ?? ""
        MapViewController.pincode = postalCode
        
        
        
        let currentAddress = "\(name), \(subLocality), \(locality), \(administrativeArea), \(country), \(postalCode)"
        print("current address from geo code is",currentAddress)
        
        
        
        MapViewController.sourceLocation = placemark.location!.coordinate
        MapViewController.sourceLocationNameTemp = currentAddress
        MapViewController.selectedSourceLocation = placemark.location!.coordinate
        MapViewController.selectedSourceLocationName = currentAddress
        self.txtEnterPickUp.text = currentAddress
        
        completion(true)*/
        
        
    }
    
    
    
    
    // func getLatLongFromPlaceID(placeID : String , placeNameStr : String){
    func getLatLongFromPlaceName(placeName : String , placeFullNameStr : String){
        
        
        //   let url = "https://maps.googleapis.com/maps/api/place/details/json?place_id=\(placeName)&fields=name,geometry&key=AIzaSyAekPLq6U-9oTyxvwSI_DmtcZSsxgoKP9o"
        
        
        let url = "https://maps.googleapis.com/maps/api/geocode/json?address=\(placeName)&key=AIzaSyAekPLq6U-9oTyxvwSI_DmtcZSsxgoKP9o"
        
        
        let urlEncoded =  url.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) ?? ""
        
        
        
        Alamofire.request( urlEncoded , method: .get , headers : nil).responseJSON(completionHandler: { (response) in
            
            print(response)
            
            if(response.result.isSuccess){
                
                let jsondata :JSON = JSON(response.data)
                
                let lat = jsondata["results"][0]["geometry"]["location"]["lat"].doubleValue // lng
                
                let lng = jsondata["results"][0]["geometry"]["location"]["lng"].doubleValue
                
                
                let locationCordenate = CLLocationCoordinate2DMake(lat, lng)
                
                
                
                
                if(self.isSelectionDestinationLocation){
                    
                    self.txtDestination.text = placeFullNameStr
                    MapViewController.selectedDestinationLocation = locationCordenate
                    MapViewController.selectedDestinationLocationName = placeFullNameStr
                    
                    self.dismissSelectDestinationVC()
                }else{
                    self.txtEnterPickUp.text = placeFullNameStr
                    MapViewController.selectedSourceLocation = locationCordenate
                    MapViewController.selectedSourceLocationName = placeFullNameStr
                    self.txtDestination.becomeFirstResponder()
                }
                
                
            }
        })
        
    }
    
    
    
    
    
    
    //MARK:- Get Lat Long From Address
    
    func getlatLongFromAddress(placeFullNameStr : String){
        
        guard let selectedAddress = placeFullNameStr.addingPercentEncoding(withAllowedCharacters: .symbols) else {
            print("Error. cannot cast name into String")
            return
        }
        
        
        //print(selectedAddress)
        let urlString =  "https://maps.googleapis.com/maps/api/geocode/json?address=\(selectedAddress)&sensor=false&key=AIzaSyAekPLq6U-9oTyxvwSI_DmtcZSsxgoKP9o"
        
        let url = URL(string: urlString)
        
        Alamofire.request(url!, method: .get, headers: nil)
            .validate()
            .responseJSON { (response) in
                switch response.result {
                case.success(let value):
                    let json = JSON(value)
                    // print(json)
                    
                    let lat = json["results"][0]["geometry"]["location"]["lat"].doubleValue
                    let lng = json["results"][0]["geometry"]["location"]["lng"].doubleValue
                    let formattedAddress = json["results"][0]["formatted_address"].rawString()
                    let postalArray = json["results"][0]["address_components"].arrayValue.last
                    let type = (postalArray?["types"].rawString() ?? "").trimmingCharacters(in: .whitespacesAndNewlines)
                   
                    
                    //                    json["results"][0]["address_components"]["short_name"].rawString()
                    
                    
                    let locationCordenate = CLLocationCoordinate2DMake(CLLocationDegrees(lat), CLLocationDegrees(lng))
                    
                    if(self.isSelectionDestinationLocation){
                        
                        self.txtDestination.text = formattedAddress
                        MapViewController.selectedDestinationLocation = locationCordenate
                        MapViewController.selectedDestinationLocationName = placeFullNameStr
                        
                        self.dismissSelectDestinationVC()
                    }else{
                        
                        if type == "[\n  \"postal_code\"\n]"{
                            MapViewController.pincode = postalArray?["short_name"].rawString() ?? ""
                        }
                        
                        self.txtEnterPickUp.text = placeFullNameStr
                        MapViewController.selectedSourceLocation = locationCordenate
                        MapViewController.selectedSourceLocationName = placeFullNameStr
                        self.txtDestination.becomeFirstResponder()
                    }
                    
                    
                case.failure(let error):
                    print("\(error.localizedDescription)")
                }
                
        }
        
    }
    
    
    
    
    private func API_getLocationList(){
        
        ActivityIndicatorWithLabel.shared.showProgressView(uiView: self.view)
        
        let userID = global.userId
        
        let dictKeys = [ "userId" : userID] as [String : Any]
        
        let url = Constants.AppLinks.API_BASE_URL + Constants.ApiUrl.locationList.rawValue
        
        Alamofire.request(url , method: .post, parameters: dictKeys, headers:nil).responseJSON(completionHandler: { (response) in
            
            //            Hud.shared.hideHud()
            ActivityIndicatorWithLabel.shared.hideProgressView()
            do {
                let json: JSON = try JSON(data: response.data!)
                if (json["statusCode"].stringValue == "200"){
                    
                    self.addresArray.removeAll()
                    self.arrySavedAddress = json["data"].arrayValue
                    self.tbl.reloadData()
                    
                }
                else{
                    // error
//                    self.showAlert(title: "Error", message: json["message"].stringValue)
                }
                
            } catch {
                // show a alert
                //                    Hud.shared.hideHud()
                ActivityIndicatorWithLabel.shared.hideProgressView()
                self.showAlert(title: "Error", message: "Please check network connection")
            }
        })
    }
    
    
    private func API_deleteSavedLocation(location id:String, index:Int){
        
        ActivityIndicatorWithLabel.shared.showProgressView(uiView: self.view)
        
        
        
        let dictKeys = [ "id" : id] as [String : Any]
        
        
        let url = Constants.AppLinks.API_BASE_URL + Constants.ApiUrl.deleteLocation.rawValue
        
        Alamofire.request(url , method: .post, parameters: dictKeys, headers:nil).responseJSON(completionHandler: { (response) in
            
            //            Hud.shared.hideHud()
            ActivityIndicatorWithLabel.shared.hideProgressView()
            do {
                let json: JSON = try JSON(data: response.data!)
                if (json["statusCode"].stringValue == "200"){
                    
                    self.showToast(message: "Location Deleted Successfully", seconds: 3)
                    self.arrySavedAddress.remove(at: index)
                    self.tbl.reloadData()
                    
                }
                else{
                    // error
                    self.showAlert(title: "Error", message: json["message"].stringValue)
                }
                
            } catch {
                // show a alert
                //                    Hud.shared.hideHud()
                ActivityIndicatorWithLabel.shared.hideProgressView()
                self.showAlert(title: "Error", message: "Please check network connection")
            }
        })
    }
    
    
}


//MARK:- TableView Delegate and Datasource

extension SelectDestinationVC:UITableViewDelegate,UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return  2
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if(indexPath.section == 0){
            return 60
        }
        return 80
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if(section == 1){
            return 70
        }
        return 0
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if(section == 0){
            return searchedPlaceData.count
        }
        
        return self.arrySavedAddress.count//addresArray.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SelectDestinationHeadTVCell") as! SelectDestinationHeadTVCell
        cell.btnHead.addTarget(self, action: #selector(btnHeadClicked(_:)) , for: .touchUpInside)
        return cell.contentView
    }
    
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return .leastNormalMagnitude
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        if(indexPath.section == 0){
            // searched result cell
            //GooglePlaceSearchTableViewCell
            let cell = tableView.dequeueReusableCell(withIdentifier: "GooglePlaceSearchTableViewCell") as! GooglePlaceSearchTableViewCell
            
            cell.placeNameLabel.text = searchedPlaceData[indexPath.row]["structured_formatting"]["main_text"].stringValue
            cell.placeDetailLabel.text = searchedPlaceData[indexPath.row]["structured_formatting"]["secondary_text"].stringValue
            
            return cell
        }
        
        /// stored addresses list cell
        let cell = tableView.dequeueReusableCell(withIdentifier: "SelectDestinationTVCell") as! SelectDestinationTVCell
        
        let dic = self.arrySavedAddress[indexPath.row].dictionaryValue
        
        cell.placeNameLabel.text = dic["type"]?.stringValue//addresArray[indexPath.row][0]
        cell.placeAddressLabel.text = dic["locations"]?.stringValue
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if(indexPath.section == 0){
            
            
            
            getlatLongFromAddress(placeFullNameStr: searchedPlaceData[indexPath.row]["description"].stringValue)
            
            
            
            
            //            getLatLongFromPlaceName(placeName: searchedPlaceData[indexPath.row]["structured_formatting"]["main_text"].stringValue, placeFullNameStr: searchedPlaceData[indexPath.row]["description"].stringValue )
            
            //getLatLongFromPlaceID(placeID: searchedPlaceData[indexPath.row]["place_id"].stringValue, placeNameStr: searchedPlaceData[indexPath.row]["description"].stringValue )
            return
        }
        
        //   get data from stored local database
        
        
        
//        let lat = addresArray[indexPath.row][2]
//        let lng = addresArray[indexPath.row][3]
//
//        let dlat : Double = Double(lat)!
//        let dlng : Double = Double(lng)!
//
        
        let dic = self.arrySavedAddress[indexPath.row].dictionaryValue
        
        let dlat : Double = dic["lat"]?.doubleValue ?? 0.0
        let dlng : Double = dic["lng"]?.doubleValue ?? 0.0
        
        
        
        if(isSelectionDestinationLocation){
            
            MapViewController.selectedDestinationLocation = CLLocationCoordinate2DMake(dlat, dlng)
            MapViewController.selectedDestinationLocationName = dic["locations"]?.stringValue ?? ""
            txtDestination.text = dic["locations"]?.stringValue ?? ""
            
            if(MapViewController.selectedSourceLocation.latitude != 0 && MapViewController.selectedSourceLocation.longitude != 0){
                dismissSelectDestinationVC()
            }
        }
        else{
            
            MapViewController.selectedSourceLocation = CLLocationCoordinate2DMake(dlat, dlng)
            MapViewController.selectedSourceLocationName = dic["locations"]?.stringValue ?? ""
            txtEnterPickUp.text = dic["locations"]?.stringValue ?? ""
            
            if(MapViewController.selectedDestinationLocation.latitude != 0 && MapViewController.selectedDestinationLocation.longitude != 0){
                dismissSelectDestinationVC()
            }
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        if indexPath.section == 1{
            return true
        }
        return false
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
        if(indexPath.section == 1){
            if (editingStyle == .delete) {
                // handle delete (by removing the data from your array and updating the tableview)
                print(indexPath.row)
//                let key = addresArray[indexPath.row][0]
//                UserDefaults.standard.removeObject(forKey: key)
//                getDataFromLocalDataBase()
                let dic = self.arrySavedAddress[indexPath.row].dictionaryValue
                API_deleteSavedLocation(location: dic["id"]?.stringValue ?? "", index: indexPath.row)
                
            }
        }
        
    }
    
}







////MARK:- PLACE PIKER CODE
//extension SelectDestinationVC: GMSAutocompleteViewControllerDelegate {
//
//
//  func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
//    // Get the place name from 'GMSAutocompleteViewController'
//    // Then display the name in textField
//
//
//    if(isSelectionDestinationLocation){
//
//        txtDestination.text = place.formattedAddress
//        MapViewController.selectedDestinationLocation = place.coordinate
//        MapViewController.selectedDestinationLocationName = place.formattedAddress ?? ""
//        // Dismiss the GMSAutocompleteViewController when something is selected
//        dismiss(animated: true, completion: nil)
//
//        dismissSelectDestinationVC()
//    }else{
//        txtEnterPickUp.text = place.formattedAddress
//        MapViewController.selectedSourceLocation = place.coordinate
//        MapViewController.selectedSourceLocationName = place.formattedAddress ?? ""
//        dismiss(animated: true, completion: nil)
//    }
//
//  }
//
//
//
//func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
//    // Handle the error
//    print("Error: ", error.localizedDescription)
//  }
//func wasCancelled(_ viewController: GMSAutocompleteViewController) {
//    // Dismiss when the user canceled the action
//    dismiss(animated: true, completion: nil)
//  }
//}
