//
//  AddCreditCardVC.swift
//  Infinite Cabs
//
//  Created by Apple on 03/01/20.
//  Copyright © 2020 micro. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import TLMonthYearPicker
import SVProgressHUD

class AddCreditCardVC: UIViewController , UITextFieldDelegate, TLMonthYearPickerDelegate {
    
    @IBOutlet weak var txtCardHolderName: UITextField!
    @IBOutlet weak var txtCardNumber: UITextField!
    @IBOutlet weak var txtExpires: UITextField!
    @IBOutlet weak var txtCvv: UITextField!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var datePikerBackgroundView: UIView!
    @IBOutlet weak var datePikerView: TLMonthYearPickerView!
    @IBOutlet weak var datePiker: UIDatePicker!
    @IBOutlet weak var imgTerms: UIImageView!
    
    
    var strTypeOfCard = ""
    var pickedDate = Date()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        txtExpires.delegate = self
        txtCardNumber.delegate = self
//        hideKeyboardWhenTappedAround()
        btnSave.layer.cornerRadius = 5
        
        datePiker.minimumDate = Date()
        
        //        self.datePikerView.calendar = calendar
        self.datePikerView.monthYearPickerMode = .monthAndYear // or '.year'
        self.datePikerView.minimumDate = Date()
        //        self.datePikerView.maximumDate = self.maximumDate
        self.datePikerView.delegate = self
        
        self.imgTerms.image = UIImage(named: "Rectangle 4")
        
    
    }
    
    
    //Picker delegate
    func monthYearPickerView(picker: TLMonthYearPickerView, didSelectDate date: Date) {
        //do your work with selected date object
        self.pickedDate = date
        print(date)
    }
    
    //MARK:- ACTION
    @IBAction func btnBackClicked(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    @IBAction func hideDatePikerButtonTapped(_ sender: Any) {
        datePikerBackgroundView.isHidden = true
        
    }
    // ok button tapped
    @IBAction func pickDataButtonTapped(_ sender: Any) {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/yyyy"
        let selectedDate = dateFormatter.string(from: self.pickedDate)
        txtExpires.text = selectedDate
        
        datePikerBackgroundView.isHidden = true
        
    }
    
    
    
    @IBAction func btnTermsCondClicked(_ sender: UIButton) {
        
        
        if self.imgTerms.image == UIImage(named: "Rectangle 4"){
            
            self.imgTerms.image = UIImage(named: "check-mark")
        }else{
            
            self.imgTerms.image = UIImage(named: "Rectangle 4")
        }
        
    }
    
    
    @IBAction func btnTermConditionLinkClicked(_ sender: UIButton) {
        
        let strbd = staticClass.getStoryboard_Settings()
        let vc = strbd?.instantiateViewController(withIdentifier: "LegalVC") as! LegalVC
        
        vc.isPrivacy = false
        vc.isTermsCond = false
        vc.isAboutUs = false
        vc.isStripeTC = true
        
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    
    
    
    @IBAction func btnSaveClicked(_ sender: UIButton) {
        
        if(validateName(txtCardHolderName.text ?? "" ) && validateCreditCardNumber(txtCardNumber.text ?? "") && validateCVV(txtCvv.text ?? "") && validateDate(txtExpires.text ?? "")){
            
            if(self.imgTerms.image! == UIImage(named: "Rectangle 4"))
            {
                HelpingMethod.shared.presentAlertWithTitle(title: "Error", message: Constants.errorMessage.TermsAndConditions.rawValue, vc: self)
            }else{
                
                updateCardOnServer()
            }
            
        }
    }
    
    
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if(textField == txtExpires){
            view.endEditing(true)
            datePikerBackgroundView.isHidden = false
            
            return false
        }
        return true
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        
        if(textField == txtCardNumber){
            
            if(textField.text?.count == 3 ||  textField.text?.count == 8 || textField.text?.count == 13 ){
                
                if(string == ""){
                    return true
                }
                else{
                    txtCardNumber.text = (textField.text ?? "") + string + "-"
                    return false
                }
                
            }
            
            if(textField == txtCardNumber){
                if(string == ""){
                    return true
                }
                
                if (19 <= textField.text?.count ?? 0) {
                    return false
                }
                
            }
        }
        
        return true
    }
    
    
    func showPopUp(){
        
        let strbd = staticClass.getStoryboard_Settings()
        let vc = strbd?.instantiateViewController(withIdentifier: "CustomAlertVC") as! CustomAlertVC
        vc.delegate = self
        vc.imgPop = #imageLiteral(resourceName: "girl")
        vc.strTitle = "Infinite Cabs"
        vc.strDescription = "Card details are invalid or expired. Please enter valid card details."
        self.present(vc, animated: true, completion: nil)
        
    }
    

    
    
    
    private func updateCardOnServer(){
        
        //       Hud.shared.showHud()
        ActivityIndicatorWithLabel.shared.showProgressView(uiView: self.view)
        let userID = global.userId
        let cardHolderName = txtCardHolderName.text ?? ""
        let cardNumber = txtCardNumber.text ?? ""
        let cardExpairy = txtExpires.text ?? ""
        let cardCSV = txtCvv.text ?? ""
        
        
        
        let dictKeys = [ "user_id"          : userID ,
                         "card_holder_name" : cardHolderName,
                         "card_number"      :  cardNumber ,
                         "expairy"          :  cardExpairy ,
                         "csv"              : cardCSV ,
                         "card_type"        : "credit",
                         "type"             : strTypeOfCard
            
            ] as [String : String]
        
        let url = Constants.AppLinks.API_BASE_URL + Constants.ApiUrl.insertCard.rawValue
        
        Alamofire.request(url , method: .post, parameters: dictKeys, headers:nil).responseJSON(completionHandler: { (response) in
            
            //              Hud.shared.hideHud()
            ActivityIndicatorWithLabel.shared.hideProgressView()
            do {
                let json: JSON = try JSON(data: response.data!)
                
                if (json["statusCode"].stringValue == "200"){
                    
                    //                      self.showToast(message: "Add card Successfully", seconds: 3)
                    //self.showMapVc()
                    DispatchQueue.main.async {
                        self.gotoCardValidation(json["cardId"].stringValue)
                    }
                    
                    
                }
                else{
                    // error
                    self.showAlert(title: "Error", message: json["message"].stringValue)
                }
                
            } catch {
                // show a alert
                //                  Hud.shared.hideHud()
                ActivityIndicatorWithLabel.shared.hideProgressView()
                self.showAlert(title: "Error", message: "Please check network connection")
            }
        })
    }
    
    
    func gotoCardValidation(_ id:String){
        
//        let strbd = staticClass.getStoryboard_Order()
//        let vc = strbd?.instantiateViewController(withIdentifier: "RatingOrderVC") as! RatingOrderVC
//        vc.view.backgroundColor = UIColor(white: 0, alpha: 0.5)
//        vc.orderId = jsonData["delivered"][sender.tag]["orderId"].stringValue
//        vc.delegate = self
//        self.definesPresentationContext = true
//        self.providesPresentationContextTransitionStyle = true
//        vc.modalPresentationStyle = .overCurrentContext
//        self.navigationController?.present(vc, animated: true, completion: nil)
        
        
        let strbd = UIStoryboard(name: "Settings", bundle: nil)
        let vc = strbd.instantiateViewController(withIdentifier: "CardValidationVC") as! CardValidationVC
        vc.cardid = id
        vc.view.backgroundColor = UIColor(white: 0, alpha: 0.5)
        vc.delegate = self
        vc.cardid = id
        self.definesPresentationContext = true
        self.providesPresentationContextTransitionStyle = true
        vc.modalPresentationStyle = .overCurrentContext
        self.navigationController?.present(vc, animated: true, completion: nil)
        
        
        
//        let strbd = UIStoryboard(name: "Settings", bundle: nil)
//        let vc = strbd.instantiateViewController(withIdentifier: "CardValidationVC") as! CardValidationVC
//        vc.cardid = id
//        vc.delegate = self
//        self.present(vc, animated: true, completion: nil)
        
    }
    
    //    cardid
    
    
    
    func showMapVc(){
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1.5) {
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: MapViewController.self) {
                    _ =  self.navigationController!.popToViewController(controller, animated: true)
                    break
                }
            }
        }
        
    }
}

extension AddCreditCardVC:customeAlertDelegate{
    
    func btnOkClicked() {
        
        //showMapVc()
    }
    
}


extension AddCreditCardVC:cardValidationDelegate{
    
    func isCardValid(_ status: Bool) {
        SVProgressHUD.dismiss()
        
        if status{
            self.showToast(message: "Add card Successfully", seconds: 3)
            
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1.5) {
             
                self.navigationController?.popViewController(animated: true)
            }
            
//            showMapVc()
        }else{
//            self.showToast(message: "error while adding card", seconds: 3)
//            self.showToast(message: "Card details are invalid or card has been expired", seconds: 3)
            
            showPopUp()
//            showMapVc()
        }
    }
    
}
