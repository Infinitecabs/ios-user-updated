//
//  CancelRideReasonViewController.swift
//  Infinite Cabs
//
//  Created by Mac on 4/2/20.
//  Copyright © 2020 micro. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON



class CancelRideReasonViewController: UIViewController {
    
    
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var proceedButton: UIButton!
    @IBOutlet weak var reasonField: UITextField!
    
    
    var cancelRideReasonData = JSON()
    
    @IBOutlet weak var cancelRideReasonTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUp_UI()
        getCancelRideReasonData()
    }
    
    func setUp_UI() {
        
        proceedButton.layer.cornerRadius = 4
        cancelButton.layer.cornerRadius = 4
        cancelButton.layer.borderWidth = 1
        cancelButton.layer.borderColor = UIColor.lightGray.cgColor
        
    }
    
    @IBAction func backButtonTapped(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    
    @IBAction func hideButtonTapped(_ sender: Any) {
        
        backgroundView.isHidden = true
        
    }
    
    
    @IBAction func proceedButtonTapped(_ sender: Any) {
        
        cancelBookedCarApi(reasonId: cancelRideReasonData[cancelRideReasonData.count - 1]["reason_id"].stringValue)
        
    }
    
    
    private func getCancelRideReasonData(){
           
//           Hud.shared.showHud()
        ActivityIndicatorWithLabel.shared.showProgressView(uiView: self.view)
           
           let url = "\(Constants.AppLinks.API_BASE_URL)\(Constants.ApiUrl.cancelreasonList.rawValue)"//Constants.ApiUrl.cancelreasonList.rawValue

           Alamofire.request(url , method: .post, parameters:  nil , headers:nil).responseJSON(completionHandler: { (response) in
//               Hud.shared.hideHud()
            ActivityIndicatorWithLabel.shared.hideProgressView()
               do {
                   let json: JSON = try JSON(data: response.data!)

         
                   if (json["statusCode"].stringValue == "200"){
                       
                    self.cancelRideReasonData =  json["data"]
                    self.cancelRideReasonTableView.reloadData()
                   }
                   else{
                       // error
                       self.showAlert(title: "Error", message: json["message"].stringValue)
                   }

               } catch {
                   // show a alert
//                   Hud.shared.hideHud()
                ActivityIndicatorWithLabel.shared.hideProgressView()
                   self.showAlert(title: "Error", message: "Please check network connection")
               }
           })
       }
   

    private func cancelBookedCarApi(reasonId : String){
          //userCancelRide
//          Hud.shared.showHud()
        ActivityIndicatorWithLabel.shared.showProgressView(uiView: self.view)
          
          let userID = global.userId
          let dictKeys = [ "user_id" : userID,
                           "driver_id" : MapViewController.AppData["driverId"] ?? "" ,//MapViewController.selectedDriverID,
                          "booking_id" : MapViewController.AppData["bookingId"] ?? "" ,
                        "reasonId" : reasonId ,
                        "type" : "user"//,
//                        "reason" : reasonField.text ?? ""
                      ] as [String : Any]


             let url = "\(Constants.AppLinks.API_BASE_URL)\(Constants.ApiUrl.declinedBookings.rawValue)"

             Alamofire.request(url , method: .post, parameters: dictKeys, headers:nil).responseJSON(completionHandler: { (response) in

//                Hud.shared.hideHud()
                ActivityIndicatorWithLabel.shared.hideProgressView()
                 do {
                     let json: JSON = try JSON(data: response.data!)

                     if (json["statusCode"].stringValue == "200"){

                        MapViewController.userCanCreateNewRide = true
                      NotificationCenter.default.post(Notification(name: Notification.Name("resetMapViewControllerAllData")))
                        
                        self.showToast(message: "Ride Cancel Successfully", seconds: 3)
                        
                        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1.5) {
                            self.navigationController?.popViewController(animated: true)
                        }
                        
                        
                        self.API_sessionDel{ (success) in
                            
                            if success{
                               
                            }else{
                                
                                
                            }
                            
                        }
                        
                        
                     }
                     else{
                         // error
                         self.showAlert(title: "Error", message: json["message"].stringValue)
                         }

                 } catch {
                     // show a alert
//                     Hud.shared.hideHud()
                    ActivityIndicatorWithLabel.shared.hideProgressView()
                     self.showAlert(title: "Error", message: "Please check network connection")
                 }
             })
          
          
          
      }
    
    
    
    
    private func API_sessionDel(completion:@escaping(Bool) -> ()){
           
//           Hud.shared.showHud()
        ActivityIndicatorWithLabel.shared.hideProgressView()
           
           let url = Constants.AppLinks.session_Delete_Url + "twiCall/session_delete.php"//Constants.ApiUrl.sessionDel.rawValue
           let dictKeys = [ "bookingId" : MapViewController.AppData["bookingId"] ?? ""]

           Alamofire.request(url , method: .post, parameters: dictKeys , headers:nil).responseJSON(completionHandler: { (response) in
//               Hud.shared.hideHud()
            ActivityIndicatorWithLabel.shared.hideProgressView()
               do {
                   let json: JSON = try JSON(data: response.data!)

         
                   if (json["statusCode"].stringValue == "200"){
                       
//                    self.cancelRideReasonData =  json["data"]
//                    self.cancelRideReasonTableView.reloadData()
                    completion(true)
                   }
                   else{
                       // error
                       self.showAlert(title: "Error", message: json["message"].stringValue)
                   }

               } catch {
                   // show a alert
//                   Hud.shared.hideHud()
                ActivityIndicatorWithLabel.shared.hideProgressView()
                   self.showAlert(title: "Error", message: "Please check network connection")
               }
           })
       }
    
    
    
}

extension CancelRideReasonViewController : UITableViewDataSource , UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cancelRideReasonData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CancelRideReasonTableViewCell") as! CancelRideReasonTableViewCell
        
        
        cell.reasonLabel.text = cancelRideReasonData[indexPath.row]["reason_title"].stringValue
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        if (indexPath.row == cancelRideReasonData.count - 1){
            backgroundView.isHidden = false
        }
        else{
            cancelBookedCarApi(reasonId: cancelRideReasonData[indexPath.row]["reason_id"].stringValue)
        }
    }
    
}
