//
//  PaymentMethodsVC.swift
//  Infinite Cabs
//
//  Created by micro on 18/10/19.
//  Copyright © 2019 micro. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class PaymentMethodsVC: UIViewController {
    
    
//    @IBOutlet weak var mytable: UITableView!
    
    var btnSelect:UIButton?
    var btnSelect1:UIButton?
    var cardData = JSON()
    

    var isPayWithAppSelected:Bool!
    var isPayDirectlySelected:Bool!
    var isCardSelected:Bool!
    var isAfterDeleteCard:Bool = false
    
    var cardId:String = ""
    
    var fromChoosePayType = false
    
    
    var arryIndexingOfRows = [0,1,2,3]
    var changeIndx = 0
    
    @IBOutlet weak var paymentTableView: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
            
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        getCardsDataFromAPI { (_) in }
    }
    
    
    @IBAction func cardSelectButtonTapped(_ sender: UIButton) {
        MapViewController.paymentType = "Card"
        // relode table
        MapViewController.paymentCardId = cardData[sender.tag]["id"].stringValue
        NotificationCenter.default.post(Notification(name: Notification.Name("ReloadTabel")))
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func cashPaymentSelectButtonTapped(_ sender: Any) {
         MapViewController.paymentType = "Cash"
        // relode table
        NotificationCenter.default.post(Notification(name: Notification.Name("ReloadTabel")))
        self.navigationController?.popViewController(animated: true)
    }
    

    
    private func getCardsDataFromAPI(completionHandler:@escaping(Bool)->()){
        
//        Hud.shared.showHud()
        ActivityIndicatorWithLabel.shared.showProgressView(uiView: self.view)
        
        let userID = global.userId
        
        var dictKeys = [String:String]()
        
        dictKeys["user_id"] = userID
        
        
        
        if !self.isAfterDeleteCard{
            if (isPayWithAppSelected != nil) && isPayWithAppSelected{
                dictKeys["selected"] = cardId
                dictKeys["paywith"] = "card"
            }else if (isPayDirectlySelected != nil) && isPayDirectlySelected{
                dictKeys["paywith"] = "cash"
            }
        }
        
        
        
        let url = Constants.AppLinks.API_BASE_URL + Constants.ApiUrl.getcardlist.rawValue
        
        Alamofire.request(url , method: .post, parameters: dictKeys, headers:nil).responseJSON(completionHandler: { (response) in
            
//            Hud.shared.hideHud()
            ActivityIndicatorWithLabel.shared.hideProgressView()
            do {
                let json: JSON = try JSON(data: response.data!)
                
                if (json["statusCode"].stringValue == "200"){
                    
                    self.cardData = json["data"]
                    
                    
                    let arryCashData = self.cardData["cash"].arrayValue
                    
                    if arryCashData[1]["selected"].stringValue == "1"{
                        self.changeIndx = 0
                        
                        let arryCardData = self.cardData["paywith"].arrayValue
                        
                        if arryCardData[0]["selected"].stringValue == "1"{
                            
                            UserDefaults.standard.set(arryCardData[0]["id"].stringValue, forKey: "CARDID")
                            
                        }else if arryCardData[1]["selected"].stringValue == "1"{
                            
                           UserDefaults.standard.set(arryCardData[1]["id"].stringValue, forKey: "CARDID")
                            
                        }else{
                            
                            UserDefaults.standard.set("", forKey: "CARDID")
                            
                        }
                        
                        UserDefaults.standard.set("Pay with the App", forKey: "PAYMENT")
                        
                        
                        
                    }else if arryCashData[0]["selected"].stringValue == "1"{
                        
                        self.changeIndx = 1
                        
                        UserDefaults.standard.set("Pay the Driver Directly", forKey: "PAYMENT")
                        UserDefaults.standard.set("", forKey: "CARDID")
                        
                    }else{
                        self.changeIndx = 0
                    }
                    
                    self.paymentTableView.reloadData()
                    
                    let arryCarddata = self.cardData["paywith"].arrayValue
                    
                    if self.fromChoosePayType && (self.isPayWithAppSelected != nil) && self.isPayWithAppSelected{
                        
                        let arryCardData = self.cardData["paywith"].arrayValue
                        
                       // MapViewController.paymentType = "Card"
                        MapViewController.paymentCardId = arryCardData[0]["id"].stringValue
                        
                        if self.fromChoosePayType && ((arryCarddata[0]["card_number"].stringValue != "") || (arryCarddata[1]["card_number"].stringValue != "")){
                            NotificationCenter.default.post(Notification(name: Notification.Name("ReloadTabel")))
                            self.navigationController?.popViewController(animated: true)
                        }
                        
                        
                    }else if self.fromChoosePayType && (self.isPayDirectlySelected != nil) && self.isPayDirectlySelected{
                        
                        //MapViewController.paymentType = "Cash"
                        
                        if self.fromChoosePayType{
                            NotificationCenter.default.post(Notification(name: Notification.Name("ReloadTabel")))
                            self.navigationController?.popViewController(animated: true)
                        }
                        
                    }
                    
                    completionHandler(true)
                    
                }
                else{
                    // error
                    completionHandler(false)
                    self.showAlert(title: "Error", message: json["message"].stringValue)
                    
                }
                
            } catch {
                // show a alert
                completionHandler(false)
//                Hud.shared.hideHud()
                ActivityIndicatorWithLabel.shared.hideProgressView()
                self.showAlert(title: "Error", message: "Please check network connection")
            }
        })
    }
       
    
    // MARK:- ACTIONS
    
    @IBAction func btnDoneClicked(_ sender: UIButton) {
        
    }
    
    
    @IBAction func btnCrossClicked(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    
//    @objc func btnAddPaymentMethodClciked(_ sender:UIButton){
//        // for select a card
//        let strbdHome = staticClass.getStoryboard_Main()
//        let vc = strbdHome?.instantiateViewController(withIdentifier: "AddCreditCardVC") as! AddCreditCardVC
//        self.navigationController?.pushViewController(vc, animated: true)
//
//    }
    
    
    @objc func btnPhoneSelectionClicked(_ sender:UIButton){

//        sender.isSelected = true
        
        
        isPayWithAppSelected = true
        isPayDirectlySelected = false
        getCardsDataFromAPI { (_) in }
        
        
        
//        let indxpth1 = IndexPath(row: 3, section: 0)
//        let cell1 = mytable.cellForRow(at: indxpth1) as! PaymentMethodTableViewCell
//        cell1.btnDriverDirectlty.isSelected = false
//
//        self.mytable.reloadData()
    }
    
    
    @objc func btnPayDriverDirectlty(_ sender:UIButton){
        
//        sender.isSelected = true
        
        
        isPayWithAppSelected = false
        isPayDirectlySelected = true
        getCardsDataFromAPI { (_) in }
        

        
        
//        let indxpth = IndexPath(row: 0, section: 0)
//        let cell = mytable.cellForRow(at: indxpth) as! PaymentMethodTableViewCell
//        cell.btnPhoneSelection.isSelected = false
        
        //self.mytable.reloadData()
        

    }
    
    
    
    @objc func btnMarkCreditClicked(_ sender:UIButton){
        
//        sender.isSelected = true
        isPayWithAppSelected = true
        isPayDirectlySelected = false
        let arryCardData = cardData["paywith"].arrayValue
        self.cardId = arryCardData[sender.tag-1]["id"].stringValue

        
//        let indxpth1 = IndexPath(row: 1, section: 0)
//        let indxpth2 = IndexPath(row: 2, section: 0)
//
//        var cell = PaymentMethodTableViewCell()
//
//        if sender.tag == 1{
//             cell = mytable.cellForRow(at: indxpth2) as! PaymentMethodTableViewCell
//             cell.btnMark.isSelected = false
//        }else{
//             cell = mytable.cellForRow(at: indxpth1) as! PaymentMethodTableViewCell
//             cell.btnMark.isSelected = false
//        }
        
        getCardsDataFromAPI { (_) in }

        
    }
    
    
    @objc func btnAddPaymentMethodClicked(_ sender:UIButton){
        
        let arryCardData = cardData["paywith"].arrayValue
        
        var typeOfCard:String!
        
        if sender.tag == (1 + changeIndx){
            typeOfCard = arryCardData[0]["type"].stringValue
        }else{
            typeOfCard = arryCardData[1]["type"].stringValue
        }
        
        if typeOfCard == ""{
            typeOfCard = "1"
        }
        
        let strbdHome = staticClass.getStoryboard_Main()
        let vc = strbdHome?.instantiateViewController(withIdentifier: "AddCreditCardVC") as! AddCreditCardVC
        vc.strTypeOfCard = typeOfCard
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    
    @objc func btnChangeCardClicked(_ sender:UIButton){
        
        
        let alert = UIAlertController(title: "Warning", message: "Do you want to change this card?", preferredStyle: .alert)
        
        let action1 = UIAlertAction(title: "Yes", style: .default) { (action) in
            
            self.deleteCardFromServer(index: sender.tag)
        }
        
        let action2 = UIAlertAction(title: "No", style: .cancel, handler: nil)
        
        alert.addAction(action1)
        alert.addAction(action2)
        
        self.present(alert, animated: true, completion: nil)
        
        
//        let strbdHome = staticClass.getStoryboard_Main()
//        let vc = strbdHome?.instantiateViewController(withIdentifier: "AddCreditCardVC") as! AddCreditCardVC
//        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    
    func deleteCardFromServer(index : Int){
        
//        Hud.shared.showHud()
        ActivityIndicatorWithLabel.shared.showProgressView(uiView: self.view)
        
        let arryCardData = cardData["paywith"].arrayValue
        let cardID = arryCardData[index-(1+changeIndx)]["id"].stringValue

        let userID = global.userId
        
        let dictKeys = [ "id" : cardID, "user_id" : userID ]
        
        let url = Constants.AppLinks.API_BASE_URL + Constants.ApiUrl.deleteCard.rawValue
                                               
           Alamofire.request(url , method: .post, parameters: dictKeys, headers:nil).responseJSON(completionHandler: { (response) in
                  
//               Hud.shared.hideHud()
            ActivityIndicatorWithLabel.shared.hideProgressView()
               do {
                   let json: JSON = try JSON(data: response.data!)
                                  
                   if (json["statusCode"].stringValue == "200"){
                       self.isAfterDeleteCard = true
                    self.getCardsDataFromAPI { (_) in }
                   }
                   else{
                       // error
                       self.showAlert(title: "Error", message: json["message"].stringValue)
                       }
                                         
               } catch {
                   // show a alert
//                   Hud.shared.hideHud()
                ActivityIndicatorWithLabel.shared.hideProgressView()
                   self.showAlert(title: "Error", message: "Please check network connection")
               }
           })
        
        
    }
    
}


// MARK:- TableView Delegate And DataSource

extension PaymentMethodsVC:UITableViewDelegate,UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        /*
        if section == 0{
            return 2 //cardData.count
        }else if section == 1{
            return 1
        }else{
            return 1
        }
        */
        if cardData["paywith"].arrayValue.count == 0{
            return 0
        }else{
            return 4
        }
       
    }
    
  /*
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if section == 0{
            
            let header = tableView.dequeueReusableCell(withIdentifier: "Head") as! PaymentMethodTableViewCell
            
            header.btnPhoneSelection.addTarget(self, action: #selector(btnPhoneSelectionClicked(_:)), for: .touchUpInside)
            
            return header.contentView
            
        }else {
            
            
//            let header = tableView.dequeueReusableCell(withIdentifier: "HeadII") as! PaymentMethodTableViewCell
//            return header.contentView
            
            
            let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: mytable.frame.width, height: 40))
            
            let wdth = self.mytable.frame.width - 40
            
            let lbl = UILabel(frame: CGRect(x: 20, y: 0, width: wdth, height: 40))
            lbl.textColor = UIColor.black
            lbl.text = "Choose a Different Payment Method"
            lbl.font = UIFont(name: "Simpel-Medium", size: 18)
            headerView.backgroundColor = .white
            headerView.addSubview(lbl)
            
            return headerView
            
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if section == 0{
            return 84//140
        }
//        else if section == 1{
//            return 0
//        }
        else{
            return 40
        }
        
    }
    */
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        /*
        if indexPath.section == 0{
            return 150
        }else{
            return 96
        }
       // return 120//96
        */
        /*
        if indexPath.row == (0 + changeIndx){
            return 64
        }else if indexPath.row == (1 + changeIndx){
            return 140//124
        }else if indexPath.row == (2 + changeIndx){
            return 140
        }else{
            return 140
        }
        */
        return UITableView.automaticDimension
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = PaymentMethodTableViewCell()
        
        
        /*
        if indexPath.section == 1{
    //button
//            cell = tableView.dequeueReusableCell(withIdentifier: "ADD") as! PaymentMethodTableViewCell
            cell = tableView.dequeueReusableCell(withIdentifier: "HeadII") as! PaymentMethodTableViewCell
//            cell.btnAddPayMethod.layer.cornerRadius = 4
//            cell.btnAddPayMethod.addTarget(self, action: #selector(btnAddPaymentMethodClciked(_:)), for: .touchUpInside)
            
            cell.btnDriverDirectlty.addTarget(self, action: #selector(btnPayDriverDirectlty(_:)), for: .touchUpInside)
            
        }else {
            
            cell = tableView.dequeueReusableCell(withIdentifier: "Method") as! PaymentMethodTableViewCell
            
            if indexPath.row == 0{
                
                
//                let cardNumber = cardData[indexPath.row]["card_number"].stringValue
//
//                cell.lblMethod.text = cardNumber
//
//                cell.lblExpiresDate.text = "Expires " + cardData[indexPath.row]["expairy_date"].stringValue
//                cell.btnMark.isHidden = true
//                cell.lblExpiresDate.isHidden = false
//
//             cell.imgPayMethod.image = UIImage(named: "mastercard")
//
//                cell.btnMark.isHidden = true
//                cell.btnView.isHidden = false
//                cell.btnView.tag = indexPath.row
                
                cell.btnAddPaymentMethod.isHidden = true
                cell.lblCardType.text = "Personal"
                cell.stackCardNumber.isHidden = false
                cell.btnChange.isHidden = false
                
            }
            else{
  
                
//                cell.lblMethod.text = "Cash/Eftpos"
//                cell.lblExpiresDate.text = ""
//                cell.lblExpiresDate.isHidden = true
//                cell.imgPayMethod.image = UIImage(named: "money")
//                cell.btnMark.isHidden = false
//                cell.btnView.isHidden = true
                
                cell.btnAddPaymentMethod.isHidden = false
                cell.stackCardNumber.isHidden = true
                cell.lblCardType.text = "Business"
                cell.btnAddPaymentMethod.layer.cornerRadius = 4
                cell.btnChange.isHidden = true
                
                
                
                
            }
        }
        
        */
        
        
        if indexPath.row == (0 + changeIndx){
            
            let arryCashData = cardData["cash"].arrayValue
            
            cell = tableView.dequeueReusableCell(withIdentifier: "Head") as! PaymentMethodTableViewCell
            
            if arryCashData[1]["selected"].stringValue == "1"{
                cell.btnPhoneSelection.isSelected = true
                self.isPayWithAppSelected = true
            }else{
                cell.btnPhoneSelection.isSelected = false
                self.isPayWithAppSelected = false
            }
            
            
            if changeIndx == 0{
                cell.lblChooseDiffPayMethod_App.isHidden = true
            }else{
                cell.lblChooseDiffPayMethod_App.isHidden = false
            }
            
            cell.btnPhoneSelection.tag = indexPath.row
            cell.btnPhoneSelection.addTarget(self, action: #selector(btnPhoneSelectionClicked(_:)), for: .touchUpInside)
            return cell
            
        }else if indexPath.row == (1 + changeIndx) || indexPath.row == (2 + changeIndx){
           
            cell = tableView.dequeueReusableCell(withIdentifier: "Method") as! PaymentMethodTableViewCell
            
             let arryCardData = cardData["paywith"].arrayValue
            
            if indexPath.row == (1 + changeIndx){
                
                if arryCardData[0]["card_number"].stringValue == ""{
                    
                    cell.stackCardNumber.isHidden = true
                    cell.btnChange.isHidden = true
                    cell.btnAddPaymentMethod.isHidden = false
                    cell.btnAddPaymentMethod.layer.cornerRadius = 4
                    cell.btnMark.isUserInteractionEnabled = false
                    cell.btnMark.isSelected = false
                    
                    if arryCardData[0]["type"].stringValue == "1"{
                        cell.lblCardType.text = "Personal"
                    }else{
                        cell.lblCardType.text = "Business"
                    }
                    
                    
                }else{
                    
                    cell.lblExpiresDate.text = "Expires " + arryCardData[0]["expairy_date"].stringValue
                    
                    let cardNumber = arryCardData[0]["card_number"].stringValue
                    cell.lblMethod.text = "**** **** **** " + String(cardNumber.suffix(4))
                    
                    if arryCardData[0]["type"].stringValue == "1"{
                        cell.lblCardType.text = "Personal"
                    }else{
                        cell.lblCardType.text = "Business"
                    }
                    
                    if arryCardData[0]["selected"].stringValue == "1"{
                        
                        UserDefaults.standard.set("Pay with the App", forKey: "PAYMENT")
                        UserDefaults.standard.set(arryCardData[0]["id"].stringValue, forKey: "CARDID")
                        cell.btnMark.isSelected = true
                        
                        
                    }else{
                        cell.btnMark.isSelected = false
                    }
                    
                    cell.stackCardNumber.isHidden = false
                    cell.btnChange.isHidden = false
                    cell.btnAddPaymentMethod.isHidden = true
                    cell.btnMark.isUserInteractionEnabled = true
                    
                    if isPayWithAppSelected {
//                        cell.btnMark.isUserInteractionEnabled = true
                    }else{
//                        cell.btnMark.isUserInteractionEnabled = false
                        cell.btnMark.isSelected = false
                    }
                    
                }
                
                
            }else{
                
                
                if arryCardData[1]["card_number"].stringValue == ""{
                    
                    cell.stackCardNumber.isHidden = true
                    cell.btnChange.isHidden = true
                    cell.btnAddPaymentMethod.isHidden = false
                    cell.btnAddPaymentMethod.layer.cornerRadius = 4
                    cell.btnMark.isUserInteractionEnabled = false
                    cell.btnMark.isSelected = false
                    if arryCardData[1]["type"].stringValue == "1"{
                        cell.lblCardType.text = "Personal"
                    }else{
                        cell.lblCardType.text = "Business"
                    }
                    
                    
                }else{
                    
                    cell.lblExpiresDate.text = "Expires " + arryCardData[1]["expairy_date"].stringValue
                    
                    let cardNumber = arryCardData[1]["card_number"].stringValue
                    cell.lblMethod.text = "**** **** **** " + String(cardNumber.suffix(4))
                    
                    if arryCardData[1]["type"].stringValue == "1"{
                        cell.lblCardType.text = "Personal"
                    }else{
                        cell.lblCardType.text = "Business"
                    }
                    
                    if arryCardData[1]["selected"].stringValue == "1"{
                        UserDefaults.standard.set(arryCardData[1]["id"].stringValue, forKey: "CARDID")
                        UserDefaults.standard.set("Pay with the App", forKey: "PAYMENT")
                        cell.btnMark.isSelected = true
                    }else{
                        cell.btnMark.isSelected = false
                    }
                    
                    cell.stackCardNumber.isHidden = false
                    cell.btnChange.isHidden = false
                    cell.btnAddPaymentMethod.isHidden = true
                    cell.btnMark.isUserInteractionEnabled = true
                    
                    if isPayWithAppSelected {
//                        cell.btnMark.isUserInteractionEnabled = true
                    }else{
//                        cell.btnMark.isUserInteractionEnabled = false
                        cell.btnMark.isSelected = false
                    }
                    
                }
               
            }
            
            
//            if isPayWithAppSelected {
//                cell.btnMark.isUserInteractionEnabled = true
//            }else{
//                cell.btnMark.isUserInteractionEnabled = false
//                cell.btnMark.isSelected = false
//            }
            
           
            cell.btnMark.tag = indexPath.row - changeIndx
            cell.btnMark.addTarget(self, action: #selector(btnMarkCreditClicked(_:)), for: .touchUpInside)
            
            cell.btnAddPaymentMethod.tag = indexPath.row
            cell.btnAddPaymentMethod.addTarget(self, action: #selector(btnAddPaymentMethodClicked(_:)), for: .touchUpInside)
            
            cell.btnChange.tag = indexPath.row
            cell.btnChange.addTarget(self, action: #selector(btnChangeCardClicked(_:)), for: .touchUpInside)
            
        }else{
            
            let arryCashData = cardData["cash"].arrayValue
            
            cell = tableView.dequeueReusableCell(withIdentifier: "HeadII") as! PaymentMethodTableViewCell
            
            if arryCashData[0]["selected"].stringValue == "1"{
                UserDefaults.standard.set("Pay the Driver Directly", forKey: "PAYMENT")
                UserDefaults.standard.set("", forKey: "CARDID")
                cell.btnDriverDirectlty.isSelected = true
                self.isPayDirectlySelected = true
            }else{
                cell.btnDriverDirectlty.isSelected = false
                self.isPayDirectlySelected = false
            }
            
            if changeIndx == 0{
                cell.lblChooseDiffPayMethod_Direct.isHidden = false
            }else{
                cell.lblChooseDiffPayMethod_Direct.isHidden = true
            }
            
            
            
            cell.btnDriverDirectlty.tag = indexPath.row
            cell.btnDriverDirectlty.addTarget(self, action: #selector(btnPayDriverDirectlty(_:)), for: .touchUpInside)
            
        }
        
        return cell
    }
    
    
//    func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
//        return true
//    }
//
//    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
//        <#code#>
//    }
    
    /// delete card on swipe
    /*
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        if(indexPath.section == 0){
            return true
        }
        return false
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
        if(indexPath.section == 0){
            if (editingStyle == .delete) {
                // handle delete (by removing the data from your array and updating the tableview)
                print(indexPath.row)
                deleteCardFromServer(index: indexPath.row)
            }
        }
        
    }
    
    */
}
