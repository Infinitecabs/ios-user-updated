//
//  DriverNoteVC.swift
//  Infinite Cabs
//
//  Created by Apple on 17/11/19.
//  Copyright © 2019 micro. All rights reserved.
//

import UIKit
import  GoogleMaps
import GooglePlaces

class DriverNoteVC: UIViewController {

    @IBOutlet weak var txtViewNote: UITextView!
    @IBOutlet weak var btnProceed: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var viewPopUp: UIView!
    
    
    var locationManager = CLLocationManager()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        hideKeyboardWhenTappedAround()
        setUp_UI()
        
        MapViewController.noteForDriver = ""
        // Do any additional setup after loading the view.
    }
    

    
    func setUp_UI() {
        
        btnProceed.layer.cornerRadius = 4
        btnCancel.layer.cornerRadius = 4
        btnCancel.layer.borderWidth = 1
        btnCancel.layer.borderColor = UIColor.lightGray.cgColor
        viewPopUp.layer.cornerRadius = 4
        
        txtViewNote.layer.borderWidth = 1
        txtViewNote.layer.borderColor = UIColor.lightGray.cgColor
    }
    
    
    
   // MARK:- ACTION
    
    @IBAction func btnProceedClicked(_ sender: UIButton) {
        
        willMove(toParent: nil)
        view.removeFromSuperview()
        removeFromParent()
        MapViewController.noteForDriver = txtViewNote.text ?? ""
        
        NotificationCenter.default.post(Notification(name: Notification.Name(rawValue: "callBookingCabApi")))
       // NotificationCenter.default.post(Notification(name: Notification.Name(rawValue: "ShowDriverDetail")))
    }
    
    
    
    @IBAction func btnCancelClicked(_ sender: UIButton) {
        willMove(toParent: nil)
        view.removeFromSuperview()
        removeFromParent()
    }
    
}
