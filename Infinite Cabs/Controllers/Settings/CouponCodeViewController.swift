//
//  CouponCodeViewController.swift
//  Infinite Cabs
//
//  Created by Mac on 3/27/20.
//  Copyright © 2020 micro. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

class CouponCodeViewController: UIViewController {
    
    var couponCodeData = JSON()
    
    @IBOutlet weak var couponCodeTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getCouponCodeData()
    }
    
    
    @IBAction func btnBackClicked(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    private func getCouponCodeData(){
        
        //        Hud.shared.showHud()
        ActivityIndicatorWithLabel.shared.showProgressView(uiView: self.view)
        
        
        //http://stageofproject.com/texiapp-new/web_service/promocodeList
        let url = "\(Constants.AppLinks.API_BASE_URL)\(Constants.ApiUrl.promocodeList.rawValue)"//Constants.ApiUrl.promocodeList.rawValue
        
        Alamofire.request(url , method: .post, parameters: nil, headers:nil).responseJSON(completionHandler: { (response) in
            //            Hud.shared.hideHud()
            ActivityIndicatorWithLabel.shared.hideProgressView()
            do {
                let json: JSON = try JSON(data: response.data!)
                
                if (json["statusCode"].stringValue == "200"){
                    
                    self.couponCodeData = json["data"]
                    self.couponCodeTableView.reloadData()
                }
                else if (json["statusCode"].stringValue == "201"){
                    self.showAlert(title: json["message"].stringValue, message: "")
                }
                else{
                    // error
                    self.showAlert(title: "Error", message: json["message"].stringValue)
                }
                
            } catch {
                // show a alert
                //                Hud.shared.hideHud()
                ActivityIndicatorWithLabel.shared.hideProgressView()
                self.showAlert(title: "Error", message: "Please check network connection")
            }
        })
    }
    
}


extension  CouponCodeViewController : UITableViewDelegate , UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        couponCodeData.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 130
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CouponCodeTableViewCell") as! CouponCodeTableViewCell
        
        cell.promoCodeNameLabel.text = couponCodeData[indexPath.row]["promocode"].stringValue
        cell.promoCodeSubNameLabel.text = couponCodeData[indexPath.row]["description"].stringValue
        
        return cell
    }
    
}
