//
//  DriverRatingVC.swift
//  Infinite Cabs
//
//  Created by Apple on 17/11/19.
//  Copyright © 2019 micro. All rights reserved.
//

import UIKit
import Cosmos
import Alamofire
import SwiftyJSON
import SDWebImage


class DriverRatingVC: UIViewController {
    
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    
    @IBOutlet weak var imgDriver: UIImageView!
   // @IBOutlet weak var viewImgDrver: UIView!
    
    @IBOutlet weak var lblDriverName: UILabel!
    @IBOutlet weak var lblVehicleNumber: UILabel!
    @IBOutlet weak var lblVehicalName: UILabel!
    
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var lblPayMode: UILabel!
    
    @IBOutlet weak var txtView: UITextView!
    @IBOutlet weak var viewContainerCosmos: UIView!
    @IBOutlet weak var vewCosmos: CosmosView!
    @IBOutlet var viewCsmsLyr: UIView!
    @IBOutlet var ratingView: CosmosView!
    @IBOutlet var viewImg: UIView!
    
    
    
    var driverData = JSON()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        txtView.delegate = self
        CheckRequestuserAPI()
        setUp_UI()
        hideKeyboardWhenTappedAround()
        
        
    }
    
    
    
    // MARK:- SetUp UI
    
    func setUp_UI(){
        
        btnSubmit.layer.cornerRadius = 4
        btnCancel.layer.cornerRadius = 4
        
        btnCancel.setShadow()
        btnSubmit.setShadow()
        
        viewCsmsLyr.layer.borderColor = UIColor(named: "lightGraySet")?.cgColor
        viewCsmsLyr.layer.borderWidth = 1
        
        txtView.delegate = self
        
        viewImg.layer.borderColor = UIColor(named: "lightGraySet")?.cgColor
        viewImg.layer.borderWidth = 1
        viewImg.layer.cornerRadius = viewImg.frame.height/2
        
        txtView.layer.borderColor = UIColor(named: "lightGraySet")?.cgColor//UIColor.lightGray.cgColor
        txtView.layer.borderWidth = 1
        lblVehicleNumber.layer.borderWidth = 1
        lblVehicleNumber.layer.borderColor = UIColor(named: "lightGraySet")?.cgColor
        
//        viewImgDrver.layer.cornerRadius = viewImgDrver.frame.height/2
//        viewImgDrver.clipsToBounds = true
//        viewImgDrver.layer.borderWidth = 1
//        viewImgDrver.layer.borderColor = UIColor.lightGray.cgColor
        
        txtView.textColor = UIColor.lightGray
        txtView.text = "Leave a comment"
        
        
//        vewCosmos.layer.borderWidth = 1
//        vewCosmos.layer.borderColor = UIColor.lightGray.cgColor
//        self.viewContainerCosmos.layer.masksToBounds = false
//        self.viewContainerCosmos.layer.shadowRadius = 2
//        self.viewContainerCosmos.layer.shadowOpacity = 0.5
//        self.viewContainerCosmos.layer.shadowColor = UIColor.lightGray.cgColor
//        self.viewContainerCosmos.layer.shadowOffset = CGSize(width: 0, height: -4)
     
        //showPopUp()
    }
    
    func showPopUp(){
        
        let strbd = staticClass.getStoryboard_Settings()
        let vc = strbd?.instantiateViewController(withIdentifier: "CustomAlertVC") as! CustomAlertVC
        vc.strTitle = "Thank you for using Infinite Cabs"
        vc.strDescription = "Your ride has successfully completed. Please rate your driver now."
//        vc.imgPop = #imageLiteral(resourceName: "checkMark")
        vc.strBtnOkTitle = "Driver Rating"
        vc.btnOkWidth = 140
        self.present(vc, animated: true, completion: nil)
        
        
    }
    
    
   func setData() {
    
    vewCosmos.rating = driverData["driverRating"].doubleValue
          lblDriverName.text = driverData["driverName"].stringValue
          lblVehicleNumber.text = driverData["driverCar_No"].stringValue
          lblVehicalName.text = driverData["driverCar_Model"].stringValue
    let fAmnt = driverData["finalAmount"].stringValue == "" ? "0" : driverData["finalAmount"].stringValue
    
          lblAmount.text = "$" + fAmnt
          lblPayMode.text = "Paid in " + driverData["paymentType"].stringValue
    
    let strimg = Constants.AppLinks.API_BASE_URL_IMG + "driverimages/" + driverData["driverImage"].stringValue
    
    if let encoded = strimg.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed){
        if let imageurl = URL(string: encoded){
            print(imageurl)
            imgDriver.sd_setImage(with: imageurl, placeholderImage: UIImage(named: "user_Ic"), options: SDWebImageOptions(rawValue: 0), context: nil)
        }
    }
    
    
//        if let imageURL = URL(string: "http://stageofproject.com/texiapp-new/user_image/" + driverData["driverImage"].stringValue) {
//            imgDriver.sd_setImage(with: imageURL, placeholderImage: UIImage(named: "user_Ic"), options: SDWebImageOptions.progressiveLoad) { (image, error, sdimagecacheType, url) in
//
//                }
//            }
        }
    
    
    private func CheckRequestuserAPI(){
              //userCancelRide
//              Hud.shared.showHud()
        ActivityIndicatorWithLabel.shared.showProgressView(uiView: self.view)
           
              let userID = global.userId
              let dictKeys = [ "user_id" : userID
                          ] as [String : Any]


                 let url = "\(Constants.AppLinks.API_BASE_URL)\(Constants.ApiUrl.checkRequestuser.rawValue)"//Constants.ApiUrl.checkRequestuser.rawValue

                 Alamofire.request(url , method: .post, parameters: dictKeys, headers:nil).responseJSON(completionHandler: { (response) in

//                    Hud.shared.hideHud()
                    ActivityIndicatorWithLabel.shared.hideProgressView()
                     do {
                         let json: JSON = try JSON(data: response.data!)

                         if (json["statusCode"].stringValue == "200"){

                            self.driverData = json["bookingData"]
                            self.setData()
                         }
                         else if (json["statusCode"].stringValue == "201"){
                           
                         }
                         else {
                             // error
                             self.showAlert(title: "Error", message: json["message"].stringValue)
                             }

                     } catch {
                         // show a alert
//                         Hud.shared.hideHud()
                        ActivityIndicatorWithLabel.shared.hideProgressView()
                         self.showAlert(title: "Error", message: "Please check network connection")
                     }
                 })
              
              
              
          }
    
    //MARK:- ACTION
    
    
    @IBAction func btnSubmitClicked(_ sender: UIButton) {
        //MARK:- TODO // chake condiction
        //Temp code for testing
        
        print(vewCosmos.rating)
        
        if ratingView.rating == 0{
            HelpingMethod.shared.presentAlertWithTitle(title: "Error", message: Constants.errorMessage.selectStar.rawValue, vc: self)
        }else if txtView.textColor == UIColor.lightGray || txtView.text == ""{
            HelpingMethod.shared.presentAlertWithTitle(title: "Error", message: Constants.errorMessage.leaveComment.rawValue, vc: self)
        }else{
             submitDriverRatting()
        }
        
        
        //   let strbd = UIStoryboard(name: "Map", bundle: nil)
        //    let vc = strbd.instantiateViewController(withIdentifier: "DriverNoteVC") as! DriverNoteVC
        //self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    private func submitDriverRatting(){
        
        
       

//        Hud.shared.showHud()
        ActivityIndicatorWithLabel.shared.showProgressView(uiView: self.view)
               
        let userID = global.userId
        
         let dictKeys = [ "user_id" : userID ,
                          "driver_id" : MapViewController.AppData["driverId"] ?? "" ,
                          "booking_id" : MapViewController.AppData["bookingId"] ?? "" ,
                          "rating" : ratingView.rating ,
                          "message" : txtView.text ?? ""
             
             ] as [String : Any]
            
            
               let url = "\(Constants.AppLinks.API_BASE_URL)\(Constants.ApiUrl.driverFeedback.rawValue)"//Constants.ApiUrl.driverFeedback.rawValue
                                                   
               Alamofire.request(url , method: .post, parameters: dictKeys, headers:nil).responseJSON(completionHandler: { (response) in
                      
//                  Hud.shared.hideHud()
                ActivityIndicatorWithLabel.shared.hideProgressView()
                 
                   do {
                       let json: JSON = try JSON(data: response.data!)
                                      
                       if (json["statusCode"].stringValue == "200"){
                     // driver data found(request accepted)
                         
                         MapViewController.userCanCreateNewRide = true
                        
                        NotificationCenter.default.post(Notification(name: Notification.Name("resetMapViewControllerAllData")))
                        
                        //todo :- reset map view data
                        DispatchQueue.main.async {
                            
                            let strbd = staticClass.getStoryboard_Settings()
                            let vc = strbd?.instantiateViewController(withIdentifier: "CustomAlertVC") as! CustomAlertVC
                            vc.strTitle = "Thank you for using Infinite Cabs"
                            vc.strDescription = "Your driver rating has successfully been received. "
                            vc.delegate = self
                            self.present(vc, animated: true, completion: nil)
                            
                        }
                        
//                        self.navigationController?.popViewController(animated: true)
                         
                       }
                         
                       else{
                           // error
                           self.showAlert(title: "Error", message: json["message"].stringValue)
                           }
                                             
                   } catch {
                       // show a alert
//                       Hud.shared.hideHud()
                    ActivityIndicatorWithLabel.shared.hideProgressView()
                       self.showAlert(title: "Error", message: "Please check network connection")
                   }
               })
        
        
    }
    
    
    @IBAction func btnCancelClicked(_ sender: UIButton) {
        //self.navigationController?.popViewController(animated: true)
    }
    
    
}



//MARK:- TextView Delegate

extension DriverRatingVC:UITextViewDelegate{
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if txtView.textColor == UIColor.lightGray{
            txtView.text = ""
            txtView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        if(txtView.text == ""){
            if txtView.textColor == UIColor.black{
                txtView.text = "Leave a comment"
                txtView.textColor = UIColor.lightGray
            }
        }
        
    }
    
    
}


extension DriverRatingVC:customeAlertDelegate{
    
    func btnOkClicked() {
        self.navigationController?.popViewController(animated: true)
    }

}
