//
//  CustomAlertVC.swift
//  CustomLogInDemo
//
//  Created by Apple on 21/07/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

protocol customeAlertDelegate {
    func btnOkClicked()
}

class CustomAlertVC: UIViewController {

    @IBOutlet weak var btnOk: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var viewPopUp: UIView!
    @IBOutlet weak var imgCheckMark: UIImageView!
    @IBOutlet weak var btnNo: UIButton!
    @IBOutlet var NSLayoutBtnOkWidth: NSLayoutConstraint!
    @IBOutlet weak var viewToast: UIView!
    
    
    
    var (strTitle,strDescription) = ("","")
    var (isTitleHidden,isDescriptionHidden,isImageHidden,isBtnNoHidden, isBottomViewHidden) = (false,false,false,true,true)
    var imgPop:UIImage = #imageLiteral(resourceName: "check_ic")
    var (strBtnOkTitle,strBtnNoTitle) = ("Ok","No")
    var btnOkWidth = 80
    
    var delegate:customeAlertDelegate?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        btnOk.layer.cornerRadius = 4
        btnNo.layer.cornerRadius = 4
        viewPopUp.layer.cornerRadius = 12
        
        self.lblTitle.text = strTitle
        self.lblDescription.text = strDescription
        
        self.lblTitle.isHidden = isTitleHidden
        self.lblDescription.isHidden = isDescriptionHidden
        self.imgCheckMark.isHidden = isImageHidden
        self.btnNo.isHidden = isBtnNoHidden
        
        
        self.imgCheckMark.image = imgPop
        
        self.btnOk.setTitle(strBtnOkTitle, for: .normal)
        self.btnNo.setTitle(strBtnNoTitle, for: .normal)
        
        NSLayoutBtnOkWidth.constant = CGFloat(btnOkWidth)
        

        if !isBottomViewHidden{
            hideShowBottomView()
        }
        
    }
    
    func hideShowBottomView(){
        
        UIView.animate(withDuration: 2, delay: 0, options: .transitionFlipFromBottom, animations: {
            
            self.viewToast.isHidden = false
            
        }) { (completed) in
            self.viewToast.layer.cornerRadius = self.viewToast.bounds.height/2
            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                self.viewToast.isHidden = true
            }
        }
        
    }
    
    

    @IBAction func btnOkClicked(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
        delegate?.btnOkClicked()
        
    }
    
    @IBAction func btnNoClicked(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
