//
//  CouponCodeTableViewCell.swift
//  Infinite Cabs
//
//  Created by Mac on 3/27/20.
//  Copyright © 2020 micro. All rights reserved.
//

import UIKit

class CouponCodeTableViewCell: UITableViewCell {

    @IBOutlet weak var promoCodeNameLabel: UILabel!
    @IBOutlet weak var promoCodeSubNameLabel: UILabel!
    
    
    @IBOutlet weak var imgPromo: UIImageView!
    @IBOutlet weak var viewContain: UIView!
    @IBOutlet weak var viewcodeName: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        viewContain.layer.cornerRadius = 4
        viewContain.clipsToBounds = true
        imgPromo.layer.cornerRadius = 4
        viewcodeName.layer.cornerRadius = 4
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
