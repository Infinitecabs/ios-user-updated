//
//  VerifyPhoneOnChangedVC.swift
//  Infinite Cabs
//
//  Created by Apple on 20/11/20.
//  Copyright © 2020 micro. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON


class VerifyPhoneOnChangedVC: UIViewController,UITextFieldDelegate {
    
    
    @IBOutlet weak var txtOtp1: UITextField!
    @IBOutlet weak var txtOtp2: UITextField!
    @IBOutlet weak var txtOtp3: UITextField!
    @IBOutlet weak var txtOtp4: UITextField!
    @IBOutlet weak var txtOtp5: UITextField!
    @IBOutlet weak var btnVerify: UIButton!
    
    var strOtp = ""
    var strPhone = ""
    var strCountryCode = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        txtOtp1.delegate = self
        txtOtp2.delegate = self
        txtOtp3.delegate = self
        txtOtp4.delegate = self
        txtOtp5.delegate = self
        txtOtp1.becomeFirstResponder()
        
        setup_UI()
        resendOtp()
        
    }
    
    
    func setup_UI(){
        
        self.hideKeyboardWhenTappedAround()
        txtOtp1.setBottomBorder()
        txtOtp2.setBottomBorder()
        txtOtp3.setBottomBorder()
        txtOtp4.setBottomBorder()
        txtOtp5.setBottomBorder()
        btnVerify.layer.cornerRadius = 4
        
    }
    
    
    
    func textFieldDidChangeSelection(_ textField: UITextField) {
        if textField.text != ""{
            
            if(textField == txtOtp1){
                txtOtp2.becomeFirstResponder()
            }
            else if(textField == txtOtp2){
                txtOtp3.becomeFirstResponder()
            }
            else if(textField == txtOtp3){
                txtOtp4.becomeFirstResponder()
            }
            else if(textField == txtOtp4){
                txtOtp5.becomeFirstResponder()
            }
            else if(textField == txtOtp5){
                resignFirstResponder()
            }
            
        }
        
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let allowedCharacters = CharacterSet(charactersIn:"0123456789")//Here change this characters based on your requirement
        let characterSet = CharacterSet(charactersIn: string)
        
        if(string.count > 1){
            return false
        }
        
        if(string == "")
        {
            return true
        }
        if (1 <= textField.text?.count ?? 0){
            
            textField.text = ""
            return true
        }
        
        return allowedCharacters.isSuperset(of: characterSet)
        
    }
    
    
    // MARK:- ACTIONS
    
    @IBAction func btnBackClicked(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func btnResendCode(_ sender: UIButton) {
        resendOtp()
    }
    
    @IBAction func btnVerifyClicked(_ sender: UIButton) {
        
        strOtp = ""
        
        strOtp.append(txtOtp1.text ?? "")
        strOtp.append(txtOtp2.text ?? "")
        strOtp.append(txtOtp3.text ?? "")
        strOtp.append(txtOtp4.text ?? "")
        strOtp.append(txtOtp5.text ?? "")
        
        
        if(strOtp.count == 5){
            // call api
            verifyOtp()
            
        }else{
            // show alert
            showAlert(title: "Please enter full OTP", message: "")
        }
        
        
    }
    
    
    
    
    
    private func resendOtp(){
        
        
        ActivityIndicatorWithLabel.shared.showProgressView(uiView: self.view)
        
        let userID = global.userId
        let dictKeys = [ "userId" : userID, "phone" : strPhone, "phoneCode":strCountryCode] as [String : Any]
        
        
        let url = Constants.AppLinks.API_BASE_URL + Constants.ApiUrl.ResendOtpforPhone.rawValue
        
        Alamofire.request(url , method: .post, parameters: dictKeys, headers:nil).responseJSON(completionHandler: { (response) in
            
            //            Hud.shared.hideHud()
            ActivityIndicatorWithLabel.shared.hideProgressView()
            do {
                let json: JSON = try JSON(data: response.data!)
                if (json["statusCode"].stringValue == "200"){
                    
                    self.showToast(message: "New code has been sent successfully on you mobile number.", seconds: 2)
                }
                else{
                    // error
                    self.showAlert(title: "Error", message: json["message"].stringValue)
                }
                
            } catch {
                // show a alert
                //                    Hud.shared.hideHud()
                ActivityIndicatorWithLabel.shared.hideProgressView()
                self.showAlert(title: "Error", message: "Please check network connection")
            }
        })
    }
    
    
    
    private func verifyOtp(){
        
        ActivityIndicatorWithLabel.shared.showProgressView(uiView: self.view)
        
        let userID = global.userId
        let dictKeys = [ "userId" : userID ,
                         "otp" : strOtp, "phone" : strPhone, "phoneCode":strCountryCode] as [String : Any]
        
        
        let url = Constants.AppLinks.API_BASE_URL + Constants.ApiUrl.VerifyOtpForPhone.rawValue
        
        Alamofire.request(url , method: .post, parameters: dictKeys, headers:nil).responseJSON(completionHandler: { (response) in
            
            //              Hud.shared.hideHud()
            ActivityIndicatorWithLabel.shared.hideProgressView()
            do {
                let json: JSON = try JSON(data: response.data!)
                if (json["statusCode"].stringValue == "200"){
                    
                    self.showToast(message: "Verification Successful", seconds: 3)
                    global.userMobileNumber = self.strPhone
                    self.showMapViewController()
                    
//                    self.navigationController?.popToRootViewController(animated: true)
                    
                }
                else{
                    // error
                    self.showAlert(title: "Error", message: json["message"].stringValue)
                }
                
            } catch {
                // show a alert
                //                      Hud.shared.hideHud()
                ActivityIndicatorWithLabel.shared.hideProgressView()
                self.showAlert(title: "Error", message: "Please check network connection")
            }
        })
    }
    
    private func showMapViewController() {
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1.5) {
            
            let stbdHome = staticClass.getStoryboard_Map()
            
            let nextViewController = stbdHome?.instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
            let navigationController = UINavigationController(rootViewController: nextViewController)
            let appdelegate = UIApplication.shared.delegate as! AppDelegate
            appdelegate.window?.rootViewController = navigationController
            
            
//            let strbdMap = staticClass.getStoryboard_Map()
//            let vc = strbdMap?.instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
//            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    
    
}
