//
//  CardValidationVC.swift
//  Infinite Cabs
//
//  Created by Apple on 12/11/20.
//  Copyright © 2020 micro. All rights reserved.
//

import UIKit
import WebKit
import SVProgressHUD


protocol cardValidationDelegate {
    func isCardValid(_ status:Bool)
}


class CardValidationVC: UIViewController,WKNavigationDelegate,WKUIDelegate {
    
    @IBOutlet weak var myWebkit: WKWebView!
    @IBOutlet weak var viewWeb: UIView!
    
    
    
    var baseUrl = Constants.AppLinks.API_BASE_URL
    var cardid = ""
    
    var delegate:cardValidationDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        myWebkit.uiDelegate = self
        myWebkit.navigationDelegate = self
        
//        https://stageofproject.com/texiapp-new/web_service/Cardsuccess
//    https://stageofproject.com/texiapp-new/web_service/cardVerify/
            
        viewWeb.layer.cornerRadius = 12
        view.clipsToBounds = true
        print("url is", "\(baseUrl)cardVerify/\(cardid)")
        myWebkit.load(URLRequest.init(url: URL(string: "\(baseUrl)cardVerify/\(cardid)")!))
//        myWebkit.load(URLRequest.init(url: URL(string: "http://stageofproject.com/testform/test.php")!))
        
        
    }
    
//    https://stageofproject.com/texiapp-new/web_service/checkPayment/
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Swift.Void){
        
        
        if let urlStr = navigationAction.request.url?.absoluteString{
            print("checkurl:::",urlStr)
            if(urlStr == "\(Constants.AppLinks.API_BASE_URL_IMG)Cardsuccess")
            {
//                https://admin.infinitecabs.com.au/Cardsuccess
                // SVProgressHUD.showSuccess(withStatus: "Your payment has been done successfully")
//                self.showToast(message: "Add card Successfully", seconds: 3)
                
                self.dismiss(animated: true){
                    self.delegate?.isCardValid(true)
                }
               
            }
            else
                if(urlStr == "\(Constants.AppLinks.API_BASE_URL_IMG)payment_failed")
                {
                   
                  //TODO:
                   
                   //https://admin.infinitecabs.com.au/payment_failed
                    
//                    self.showToast(message: "card error", seconds: 3)
                    self.dismiss(animated: true) {
                        self.delegate?.isCardValid(false)
                    }
//                    self.dismiss(animated: true, completion: nil)
                    print("there is error with this")
                }
            
        }
        //        if let urlStr = navigationAction.request.URL?.absoluteString{
        //            //urlStr is what you want, I guess.
        //        }@
        decisionHandler(.allow)
    }
    
    public func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!){
        SVProgressHUD.show(withStatus: "Please wait..")
    }
    
    func webView(_ webView: WKWebView, didReceiveServerRedirectForProvisionalNavigation navigation: WKNavigation!)
    {
        SVProgressHUD.dismiss()
    }
    
    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error)
    {
        debugPrint(error)
        SVProgressHUD.dismiss()
        self.dismiss(animated: true, completion: nil)
    }
    
    
    public func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!){
        
         SVProgressHUD.dismiss()
        //        navigation.act
        //        if let urlStr = navigationAction.request.URL?.absoluteString{
        //            //urlStr is what you want, I guess.
        //        }
        //        decisionHandler(.Allow)
        
    }
    
    
}
