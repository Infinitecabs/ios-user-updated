//
//  MyRideHeaderTableViewCell.swift
//  Infinite Cabs
//
//  Created by Mac on 2/28/20.
//  Copyright © 2020 micro. All rights reserved.
//

import UIKit

class MyRideHeaderTableViewCell: UITableViewCell {

    @IBOutlet weak var textlabel: UILabel!
    
    @IBOutlet weak var getStartButton: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
