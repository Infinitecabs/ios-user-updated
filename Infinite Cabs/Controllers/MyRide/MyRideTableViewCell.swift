//
//  MyRideTableViewCell.swift
//  Infinite Cabs
//
//  Created by Mac on 2/28/20.
//  Copyright © 2020 micro. All rights reserved.
//

import UIKit

class MyRideTableViewCell: UITableViewCell {

    
    @IBOutlet weak var selectCellButton: UIButton!
    
    @IBOutlet weak var bookingidLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var lbltime: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    
    @IBOutlet weak var dropLocationLabel: UILabel!
    @IBOutlet weak var pickupLocationLabel: UILabel!
    
    
    @IBOutlet weak var date2label: UILabel!
    
    @IBOutlet weak var driverlabel: UILabel!
    @IBOutlet weak var reasonLabel: UILabel!
    @IBOutlet weak var cancelByLabel: UILabel!
    @IBOutlet weak var carTypelabel: UILabel!
    @IBOutlet weak var price2label: UILabel!
    
    @IBOutlet weak var botomCardView: CardView!
    @IBOutlet weak var btnCancelBooking: UIButton!
    
    @IBOutlet weak var stackBtnCancel: UIStackView!
    
    @IBOutlet weak var lblCarNumber: UILabel!
    @IBOutlet weak var lblDriverId: UILabel!
    @IBOutlet weak var lblPickUpTime: UILabel!
    @IBOutlet weak var lblDropTime: UILabel!
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        btnCancelBooking.layer.cornerRadius = 4
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
