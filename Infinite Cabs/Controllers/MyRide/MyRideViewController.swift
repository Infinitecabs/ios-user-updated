//
//  MyRideViewController.swift
//  Infinite Cabs
//
//  Created by micro on 18/10/19.
//  Copyright © 2019 micro. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class MyRideViewController: UIViewController {
    
    @IBOutlet weak var btnUpcoming: UIButton!
    @IBOutlet weak var btnCompleted: UIButton!
    @IBOutlet weak var btnCancelled: UIButton!
  
    @IBOutlet weak var viewUpcoming: UIView!
    @IBOutlet weak var viewCompleted: UIView!
    @IBOutlet weak var viewCancelled: UIView!
    
    @IBOutlet weak var containerView: UIView! //F8F9FD old one color code
    
    @IBOutlet weak var tbl: UITableView!
    
    var SelectedView = 1
    
    var data = JSON()
    
    var cancelTag:Int?
    var selectedIndexData = [Int] ()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
//        btnUpcoming.layer.cornerRadius = 12
//        btnCompleted.layer.cornerRadius = 12
//        btnCancelled.layer.cornerRadius = 12
//
//        btnUpcoming.setTitleColor(UIColor.white , for: .normal)
//        btnCompleted.setTitleColor(UIColor.darkGray , for: .normal)
//        btnCancelled.setTitleColor(UIColor.darkGray , for: .normal)
//
//        btnUpcoming.backgroundColor = UIColor(named: "ThemeColor")
//        btnCompleted.backgroundColor = UIColor.white
//        btnCancelled.backgroundColor = UIColor.white
        
        
        
        
        getData()
    }
    
    
    
    @IBAction func selectCellButtonTapped(_ sender: UIButton) {
        
        if( selectedIndexData.contains(sender.tag)){
            
            if let index = selectedIndexData.firstIndex(of: sender.tag) {
                selectedIndexData.remove(at: index)
                tbl.reloadData()
                return
            }
        }
         
        selectedIndexData.append(sender.tag)
        tbl.reloadData()
    }
    
    
    @IBAction func getStartButtonTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    private func getData(){
        
        ActivityIndicatorWithLabel.shared.showProgressView(uiView: self.view)
        //Hud.shared.showHud()
        data = JSON()
        selectedIndexData.removeAll()
        
        let userID = global.userId
        
        let dictKeys = [ "user_id" : userID ] as [String : Any]
        
        var url = ""
            
        if(SelectedView == 1){
            url = Constants.AppLinks.API_BASE_URL + Constants.ApiUrl.userUpcommingRideLIST.rawValue
        }
        
        if(SelectedView == 2){
            url = Constants.AppLinks.API_BASE_URL + Constants.ApiUrl.userCompleteRideLIST.rawValue
                
        }
        
        if(SelectedView == 3){
            url = Constants.AppLinks.API_BASE_URL + Constants.ApiUrl.userCancalTripLIST.rawValue
        }
        
                                               
        Alamofire.request(url , method: .post, parameters: dictKeys, headers:nil).responseJSON(completionHandler: { (response) in
                  
//            Hud.shared.hideHud()
            ActivityIndicatorWithLabel.shared.hideProgressView()
                do {
                    let json: JSON = try JSON(data: response.data!)
                    if (json["statusCode"].stringValue == "200"){
                    self.data = json["data"]
                        DispatchQueue.main.async {
                            self.tbl.reloadData()
                        }
                    
                }
                else{
                    // error
                   // self.showAlert(title: "Error", message: json["message"].stringValue)
                    DispatchQueue.main.async {
                        self.tbl.reloadData()
                    }
                }
                                         
               } catch {
                   // show a alert
//                   Hud.shared.hideHud()
                ActivityIndicatorWithLabel.shared.hideProgressView()
                   self.showAlert(title: "Error", message: "Please check network connection")
               }
        })
    }
    
    
    // MARK:- ACTIONS
    
    @IBAction func btnBackClickedOn(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnUpcomingClicked(_ sender: UIButton) {
        
        SelectedView = 1
        
        btnUpcoming.setTitleColor(UIColor(named: "ThemeColor") , for: .normal)
        btnCompleted.setTitleColor(UIColor(named: "MyRideHeadCLR") , for: .normal)
        btnCancelled.setTitleColor(UIColor(named: "MyRideHeadCLR") , for: .normal)
        
//        btnUpcoming.setTitleColor(UIColor.white , for: .normal)
//        btnCompleted.setTitleColor(UIColor.darkGray , for: .normal)
//        btnCancelled.setTitleColor(UIColor.darkGray , for: .normal)
//
//        btnUpcoming.backgroundColor = UIColor(named: "ThemeColor")
//        btnCompleted.backgroundColor = UIColor.white
//        btnCancelled.backgroundColor = UIColor.white
        
        
        viewUpcoming.backgroundColor = UIColor(named: "ThemeColor")
        viewCompleted.backgroundColor = UIColor.white
        viewCancelled.backgroundColor = UIColor.white
        
        getData()
    }
    
    @IBAction func btnCompletedClicked(_ sender: UIButton) {
        
        SelectedView = 2
        
        btnCompleted.setTitleColor(UIColor(named: "ThemeColor") , for: .normal)
        btnUpcoming.setTitleColor(UIColor(named: "MyRideHeadCLR") , for: .normal)
        btnCancelled.setTitleColor(UIColor(named: "MyRideHeadCLR") , for: .normal)
        
//        btnUpcoming.setTitleColor(UIColor.darkGray , for: .normal)
//        btnCompleted.setTitleColor(UIColor.white , for: .normal)
//        btnCancelled.setTitleColor(UIColor.darkGray , for: .normal)
//
//        btnUpcoming.backgroundColor = UIColor.white
//        btnCompleted.backgroundColor = UIColor(named: "ThemeColor")
//        btnCancelled.backgroundColor = UIColor.white
        
        
        viewUpcoming.backgroundColor = UIColor.white
        viewCompleted.backgroundColor = UIColor(named: "ThemeColor")
        viewCancelled.backgroundColor = UIColor.white
        
        getData()
       
    }
    
    @IBAction func btnCancelledClicked(_ sender: UIButton) {
        
        SelectedView = 3
        
        btnCancelled.setTitleColor(UIColor(named: "ThemeColor") , for: .normal)
        btnUpcoming.setTitleColor(UIColor(named: "MyRideHeadCLR") , for: .normal)
        btnCompleted.setTitleColor(UIColor(named: "MyRideHeadCLR") , for: .normal)
        
//        btnUpcoming.setTitleColor(UIColor.darkGray , for: .normal)
//        btnCompleted.setTitleColor(UIColor.darkGray , for: .normal)
//        btnCancelled.setTitleColor(UIColor.white , for: .normal)
//
//        btnUpcoming.backgroundColor = UIColor.white
//        btnCompleted.backgroundColor = UIColor.white
//        btnCancelled.backgroundColor = UIColor(named: "ThemeColor")
        
        
        
        viewUpcoming.backgroundColor = UIColor.white
        viewCompleted.backgroundColor = UIColor.white
        viewCancelled.backgroundColor = UIColor(named: "ThemeColor")
        
        getData()
    }
    
    
    @objc func btnCancelbookingClicked(_ sender:UIButton){
        
        self.cancelTag = sender.tag
        
        let strbd = staticClass.getStoryboard_Settings()
        let vc = strbd?.instantiateViewController(withIdentifier: "CustomAlertVC") as! CustomAlertVC
        vc.strTitle = "Infinite Cabs"
        vc.strDescription = "Are you sure you want to cancel your ride ?"
        vc.imgPop = #imageLiteral(resourceName: "girl")
        vc.isBtnNoHidden = false
        vc.strBtnOkTitle = "Yes"
        vc.delegate = self
        self.present(vc, animated: true, completion: nil)
        
        
//        let alert = UIAlertController(title: "", message: "Do you want to cancel the ride?", preferredStyle: .alert)
//
//        //actionsheet.view.tintColor = UIColor.black
//
//        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (action) in
//
//            self.cancelBooking(bookingId: self.data[sender.tag]["booking_id"].stringValue)
//
//        }))
//
//        alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
//
//        self.present(alert, animated: true, completion: nil)
        
    }
    
    
    
    func cancelBooking(bookingId:String){
        
//        Hud.shared.showHud()
        ActivityIndicatorWithLabel.shared.showProgressView(uiView: self.view)
        let userID = global.userId
        let dictKeys = [ "user_id" : userID,
                         "driver_id" : "0",
            "booking_id" : bookingId,
            "reasonId" : "0" ,
            "type" : "user"
            ] as [String : Any]
        
        
        let url = Constants.AppLinks.API_BASE_URL + Constants.ApiUrl.declinedBookings.rawValue
        
        Alamofire.request(url , method: .post, parameters: dictKeys, headers:nil).responseJSON(completionHandler: { (response) in
            
            ActivityIndicatorWithLabel.shared.hideProgressView()
           // Hud.shared.hideHud()
            do {
                let json: JSON = try JSON(data: response.data!)
                
                if (json["statusCode"].stringValue == "200"){
                    self.showToast(message: "Booking Cancel Successfully", seconds: 3)
                    self.getData()
                }
                else{
                    // error
                    self.showAlert(title: "Error", message: json["message"].stringValue)
                }
                
            } catch {
                // show a alert
//                Hud.shared.hideHud()
                ActivityIndicatorWithLabel.shared.hideProgressView()
                self.showAlert(title: "Error", message: "Please check network connection")
            }
        })
        
        
    }
    
    
}


extension MyRideViewController : UITableViewDelegate , UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if(data.count > 0){
            return data.count
        }
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
      
        if( selectedIndexData.contains(indexPath.row)){
            /*
            if(SelectedView == 1){
               return 350 + 32 + 40
            }
            if(SelectedView == 2){
                return 350 + 40
            }
            if(SelectedView == 3){
                return 400 + 40
            }*/
            return UITableView.automaticDimension
        }

        return 85
        
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if(data.count == 0){
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "MyRideHeaderTableViewCell") as! MyRideHeaderTableViewCell
            
            if(SelectedView == 1){
                cell.textlabel.text = "You have no upcoming rides."
            }
            
            if(SelectedView == 2){
                cell.textlabel.text = "You have no completed rides."
            }
            
            if(SelectedView == 3){
                cell.textlabel.text = "You have no canceled rides."
            }
            
            cell.getStartButton.layer.cornerRadius = 5
            
            return cell
        }
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyRideTableViewCell") as! MyRideTableViewCell
        
        
        if( selectedIndexData.contains(indexPath.row)){
            cell.botomCardView.isHidden = false
        }else{
            cell.botomCardView.isHidden = true
        }
        
       
        
        
        cell.reasonLabel.isHidden = false
        cell.cancelByLabel.isHidden = false
        //cell.carTypelabel.isHidden = true
        cell.driverlabel.isHidden = false
//        cell.date2label.isHidden = false
        cell.reasonLabel.isHidden = false
        cell.cancelByLabel.isHidden = false
        
        
        
        
        cell.selectCellButton.tag = indexPath.row
        
        
        
        
        if(SelectedView == 1){
//            cell.bookingidLabel.text = "Upcoming Ride"
            cell.bookingidLabel.text = "Booking ID: " + data[indexPath.row]["booking_id"].stringValue
            
           
            // hide
            cell.reasonLabel.isHidden = true
            cell.cancelByLabel.isHidden = true
            //cell.carTypelabel.isHidden = true
            cell.driverlabel.isHidden = true
            
            cell.lblCarNumber.isHidden = true
            cell.lblDriverId.isHidden = true
//            cell.lblPickUpTime.isHidden = true
//            cell.lblDropTime.isHidden = true
//            cell.lbltime.isHidden = false
            cell.price2label.isHidden = true
            
//            cell.dateLabel.text = data[indexPath.row]["book_create_date_time"].stringValue
            
            let strDate = data[indexPath.row]["book_create_date_time"].stringValue
            
            var newStrDate:[String.SubSequence]!
            
            if strDate.contains("-"){
                newStrDate = strDate.split(separator: "-")
            }else{
                newStrDate = strDate.split(separator: " ")
            }
            
            cell.dateLabel.text = "Date: " + newStrDate[0]
            cell.lbltime.text = "Time: " + newStrDate[1]
//            cell.priceLabel.text = "$" + data[indexPath.row]["amount"].stringValue
            cell.priceLabel.text = "$N/A"
            
            cell.dropLocationLabel.text =  data[indexPath.row]["drop_area"].stringValue
            cell.pickupLocationLabel.text = data[indexPath.row]["pickup_area"].stringValue
            
//            cell.date2label.text =  data[indexPath.row]["book_create_date_time"].stringValue
            let cabtype = "Cab Type: " + data[indexPath.row]["car_type"].stringValue
            cell.carTypelabel.text = cabtype == "Cab Type: " ? "Cab Type: N/A" : cabtype
//            cell.price2label.text = "Fare : $" +  data[indexPath.row]["amount"].stringValue
            
            let strScheduleDate = data[indexPath.row]["book_scheduled_time"].stringValue
            if strScheduleDate != ""{
                newStrDate = strScheduleDate.split(separator: " ")
                cell.lblPickUpTime.text = "Pick Up Date & Time: " + newStrDate[0] + "," + newStrDate[1]
            }
//            newStrDate = strScheduleDate.split(separator: " ")
//            cell.lblPickUpTime.text = "Pick Up Date & Time: " + newStrDate[0] + "," + newStrDate[1]  //data[indexPath.row]["book_scheduled_time"].stringValue
            
            cell.lblDropTime.text = "Drop Off Date & Time: " + "N/A"
           
            cell.btnCancelBooking.isHidden = false
            cell.stackBtnCancel.isHidden = false
            cell.btnCancelBooking.tag = indexPath.row
            
            cell.btnCancelBooking.addTarget(self, action: #selector(btnCancelbookingClicked(_:)), for: .touchUpInside)
        }
                   
        if(SelectedView == 2){
            
           
           cell.bookingidLabel.text = "Booking ID: " + data[indexPath.row]["booking_id"].stringValue
            
//            cell.date2label.isHidden = true
            cell.reasonLabel.isHidden = true
            cell.cancelByLabel.isHidden = true
//            cell.lbltime.isHidden = true
            
            cell.lblCarNumber.isHidden = false
            cell.lblDriverId.isHidden = false
//            cell.lblPickUpTime.isHidden = false
//            cell.lblDropTime.isHidden = false
            cell.dateLabel.isHidden = false
            cell.price2label.isHidden = false
            
            let strDate = data[indexPath.row]["book_create_date_time"].stringValue
            
            var newStrDate:[String.SubSequence]!
            
            if strDate.contains("-"){
                newStrDate = strDate.split(separator: "-")
            }else{
                newStrDate = strDate.split(separator: " ")
            }
            
            cell.dateLabel.text = "Date: " + newStrDate[0]
            cell.lbltime.text = "Time: " + newStrDate[1]
            
            cell.priceLabel.text = "$" + data[indexPath.row]["amount"].stringValue
            
                   
            
            cell.dropLocationLabel.text = data[indexPath.row]["drop_area"].stringValue
            cell.pickupLocationLabel.text = data[indexPath.row]["pick_address"].stringValue
                   
            
            cell.price2label.text = "Total Fare: $" +  data[indexPath.row]["amount"].stringValue
            
            
            cell.driverlabel.text = "Driver Name: " + data[indexPath.row]["driver_name"].stringValue
            
            
            let cabtype = "Cab Type: " + data[indexPath.row]["car_type"].stringValue
            cell.carTypelabel.text = cabtype == "Cab Type: " ? "Cab Type: N/A" : cabtype
            
            let cabno = "Cab Number: " + data[indexPath.row]["driver_car_number"].stringValue
            cell.lblCarNumber.text = cabno == "Cab Number: " ? "Cab Number: N/A" : cabno
            
//            cell.lblCarNumber.text = "Cab Number: " + data[indexPath.row]["driver_car_number"].stringValue
            cell.lblDriverId.text = "Driver ID: " + data[indexPath.row]["driver_id"].stringValue
            cell.lblPickUpTime.text = "Pick Up Date & Time: " + data[indexPath.row]["pickupDateTime"].stringValue
            cell.lblDropTime.text = "Drop Off Date & Time: " + data[indexPath.row]["dropTime"].stringValue
            
            
            
            cell.btnCancelBooking.isHidden = true
            cell.stackBtnCancel.isHidden = true
        }
        if(SelectedView == 3){
            
            cell.lblCarNumber.isHidden = false
            cell.lblDriverId.isHidden = false
//            cell.lblPickUpTime.isHidden = false
//            cell.lblDropTime.isHidden = false
//            cell.lbltime.isHidden = true
            cell.price2label.isHidden = false
            
            cell.bookingidLabel.text = "Booking ID: " + data[indexPath.row]["booking_id"].stringValue
            
//            cell.date2label.isHidden = true
            
//            cell.dateLabel.text = "Date: " + data[indexPath.row]["book_create_date_time"].stringValue
            
            let strDate = data[indexPath.row]["book_create_date_time"].stringValue
            
            var newStrDate:[String.SubSequence]!
            
            if strDate.contains("-"){
                newStrDate = strDate.split(separator: "-")
            }else{
                newStrDate = strDate.split(separator: " ")
            }
            
            cell.dateLabel.text = "Date: " + newStrDate[0]
            cell.lbltime.text = "Time: " + newStrDate[1]
            
            cell.priceLabel.text = "$N/A"
                   
            cell.dropLocationLabel.text = data[indexPath.row]["drop_area"].stringValue
            cell.pickupLocationLabel.text = data[indexPath.row]["pickup_area"].stringValue
                   
            cell.date2label.text =  data[indexPath.row]["book_create_date_time"].stringValue
//            cell.price2label.text = "Total Fare : $" +  data[indexPath.row]["amount"].stringValue
            cell.price2label.text = "Total Fare: $N/A"
            
            cell.reasonLabel.text = "Reason: " + data[indexPath.row]["cancel"].stringValue
            cell.cancelByLabel.text = "Cancelled By: " + data[indexPath.row]["status_code"].stringValue
            
            cell.driverlabel.text = "Driver Name: " + data[indexPath.row]["driver_name"].stringValue
           cell.carTypelabel.text = "Cab Type : " + data[indexPath.row]["car_type"].stringValue
            
            cell.lblCarNumber.text = "Cab Number: " + data[indexPath.row]["driver_car_number"].stringValue
            cell.lblDriverId.text = "Driver ID: " + data[indexPath.row]["driver_id"].stringValue
            cell.lblPickUpTime.text = "Pick Up Date & Time: " + data[indexPath.row]["pickupDateTime"].stringValue
            cell.lblDropTime.text = "Drop Off Date & Time: " + data[indexPath.row]["dropTime"].stringValue
            
            
            
            cell.btnCancelBooking.isHidden = true
            cell.stackBtnCancel.isHidden = true
       }
                   
        
        
        return cell
    }
    
}


extension MyRideViewController:customeAlertDelegate{
    
    func btnOkClicked() {
        
        self.cancelBooking(bookingId: self.data[self.cancelTag!]["booking_id"].stringValue)
        
    }
    
}
