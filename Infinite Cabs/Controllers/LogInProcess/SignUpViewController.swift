//
//  SignUpViewController.swift
//  Infinite Cabs
//
//  Created by micro on 17/10/19.
//  Copyright © 2019 micro. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import SKCountryPicker

class SignUpViewController: UIViewController , UITextViewDelegate , UITextFieldDelegate {
    
    //Outlets
    
    @IBOutlet weak var txtFirstName: SkyFloatingLabelTextField!
    @IBOutlet weak var txtLastName: SkyFloatingLabelTextField!
    @IBOutlet weak var txtEmail: SkyFloatingLabelTextField!
    @IBOutlet weak var txtMobile: SkyFloatingLabelTextField!
    @IBOutlet weak var txtPassword: SkyFloatingLabelTextField!
    @IBOutlet weak var txtConfirmPassword: SkyFloatingLabelTextField!
    @IBOutlet weak var txtViewAddress: UITextView!
    @IBOutlet weak var txtGender: SkyFloatingLabelTextField!
    
    @IBOutlet weak var subView: UIView!
    
    @IBOutlet weak var addressLabel: UILabel!
    
    @IBOutlet weak var addressView: UIView!
    
    @IBOutlet weak var imgTerms: UIImageView!
    @IBOutlet weak var btnRegister: UIButton!
    @IBOutlet weak var btnTermsCond: UIButton!
    
    
   // @IBOutlet weak var pickerView: UIPickerView!
    
    
    @IBOutlet weak var passwordEyeButton: UIButton!
    @IBOutlet weak var confirmPasswordEyeButton: UIButton!
    
    @IBOutlet weak var imgFlag: UIImageView!
    @IBOutlet weak var lblCountryCode: UILabel!
    @IBOutlet weak var lblAddressPlaceHolder: UILabel!
    
    
    var userEmailText = ""
    var countryName = "Australia"
    var isPasswordIsVisible =  false
    var isConfirmPasswordIsVisible =  false
    
    var arrayGender = [String]()
    
    var toolBar = UIToolbar()
    var picker  = UIPickerView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        txtEmail.text = userEmailText
        
        setup_UI()
        
        arrayGender = ["Select Gender","Male","Female"]
        
       
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(false)
        self.navigationController?.isNavigationBarHidden = true
        //view.endEditing(true)
    }
    
    // Set Up Initial UI
    
    func setup_UI(){
        
        //self.hideKeyboardWhenTappedAroundOnView()
        addressView.backgroundColor  = .lightGray
        
        
        btnRegister.layer.cornerRadius = 4
        
        
        txtViewAddress.delegate = self
        
        txtFirstName.delegate = self
        txtLastName.delegate = self
        txtEmail.delegate = self
        txtMobile.delegate = self
        txtPassword.delegate = self
        self.txtGender.delegate = self
        
        txtViewAddress.text = "Enter Your Address"
        txtViewAddress.textColor = UIColor.lightGray
        
        self.imgTerms.image = UIImage(named: "Rectangle 4")
        
        guard let country = CountryManager.shared.currentCountry else {
            
            lblCountryCode.text = "+61"
            imgFlag.image = UIImage(named: "australiyaflag")
            
            return
        }
        
        lblCountryCode.text = country.dialingCode
        imgFlag.image = country.flag
        countryName = country.countryName
        
        
    }
    
    
    @objc func onDoneButtonTapped() {
        toolBar.removeFromSuperview()
        picker.removeFromSuperview()
    }
    
    // Hide KeyBoard when touch around
    
    func hideKeyboardWhenTappedAroundOnView() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard2))
        
        tap.cancelsTouchesInView = false
        subView.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard2() {
        addressView.backgroundColor = .gray
        addressLabel.textColor = .gray
        view.endEditing(true)
    }
    
    
    // MARK:- ACTIONS
    
    @IBAction func btnLogInClicked(_ sender: UIButton) {
        
//        let vc = self.storyboard?.instantiateViewController(withIdentifier: "LogInViewController") as! LogInViewController
//        self.navigationController?.pushViewController(vc, animated: true)
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    func memberregister(){
        //        if(Constants.AppLinks.APP_MODE == Constants.AppMode.Navigation.rawValue)
        //        {
        //            return
        //        }
        
        txtFirstName.text = txtFirstName.text?.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
        
        txtLastName.text = txtLastName.text?.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
        
        txtEmail.text = txtEmail.text?.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
        txtGender.text = txtGender.text?.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
        txtMobile.text = txtMobile.text?.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
        txtPassword.text = txtPassword.text?.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
        txtConfirmPassword.text = txtConfirmPassword.text?.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
        txtViewAddress.text = txtViewAddress.text?.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
        
        if(txtFirstName.text!.isEmpty ==  true)
        {
            HelpingMethod.shared.presentAlertWithTitle(title: "Error", message: Constants.errorMessage.FirstNameEmpty.rawValue, vc: self)
        }
            //        else if(!(txtName.text!.count >= 2))
            //        {
            //            HelpingMethod.shared.presentAlertWithTitle(title: "Error", message: Constants.errorMessage.NameLimit.rawValue, vc: self)
            //        }
            
            
            /*
             else if(txtCountryCode.text?.isEmpty ==  true)
             {
             HelpingMethod.shared.presentAlertWithTitle(title: "Error", message: Constants.errorMessage.CountryCodeEmpty.rawValue, vc: self)
             }
             */
        else if(txtLastName.text?.isEmpty ==  true)
        {
            HelpingMethod.shared.presentAlertWithTitle(title: "Error", message: Constants.errorMessage.LastNameEmpty.rawValue, vc: self)
        }
            
        else if(txtEmail.text?.isEmpty ==  true)
        {
            HelpingMethod.shared.presentAlertWithTitle(title: "Error", message: Constants.errorMessage.EmailEmpty.rawValue, vc: self)
        }
//        else if(txtGender.text?.isEmpty ==  true)
//        {
//            HelpingMethod.shared.presentAlertWithTitle(title: "Error", message: Constants.errorMessage.GenderEmpty.rawValue, vc: self)
//        }
        else if(HelpingMethod.shared.VaildateTxtEmail(strEmail: txtEmail.text!) ==  false)
        {
            HelpingMethod.shared.presentAlertWithTitle(title: "Error", message: Constants.errorMessage.EmailInvalid.rawValue, vc: self)
        }
        else if(txtMobile.text?.isEmpty ==  true)
        {
            HelpingMethod.shared.presentAlertWithTitle(title: "Error", message: Constants.errorMessage.MobileNoEmpty.rawValue, vc: self)
        }
        else if(txtMobile.text!.count < 9 || txtMobile.text!.count > 10)
        {
            HelpingMethod.shared.presentAlertWithTitle(title: "Error", message: Constants.errorMessage.MobileNoInvalid.rawValue, vc: self)
        }
            
        else if(txtPassword.text?.isEmpty ==  true)
        {
            HelpingMethod.shared.presentAlertWithTitle(title: "Error", message: Constants.errorMessage.PasswordEmpty.rawValue, vc: self)
        }
        else if(txtConfirmPassword.text?.isEmpty ==  true)
        {
            HelpingMethod.shared.presentAlertWithTitle(title: "Error", message: Constants.errorMessage.ConfirmPasswordEmpty.rawValue, vc: self)
        }else if txtPassword.text! != txtConfirmPassword.text{
            
            HelpingMethod.shared.presentAlertWithTitle(title: "Error", message: Constants.errorMessage.PasswordDonNotMatch.rawValue, vc: self)
        }
           
            
            
            //            else if(txtPassword.text!.count < 8 || txtPassword.text!.count > 16)
            //            {
            //                HelpingMethod.shared.presentAlertWithTitle(title: "Error", message: Constants.errorMessage.PasswordMinimum.rawValue, vc: self)
            //            }
        else if(txtViewAddress.text?.isEmpty ==  true || txtViewAddress.text == "Enter Your Address" )
        {
            HelpingMethod.shared.presentAlertWithTitle(title: "Error", message: Constants.errorMessage.Address.rawValue, vc: self)
        }
        else if( self.imgTerms.image == UIImage(named: "Rectangle 4"))
        {
            HelpingMethod.shared.presentAlertWithTitle(title: "Error", message: Constants.errorMessage.TermsAndConditions.rawValue, vc: self)
        }
        else
        {
            //self.btnRegister.isUserInteractionEnabled = false
            methodUserSignUpProcess()
            //
        }
        
    }
    
        func methodUserSignUpProcess()
        {
            
//            Hud.shared.showHud()
            ActivityIndicatorWithLabel.shared.showProgressView(uiView: self.view)
            
            let FirstName = self.txtFirstName.text
            let LastName = self.txtLastName.text
            let Email = self.txtEmail.text
            let strPassword = self.txtPassword.text
            let ViewAddress = self.txtViewAddress.text
            let Mobilenumber = self.txtMobile.text
            let deviceToken = global.deviceToken
            let gender = "0"//self.txtGender.text
            
            
            let deviceId = UIDevice.current.identifierForVendor!.uuidString
                  print(deviceId)  //0AE3ABEA-A65A-4CF3-A0E2-D7A38A13D6DE
            UserDefaults.standard.set(deviceId, forKey:"deviceId")
            
            
        let dictKeys = ["first_name" : FirstName,"last_name": LastName,
                        "password": strPassword,"email": Email,"address":ViewAddress,"mobile":Mobilenumber,"deviceToken":deviceToken,"gender":gender,"phoneCode":self.lblCountryCode.text,"country":countryName] as! [String:String]
             print(dictKeys as Any)
           // var data : Data!
//            if(self.isImageUpload)
//            {
//                data = imgProfile.image?.jpegData(compressionQuality: 0.6)
//            }
            
            DispatchQueue.global(qos: .background).async
                {
                    
                   ApiLibrary.shared.APICallingWithParameters(postDictionary: dictKeys, strApiUrl: Constants.ApiUrl.SignUp.rawValue, in: self) { (response,status,message) in
                    
//                    Hud.shared.hideHud()
                    ActivityIndicatorWithLabel.shared.hideProgressView()
                        
                    self.btnRegister.isUserInteractionEnabled = true
                    
                
                    
                   guard  let res = response as? NSDictionary else
                    {
                        return
                    }
                    
                    let statusCode = "\(String(describing: res["statusCode"]!))"
                    let strMessage = res["message"] as! String
                    
                    
                    if statusCode == "200"
                    {
                        
                        if let data = res["data"] as? NSArray {

                            for item in data{
                                
                                let dict = item as! NSDictionary
                              
                                global.userId = dict["id"] as! String
                                global.userMobileNumber = dict["mobile"] as! String
                                global.userCountryCode = dict["phoneCode"] as! String
                                global.userEmail = dict["email"] as! String
                                global.userFirstName = dict["first_name"] as! String
                                global.userLastName = dict["last_name"] as! String
                                
                                
//                                let payMode = dict["paywith"] as! String
                                
                               

                                    UserDefaults.standard.set("Pay the Driver Directly", forKey: "PAYMENT")
                                    UserDefaults.standard.set("", forKey: "CARDID")
                                  
                                
                            }
                            
                            self.showToast(message: "Sign Up Successfully", seconds: 3)
                            self.shoeVerifyPhoneVC()
                        }
                    
                    }
                    else
                    {
//                        Hud.shared.hideHud()
                        ActivityIndicatorWithLabel.shared.hideProgressView()
                        HelpingMethod.shared.presentAlertWithTitle(title: "Error!", message: strMessage, vc: self)
                    }
                }
            }
        }
        
    
    @IBAction func passwordEyeButtonTapped(_ sender: Any) {
            
            
            if(isPasswordIsVisible){
                // make password inVisible and change image
                txtPassword.isSecureTextEntry = true
                passwordEyeButton.setImage(UIImage(named: "icon_eye_slash_password"), for: .normal)
                
    //            if #available(iOS 13.0, *) {
    //                let image = UIImage(systemName: "eye.slash.fill")
    //                passwordEyeButton.setImage(image, for: .normal)
    //
    //            }
            }else{
                // make password visible and change image
                txtPassword.isSecureTextEntry = false
                passwordEyeButton.setImage(UIImage(named: "icon_eye_password"), for: .normal)
            
            }
            
            
            isPasswordIsVisible = !isPasswordIsVisible
            
        }
    
    
    
    
    @IBAction func btnConfirmPassEyeBittonTapped(_ sender: UIButton) {
        
        if(isConfirmPasswordIsVisible){
            // make password inVisible and change image
            txtConfirmPassword.isSecureTextEntry = true
            confirmPasswordEyeButton.setImage(UIImage(named: "icon_eye_slash_password"), for: .normal)
            
        }else{
            // make password visible and change image
            txtConfirmPassword.isSecureTextEntry = false
            confirmPasswordEyeButton.setImage(UIImage(named: "icon_eye_password"), for: .normal)
            
        }
        
        
        isConfirmPasswordIsVisible = !isConfirmPasswordIsVisible
        
        
    }
    
    
    
    func shoeVerifyPhoneVC(){
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1.5) {
            let strbdMap = staticClass.getStoryboard_Main()
            let vc = strbdMap?.instantiateViewController(withIdentifier: "VerifyPhoneVC") as! VerifyPhoneVC
            vc.isWorkingVC = "signup"
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    
    
    @IBAction func btnBackClicked(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func btnTermsCondClicked(_ sender: UIButton) {
        
       
        if self.imgTerms.image == UIImage(named: "Rectangle 4"){
            
            self.imgTerms.image = UIImage(named: "check-mark")
        }else{
            
            self.imgTerms.image = UIImage(named: "Rectangle 4")
        }
        
    }
    
    
    
    @IBAction func btnTermConditionLinkClicked(_ sender: UIButton) {
        
        let strbd = staticClass.getStoryboard_Settings()
        let vc = strbd?.instantiateViewController(withIdentifier: "LegalVC") as! LegalVC
        
        vc.isPrivacy = false
        vc.isTermsCond = true
        vc.isAboutUs = false
        
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    
    
    @IBAction func btnSelectCountryClicked(_ sender: UIButton) {
        
       let _ = CountryPickerController.presentController(on: self) { (country) in
            self.imgFlag.image = country.flag
            self.lblCountryCode.text = country.dialingCode
            self.countryName = country.countryName
        }
        
//        countryController.flagStyle = .corner
//        countryController.isCountryDialHidden = true
        
        
    }
    
    
    
    
    @IBAction func btnRegisterClicked(_ sender: UIButton) {
        
        memberregister()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
    }
    
    
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        
        if text == "\n" {
            textView.resignFirstResponder()
            return false
        }
        
        return true
    }
    
    // MARK:- TextView Delegate and Datasource
    
     func textViewDidBeginEditing(_ textView: UITextView) {
        
        if txtViewAddress.textColor == UIColor.lightGray {
            
            txtViewAddress.text = ""
            addressLabel.isHidden = false
            addressView.backgroundColor = #colorLiteral(red: 0.9868121743, green: 0.6996515989, blue: 0.12979725, alpha: 1)
            addressLabel.textColor = #colorLiteral(red: 0.9868121743, green: 0.6996515989, blue: 0.12979725, alpha: 1)
            txtViewAddress.textColor = UIColor.black
            
//            lblAddressPlaceHolder.isHidden = true
        }
    }
    
  /*  func textFieldDidBeginEditing(_ textField: UITextField) {
        addressView.backgroundColor = .lightGray
        addressLabel.textColor = .lightGray
        
        if textField == txtGender{
            txtEmail.resignFirstResponder()
            //self.view.endEditing(true)
            
            picker = UIPickerView.init()
            picker.delegate = self
            picker.backgroundColor = UIColor.lightGray//UIColor(named: "ThemeColor")
            picker.setValue(UIColor.black, forKey: "textColor")
            picker.autoresizingMask = .flexibleWidth
            picker.contentMode = .center
            picker.frame = CGRect.init(x: 0.0, y: UIScreen.main.bounds.size.height - 300, width: UIScreen.main.bounds.size.width, height: 300)
            
            toolBar = UIToolbar.init(frame: CGRect.init(x: 0.0, y: UIScreen.main.bounds.size.height - 300, width: UIScreen.main.bounds.size.width, height: 50))
            toolBar.barStyle = .default
            toolBar.items = [UIBarButtonItem.init(title: "Done", style: .done, target: self, action: #selector(onDoneButtonTapped))]
            
            self.view.addSubview(picker)
            self.view.addSubview(toolBar)
           // pickerView.isHidden = false
            
        }
        
    }*/
    

    
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        addressView.backgroundColor = .lightGray
        addressLabel.textColor = .lightGray
        
        toolBar.removeFromSuperview()
        picker.removeFromSuperview()
        
        if textField == txtGender{
            
            //txtEmail.resignFirstResponder()
            self.view.endEditing(true)
            
            picker = UIPickerView.init()
            picker.delegate = self
            picker.backgroundColor = UIColor.lightGray//UIColor(named: "ThemeColor")
            picker.setValue(UIColor.black, forKey: "textColor")
            picker.autoresizingMask = .flexibleWidth
            picker.contentMode = .center
            picker.frame = CGRect.init(x: 0.0, y: UIScreen.main.bounds.size.height - 300, width: UIScreen.main.bounds.size.width, height: 300)
            
            toolBar = UIToolbar.init(frame: CGRect.init(x: 0.0, y: UIScreen.main.bounds.size.height - 300, width: UIScreen.main.bounds.size.width, height: 50))
            toolBar.barStyle = .default
            toolBar.items = [UIBarButtonItem.init(title: "Done", style: .done, target: self, action: #selector(onDoneButtonTapped))]
            
            self.view.addSubview(picker)
            self.view.addSubview(toolBar)
           // pickerView.isHidden = false
            return false
        }
        return true
    }
    
    
     func textViewDidEndEditing(_ textView: UITextView) {
        
        if txtViewAddress.text == "" {
            txtViewAddress.text = "Enter Your Address"
            addressLabel.isHidden = true
            
            txtViewAddress.textColor = UIColor.lightGray
            
//            lblAddressPlaceHolder.isHidden = false
            
        }
        
    }
    
    
    
    
}


//MARK:- Picker View Delegate And Datasource

extension SignUpViewController:UIPickerViewDataSource,UIPickerViewDelegate{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrayGender.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
       
        return arrayGender[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        self.txtGender.text = row == 0 ? "" : arrayGender[row]
       // self.pickerView.isHidden = true
    }
    
}

