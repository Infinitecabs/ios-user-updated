//
//  FirstLogInViewController.swift
//  Infinite Cabs
//
//  Created by Mac on 2/10/20.
//  Copyright © 2020 micro. All rights reserved.
//

import UIKit

class FirstLogInViewController: UIViewController , UITextFieldDelegate {

    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var crossButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        loginButton.layer.cornerRadius = 5
        emailTextField.delegate = self
        
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(false)
        self.navigationController?.isNavigationBarHidden = true
        view.endEditing(true)
        
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
            
        if(textField.text?.count ?? 0 == 1 && string == "" ){
            crossButton.isHidden = true
            return true
        }
            
        if(string != ""){
            crossButton.isHidden = false
        }
        
        return true
    }
    
    
    @IBAction func removeTextButtonTapped(_ sender: Any) {
        emailTextField.text = ""
        crossButton.isHidden = true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
    }
    
    
    @IBAction func loginButtonTapped(_ sender: Any) {
        //FIXME:
       
        
        let nameEmail = emailTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        
        if nameEmail != ""{
            
            if (nameEmail?.first!.isNumber)!{
                
                methodEmailExistOrNotInProcess()
                
            }else{
                
                if(validateEmail(emailTextField.text ?? "")){
                    
                    methodEmailExistOrNotInProcess()
                }
            }
            
            
        }else{
            
            HelpingMethod.shared.presentAlertWithTitle(title: "Error", message: Constants.errorMessage.EmailPhoneEmpty.rawValue, vc: self)
        }
        
    }
    
    @IBAction func signUpButtonTapped(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SignUpViewController") as! SignUpViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    
    //MARK:- Check email exist or not api
      func methodEmailExistOrNotInProcess()
      {
//          Hud.shared.showHud()
        ActivityIndicatorWithLabel.shared.showProgressView(uiView: self.view)
          
          let strEmail = self.emailTextField.text
        
          
          let  dictKeys = ["email": strEmail ] as! [String:String]
          print(dictKeys as Any)
          DispatchQueue.global(qos: .background).async
              {
                  
                  ApiLibrary.shared.APICallingWithParameters(postDictionary: dictKeys, strApiUrl: Constants.ApiUrl.EmailExistOrNot.rawValue, in: self) { (response,status,message) in
                      
                      
                      guard  let res = response as? NSDictionary else
                      {
//                          Hud.shared.hideHud()
                        ActivityIndicatorWithLabel.shared.hideProgressView()
                        
                          return
                      }
                    
                      let statusCode = "\(String(describing: res["statusCode"]!))"
                      let strMessage = res["message"] as! String
                     
                    
                      if statusCode == "200"
                      {
//                          Hud.shared.hideHud()
                        ActivityIndicatorWithLabel.shared.hideProgressView()
                          // show login screen
                        self.showLoginViewController()
                      }
                    else if statusCode == "201"
                    {
//                        Hud.shared.hideHud()
                        ActivityIndicatorWithLabel.shared.hideProgressView()
                        // show sign Up (registration) screen
                        self.showRegistrationViewController()
                    }
                        
                      else
                      {
//                          Hud.shared.hideHud()
                        ActivityIndicatorWithLabel.shared.hideProgressView()
                          HelpingMethod.shared.presentAlertWithTitle(title: "Error!", message: strMessage, vc: self)
                      }
                      
                  }
          }
      }
    
    
    private func showLoginViewController(){
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "LogInViewController") as! LogInViewController
        vc.userEmail = emailTextField.text ?? ""
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    private func showRegistrationViewController(){
           let vc = self.storyboard?.instantiateViewController(withIdentifier: "SignUpViewController") as! SignUpViewController
            vc.userEmailText = emailTextField.text ?? ""
           self.navigationController?.pushViewController(vc, animated: true)
       }
    
    
}
