//
//  ForgotPasswordVC.swift
//  Infinite Cabs
//
//  Created by micro on 17/10/19.
//  Copyright © 2019 micro. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SKCountryPicker


class ForgotPasswordVC: UIViewController , UITextFieldDelegate {
    
    // Outlets
    @IBOutlet weak var txtPhoneNumber: UITextField!
    @IBOutlet weak var btnSubmit: UIButton!
    
    @IBOutlet weak var crossButton: UIButton!
    
    
    @IBOutlet weak var txtOtp1: UITextField!
    @IBOutlet weak var txtOtp2: UITextField!
    @IBOutlet weak var txtOtp3: UITextField!
    @IBOutlet weak var txtOtp4: UITextField!
    @IBOutlet weak var txtOtp5: UITextField!
    @IBOutlet weak var stackOtp: UIStackView!
    
    
    @IBOutlet weak var imgFlag: UIImageView!
    @IBOutlet weak var lblCountryCode: UILabel!
    
    
    var strOtp = ""
    var countryName = "Australia"
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        self.hideKeyboardWhenTappedAround()
        
        initialSetUp()
        // Do any additional setup after loading the view.
    }
    
    func initialSetUp(){
        
        txtPhoneNumber.delegate = self
        btnSubmit.layer.cornerRadius = 4

        txtOtp1.delegate = self
        txtOtp2.delegate = self
        txtOtp3.delegate = self
        txtOtp4.delegate = self
        txtOtp5.delegate = self
        
        
        txtOtp1.setBottomBorder(borderColor: UIColor(named: "ThemeColor")!.cgColor)
        txtOtp2.setBottomBorder(borderColor: UIColor(named: "ThemeColor")!.cgColor)
        txtOtp3.setBottomBorder(borderColor: UIColor(named: "ThemeColor")!.cgColor)
        txtOtp4.setBottomBorder(borderColor: UIColor(named: "ThemeColor")!.cgColor)
        txtOtp5.setBottomBorder(borderColor: UIColor(named: "ThemeColor")!.cgColor)
        
        stackOtp.isHidden = true
        
        guard let country = CountryManager.shared.currentCountry else {
                   
                   lblCountryCode.text = "+61"
                   imgFlag.image = UIImage(named: "australiyaflag")
                   
                   return
               }
               
               lblCountryCode.text = country.dialingCode
               imgFlag.image = country.flag

        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(false)
        self.navigationController?.isNavigationBarHidden = true
        view.endEditing(true)
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == txtPhoneNumber{
            
            if(textField.text?.count ?? 0 == 1 && string == "" ){
                crossButton.isHidden = true
                return true
            }
                
            if(string != ""){
                crossButton.isHidden = false
            }
            
            
            return true
            
        }
        
        else{
            
            let allowedCharacters = CharacterSet(charactersIn:"0123456789")//Here change this characters based on your requirement
            let characterSet = CharacterSet(charactersIn: string)
                  
            if(string.count > 1){
                 return false
            }
            
            if(string == "")
                {
                return true
                }
            if (1 <= textField.text?.count ?? 0){
                
                  textField.text = ""
                    return true
                }
                  
            return allowedCharacters.isSuperset(of: characterSet)
            
        }
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
    }

    
    func textFieldDidChangeSelection(_ textField: UITextField) {
        if textField.text != ""{
            
            if(textField == txtOtp1){
                txtOtp2.becomeFirstResponder()
            }
            else if(textField == txtOtp2){
                txtOtp3.becomeFirstResponder()
            }
            else if(textField == txtOtp3){
                txtOtp4.becomeFirstResponder()
            }
            else if(textField == txtOtp4){
                txtOtp5.becomeFirstResponder()
            }
            else if(textField == txtOtp5){
                resignFirstResponder()
            }
            
        }
        
    }
    
    
    
    //MARK:- ACTIONS
    
    
    @IBAction func btnSelectCountryClicked(_ sender: UIButton) {
           
          let _ = CountryPickerController.presentController(on: self) { (country) in
               self.imgFlag.image = country.flag
               self.lblCountryCode.text = country.dialingCode
               self.countryName = country.countryName
           }
           
   //        countryController.flagStyle = .corner
   //        countryController.isCountryDialHidden = true
           
           
       }
    
    
    @IBAction func btnSubmitClicked(_ sender: UIButton)
    {
        if stackOtp.isHidden{
            if(validateMobileNumber(txtPhoneNumber.text ?? "")){
                 callForgetPasswordApi()
            }
        }else{
            
               
            //otp verification
            strOtp = ""
            
            strOtp.append(txtOtp1.text ?? "")
            strOtp.append(txtOtp2.text ?? "")
            strOtp.append(txtOtp3.text ?? "")
            strOtp.append(txtOtp4.text ?? "")
            strOtp.append(txtOtp5.text ?? "")
            
            
            if(strOtp.count == 5){
                // call api
                verifyOtp()
                
            }else{
                // show alert
                showAlert(title: "Please enter full OTP", message: "")
            }
            
        }
        
        
    }
    
    @IBAction func btnRemoveEmailClicked(_ sender: UIButton) {
        crossButton.isHidden = true
        txtPhoneNumber.text = ""
    }
    
    
    @IBAction func btnBackClicked(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    
    
    
    func methodRedirectToOtpVc()
    {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "VerifyPhoneVC") as! VerifyPhoneVC
        vc.isWorkingVC = "password"
        self.navigationController?.pushViewController(vc, animated: true)
    }

    
    
    func callForgetPasswordApi(){
             
//        Hud.shared.showHud()
        ActivityIndicatorWithLabel.shared.showProgressView(uiView: self.view)
        let userID = global.userId
        let dictKeys = [ "phone" : txtPhoneNumber.text ?? "","phoneCode":self.lblCountryCode.text!,"country":countryName ] as [String : Any]


        let url = Constants.AppLinks.API_BASE_URL + Constants.ApiUrl.forgot_password.rawValue//Constants.ApiUrl.forgot_password.rawValue

        Alamofire.request(url , method: .post, parameters: dictKeys, headers:nil).responseJSON(completionHandler: { (response) in

//                 Hud.shared.hideHud()
            ActivityIndicatorWithLabel.shared.hideProgressView()
            do {
                let json: JSON = try JSON(data: response.data!)
                if (json["statusCode"].stringValue == "200"){

                    global.userId = json["userId"].stringValue
//                    self.methodRedirectToOtpVc()
                    self.stackOtp.isHidden = false
                    self.txtPhoneNumber.isUserInteractionEnabled = false
                     
                }
                else{
                    // error
                    self.showAlert(title: "Error", message: json["message"].stringValue)
                }

            } catch {
                    // show a alert
//                    Hud.shared.hideHud()
                ActivityIndicatorWithLabel.shared.hideProgressView()
                    self.showAlert(title: "Error", message: "Please check network connection")
            }
        })
    }
    
    
    func showPopUp(){
        
        let strbd = staticClass.getStoryboard_Settings()
        let vc = strbd?.instantiateViewController(withIdentifier: "CustomAlertVC") as! CustomAlertVC
        vc.delegate = self
        vc.imgPop = #imageLiteral(resourceName: "check_ic")
        vc.strTitle = "Infinite Cabs"
        vc.strDescription = "We sent your new password via SMS to mobile number you entered."
        self.present(vc, animated: true, completion: nil)
        
    }
    
    
    
    
    private func verifyOtp(){
        
        ActivityIndicatorWithLabel.shared.showProgressView(uiView: self.view)
        
        let userID = global.userId
        let dictKeys = [ "userId" : userID ,
                         "otp" : strOtp ] as [String : Any]
        
        let url = Constants.AppLinks.API_BASE_URL + Constants.ApiUrl.veriftOtpforpassword.rawValue//Constants.ApiUrl.veriftOtpforpassword.rawValue
        
        Alamofire.request(url , method: .post, parameters: dictKeys, headers:nil).responseJSON(completionHandler: { (response) in
            
            //              Hud.shared.hideHud()
            ActivityIndicatorWithLabel.shared.hideProgressView()
            do {
                let json: JSON = try JSON(data: response.data!)
                if (json["statusCode"].stringValue == "200"){
                    
                    
//                    self.showToast(message: "We send your new password in SMS", seconds: 3)
//                    self.showToast(message: "We sent your new password via SMS to mobile number you entered.", seconds: 3)
                    
                    DispatchQueue.main.async {
                        self.showPopUp()
                    }
                    
                    
                }
                else{
                    // error
                    self.showAlert(title: "Error", message: json["message"].stringValue)
                }
                
            } catch {
                // show a alert
                //                      Hud.shared.hideHud()
                ActivityIndicatorWithLabel.shared.hideProgressView()
                self.showAlert(title: "Error", message: "Please check network connection")
            }
        })
    }
   
}


extension ForgotPasswordVC:customeAlertDelegate
{
    
    func btnOkClicked() {
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.5) {
            
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: FirstLogInViewController.self) {
                    self.navigationController!.popToViewController(controller, animated: true)
                    break
                }
            }
        }
        
    }
    
    
}
