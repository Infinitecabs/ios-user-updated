//
//  VerifyPhoneVC.swift
//  Infinite Cabs
//
//  Created by micro on 17/10/19.
//  Copyright © 2019 micro. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class VerifyPhoneVC: UIViewController , UITextFieldDelegate {
    
    // Outlets
    
    @IBOutlet weak var txtOtp1: UITextField!
    @IBOutlet weak var txtOtp2: UITextField!
    @IBOutlet weak var txtOtp3: UITextField!
    @IBOutlet weak var txtOtp4: UITextField!
    @IBOutlet weak var txtOtp5: UITextField!
    
    
    @IBOutlet weak var btnVerify: UIButton!
    
    var strOtp = ""
    
    var isWorkingVC = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setup_UI()
        
        txtOtp1.delegate = self
        txtOtp2.delegate = self
        txtOtp3.delegate = self
        txtOtp4.delegate = self
        txtOtp5.delegate = self
        txtOtp1.becomeFirstResponder()
        
        if ( isWorkingVC == "login" ) {
            // send otp
            resendOtp()
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(false)
        
        self.navigationController?.isNavigationBarHidden = true
    }
    
    // Set Up Initial UI
    
    func setup_UI(){
        
        self.hideKeyboardWhenTappedAround()
        txtOtp1.setBottomBorder()
        txtOtp2.setBottomBorder()
        txtOtp3.setBottomBorder()
        txtOtp4.setBottomBorder()
        txtOtp5.setBottomBorder()
        btnVerify.layer.cornerRadius = 4
        
    }
    
    
    
    
    func textFieldDidChangeSelection(_ textField: UITextField) {
        if textField.text != ""{
            
            if(textField == txtOtp1){
                txtOtp2.becomeFirstResponder()
            }
            else if(textField == txtOtp2){
                txtOtp3.becomeFirstResponder()
            }
            else if(textField == txtOtp3){
                txtOtp4.becomeFirstResponder()
            }
            else if(textField == txtOtp4){
                txtOtp5.becomeFirstResponder()
            }
            else if(textField == txtOtp5){
                resignFirstResponder()
            }
            
        }
        
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let allowedCharacters = CharacterSet(charactersIn:"0123456789")//Here change this characters based on your requirement
        let characterSet = CharacterSet(charactersIn: string)
        
        if(string.count > 1){
            return false
        }
        
        if(string == "")
        {
            return true
        }
        if (1 <= textField.text?.count ?? 0){
            
            textField.text = ""
            return true
        }
        
        return allowedCharacters.isSuperset(of: characterSet)
        
    }
    
    
    
    // MARK:- ACTIONS
    
    @IBAction func btnBackClicked(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func btnResendCode(_ sender: UIButton) {
        resendOtp()
    }
    
    @IBAction func btnVerifyClicked(_ sender: UIButton) {
        
        strOtp = ""
        
        strOtp.append(txtOtp1.text ?? "")
        strOtp.append(txtOtp2.text ?? "")
        strOtp.append(txtOtp3.text ?? "")
        strOtp.append(txtOtp4.text ?? "")
        strOtp.append(txtOtp5.text ?? "")
        
        
        if(strOtp.count == 5){
            // call api
            verifyOtp()
            
        }else{
            // show alert
            showAlert(title: "Please enter full OTP", message: "")
        }
        
        //        let strbdMap = staticClass.getStoryboard_Map()
        //        let vc = strbdMap?.instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
        //        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func btnSgnUpCliicked(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    private func resendOtp(){
        
        //  http://stageofproject.com/texiapp-new/web_service/ResendOtp
        //userId
        //        Hud.shared.showHud()
        ActivityIndicatorWithLabel.shared.showProgressView(uiView: self.view)
        
        let userID = global.userId
        let dictKeys = [ "userId" : userID ] as [String : Any]
        
        
        let url = Constants.AppLinks.API_BASE_URL + Constants.ApiUrl.resendOtp.rawValue//Constants.ApiUrl.resendOtp.rawValue
        
        Alamofire.request(url , method: .post, parameters: dictKeys, headers:nil).responseJSON(completionHandler: { (response) in
            
            //            Hud.shared.hideHud()
            ActivityIndicatorWithLabel.shared.hideProgressView()
            do {
                let json: JSON = try JSON(data: response.data!)
                if (json["statusCode"].stringValue == "200"){
                    
                    self.showToast(message: "New code has been sent successfully on you mobile number.", seconds: 2)
                }
                else{
                    // error
                    self.showAlert(title: "Error", message: json["message"].stringValue)
                }
                
            } catch {
                // show a alert
                //                    Hud.shared.hideHud()
                ActivityIndicatorWithLabel.shared.hideProgressView()
                self.showAlert(title: "Error", message: "Please check network connection")
            }
        })
    }
    
    private func verifyOtp(){
        //verifyOtp
        //p  userId
        //p  otp
        
        //          Hud.shared.showHud()
        ActivityIndicatorWithLabel.shared.showProgressView(uiView: self.view)
        
        let userID = global.userId
        let dictKeys = [ "userId" : userID ,
                         "otp" : strOtp ] as [String : Any]
        
        
        
        var url = Constants.AppLinks.API_BASE_URL + Constants.ApiUrl.verifyOtp.rawValue//Constants.ApiUrl.verifyOtp.rawValue
        
        
        if( isWorkingVC == "password"){
            
            url = Constants.AppLinks.API_BASE_URL + Constants.ApiUrl.veriftOtpforpassword.rawValue//Constants.ApiUrl.veriftOtpforpassword.rawValue
        }
        
        
        Alamofire.request(url , method: .post, parameters: dictKeys, headers:nil).responseJSON(completionHandler: { (response) in
            
            //              Hud.shared.hideHud()
            ActivityIndicatorWithLabel.shared.hideProgressView()
            do {
                let json: JSON = try JSON(data: response.data!)
                if (json["statusCode"].stringValue == "200"){
                    
                    if( self.isWorkingVC == "password"){
                        self.showToast(message: "We send your new password in SMS", seconds: 3)
                        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1.5) {
                            
                            for controller in self.navigationController!.viewControllers as Array {
                                if controller.isKind(of: FirstLogInViewController.self) {
                                    self.navigationController!.popToViewController(controller, animated: true)
                                    break
                                }
                            }
                        }
                    }
                    else{
                        // show home screen
                        global.isUserLogin = true
                        self.showToast(message: "Verification Successful", seconds: 3)
                        self.showMapViewController()
                    }
                    
                }
                else{
                    // error
                    self.showAlert(title: "Error", message: json["message"].stringValue)
                }
                
            } catch {
                // show a alert
                //                      Hud.shared.hideHud()
                ActivityIndicatorWithLabel.shared.hideProgressView()
                self.showAlert(title: "Error", message: "Please check network connection")
            }
        })
    }
    
    
    private func showMapViewController() {
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1.5) {
            
            let stbdHome = staticClass.getStoryboard_Map()
            
            let nextViewController = stbdHome?.instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
            let navigationController = UINavigationController(rootViewController: nextViewController)
            let appdelegate = UIApplication.shared.delegate as! AppDelegate
            appdelegate.window?.rootViewController = navigationController
            
            
//            let strbdMap = staticClass.getStoryboard_Map()
//            let vc = strbdMap?.instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
//            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    
    
    //    func callForgetPasswordApi(){
    //
    //           Hud.shared.showHud()
    //
    //           let userID = global.userId
    //           let dictKeys = [ "phone" : userID ] as [String : Any]
    //
    //
    //           var url =  Constants.ApiUrl.resendOtp.rawValue
    //
    //
    //        if(isWorkingVC == "password"){
    //            url =  Constants.ApiUrl.resendOtp.rawValue
    //        }
    //
    //
    //           Alamofire.request(url , method: .post, parameters: dictKeys, headers:nil).responseJSON(completionHandler: { (response) in
    //
    //               Hud.shared.hideHud()
    //               do {
    //                   let json: JSON = try JSON(data: response.data!)
    //                       if (json["statusCode"].stringValue == "200"){
    //
    //                           if(self.isWorkingVC == "login"){
    //                               self.showToast(message: "OTP Send Successfully", seconds: 2)
    //                           }else{
    //                               self.showToast(message: "OTP Resend Successfully", seconds: 2)
    //                           }
    //
    //                           }
    //                       else{
    //                           // error
    //                           self.showAlert(title: "Error", message: json["message"].stringValue)
    //                       }
    //
    //               } catch {
    //                       // show a alert
    //                       Hud.shared.hideHud()
    //                       self.showAlert(title: "Error", message: "Please check network connection")
    //                   }
    //           })
    //       }
}
